const API_ROOT = process.env.PUBLIC_URL; // using public folder temporarily

// action key with info for Redux middleware
export const CALL_API = 'Call API';

// A Redux middleware that interprets actions with CALL_API info specified.
export default store => next => action => {
  const callAPI = action[CALL_API];

  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  let { endpoint } = callAPI;

  if (typeof endpoint !== 'string') {
    throw new Error('Specify a string endpoint URL.');
  }

  const actionWith = data => {
    const finalAction = Object.assign({}, callAPI, data);
    delete finalAction['endpoint'];
    return finalAction;
  }

  next(actionWith({isFetching: true}))

  return callApi(endpoint)
    .then(
      response => next(actionWith({response, isFetching: false})),
      error => next(actionWith({error: error.message || 'Something bad happened', isFetching: false}))
    );
};

const callApi = endpoint => {
  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint;
  return fetch(fullUrl)
    .then(
      response => 
        response.json().then(json => {
          if (!response.ok) {
            return Promise.reject(json);
          }

          return Promise.resolve(json);
        })
    );
}