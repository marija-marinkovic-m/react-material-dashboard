import { MEDIA_CHANGED, SIDEBAR_NAV_TOGGLED, FETCH_API_DATA, TRIGGERED_SEARCH } from '../constants/ActionTypes';
import { CALL_API } from '../middleware/api';

// actons
export const mediaChanged = () => ({type: MEDIA_CHANGED});
export const updateSideNav = data => ({type: SIDEBAR_NAV_TOGGLED, expanded: data.expanded});
export const triggeredSearch = (triggered) => ({type: TRIGGERED_SEARCH, triggered});

// 2lvl deep for now
export const fetchData = (endpoint, propName, propName2, propName3) => ({[CALL_API]: {type: FETCH_API_DATA, endpoint, propName, propName2, propName3}});