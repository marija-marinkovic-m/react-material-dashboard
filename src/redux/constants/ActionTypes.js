export const MEDIA_CHANGED = '@@pna/MEDIA_CHANGE';
export const SIDEBAR_NAV_TOGGLED = '@@pna/SIDEBAR_NAV_TOGGLE';
export const TRIGGERED_SEARCH = '@@pna/SEARCH';

// API actions
export const FETCH_API_DATA = '@@pna/FETCH_API_DATA';