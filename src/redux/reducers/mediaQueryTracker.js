import { MEDIA_CHANGED } from '../constants/ActionTypes';

const reducer = (state = {screenWidth: 0, screenHeight: 0}, action) => {
  switch(action.type) {
    case MEDIA_CHANGED:
      if (typeof window === 'undefined' || typeof document === 'undefined') return state;
      let w = window,
          d = document,
          documentElement = d.documentElement,
          body = d.getElementsByTagName('body')[0],
          screenWidth = w.innerWidth || documentElement.clientWidth || body.clientWidth,
          screenHeight = w.innerHeight || documentElement.clientHeight || body.clientHeight;

      return Object.assign({}, state, {screenWidth, screenHeight})
    default: 
      return state;
  }
}


export default reducer;