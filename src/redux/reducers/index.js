import mediaQueryTracker from './mediaQueryTracker';
import sideNavController from './sideNavController';
import apiDataController from './apiDataController';
import searchController from './searchController';

import { routerReducer as router } from 'react-router-redux';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  mediaQueryTracker,
  sideNavController,
  apiDataController,
  searchController,
  router
});

export default rootReducer;