import { TRIGGERED_SEARCH } from '../constants/ActionTypes';

const reducer = (state = {triggered: false}, action) => {
  switch(action.type) {
    case TRIGGERED_SEARCH:
      return Object.assign({}, state, {triggered: action.triggered});
    default: 
      return state;
  }
}

export default reducer;