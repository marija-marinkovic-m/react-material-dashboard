import { SIDEBAR_NAV_TOGGLED } from '../constants/ActionTypes';

const reducer = (state = {expanded: false}, action) => {
  switch(action.type) {
    case SIDEBAR_NAV_TOGGLED: 
      return Object.assign({}, state, {expanded: action.expanded});
    default:
      return state;
  }
}

export default reducer;