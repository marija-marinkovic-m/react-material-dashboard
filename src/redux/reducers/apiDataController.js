import { FETCH_API_DATA } from '../constants/ActionTypes';

const apiReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_API_DATA: 
      // 2lvl deep for now 
      // @update: 3rd level added on 20 July 2017. 11:33AM
      const data = action.propName2 ?
        Object.assign({}, state[action.propName], {[action.propName2]: (
          action.propName3 ?
            state[action.propName] &&
            state[action.propName].hasOwnProperty(action.propName2) ? 
              Object.assign({}, state[action.propName][action.propName2], {[action.propName3]: action}) : 
              {[action.propName3]: action} :
          action
        )}) :
        action;

      return Object.assign({}, state, {[action.propName]: data});
    default: 
      return state;
  }
}

export default apiReducer;