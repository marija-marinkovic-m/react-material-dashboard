import React from 'react';
import { Route, Switch } from 'react-router-dom';

// pages
import * as Screens from './shared/screens';


export const routesConfig = [
  {
    path: '/',
    exact: true,
    mainComponent: Screens.Home,
    appBarTitle: 'React D3 Dashboard Visualisations'
  },
  {
    path: '/contacts',
    exact: true,
    mainComponent: Screens.Contacts,
    appBarTitle: 'Contacts'
  },
  {
    path: '/contacts/add',
    exact: true,
    mainComponent: Screens.AddContact,
    appBarTitle: 'Add Contact'
  },
  {
    path: '/contacts/:profile',
    mainComponent: Screens.ContactProfile
  },
  {
    path: '/sales',
    mainComponent: Screens.Sales,
    appBarTitle: 'Sales'
  },
  {
    path: '/messages',
    mainComponent: Screens.Messages,
    appBarTitle: 'Messages'
  },
  {
    path: '/search/:term',
    mainComponent: Screens.Search,
    appBarTitle: 'Search Results'
  },
  {
    mainComponent: Screens.NotFound
  }
];

export const noAuthRoutesConfig = [
  {
    path: '/noauth/login',
    mainComponent: Screens.LogIn,
    exact: true
  },
  {
    path: '/noauth/signup',
    mainComponent: Screens.SignUp,
    exact: true
  },
  {
    path: '/noauth/forgot-password',
    mainComponent: Screens.ForgotPassword
  },
  {
    path: '/noauth/reset-password',
    mainComponent: Screens.ResetPassword
  },
  {
    path: '/noauth/onboard-start',
    mainComponent: Screens.OnboardStart
  },
  {
    mainComponent: Screens.NoAuth404
  }
];

export const noAuthRoutes = (
  <Switch>
    { noAuthRoutesConfig.map((r,i) => (
      <Route
        key={i}
        path={r.path}
        exact={r.exact}
        component={r.mainComponent} />
    )) }
  </Switch>
);

export default (
  <Switch>
    { routesConfig.map((r,i) => (
      <Route
        key={i}
        path={r.path}
        exact={r.exact}
        component={r.mainComponent} />
    )) }
  </Switch>
);