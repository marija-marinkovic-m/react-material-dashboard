import React from 'react';
import { render } from 'react-dom';

import createHistory from 'history/createBrowserHistory';

import Root from './shared/containers/Root';
import configureStore from './redux/store/configureStore';

import registerServiceWorker from './registerServiceWorker';
import './index.css';


const store = configureStore();
export const history = createHistory({
  basename: '/'
})

// @TEMP:
// import injectTapEventPlugin from 'react-tap-event-plugin';

render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);
registerServiceWorker();

// @TEMP
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
// injectTapEventPlugin();