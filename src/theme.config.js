import React from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// icons
import { CheckboxCheckedIcon, CheckboxUncheckedIcon } from './shared/util/svg-icons/checkbox';
import { RadioCheckedIcon, RadioUncheckedIcon } from './shared/util/svg-icons/radio';

import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down'

const spacing = {
  iconSize: 24,
  desktopGutter: 24,
  desktopGutterMore: 32,
  desktopGutterLess: 16,
  desktopGutterMini: 8,
  desktopKeylineIncrement: 64,
  desktopDropDownMenuItemHeight: 32,
  desktopDropDownMenuFontSize: 15,
  desktopDrawerMenuItemHeight: 48,
  desktopSubheaderHeight: 48,
  desktopToolbarHeight: 56,
}

export const themeColors = {
  gold: '#FBDF30',
  white: '#FFFFFF',
  red: '#F25967',
  green: '#7ED321',
  blue: '#1875F0',
  orange: '#FBC738',
  statusBlue: '#4A90E2',
  statusGold: '#FBC738',
  statusRed: '#F25967',
  gray: '#4F596A',
  mediumGray: '#7F8FA4',
  lightGray: '#BFCCDC',
  lightestGray: '#ECEFF2',
  sidebarNavText: '#7F8FA4',
  topBarBg: '#FFFFFF',
  darkGray: '#222B3A',
  contactToolbarBg: '#222B3A',
  contactFiltersDrawerBg: '#1A202A'
}


export const themeInputProps = {
  checkbox: {
    uncheckedIcon: <CheckboxCheckedIcon />,
    checkedIcon: <CheckboxUncheckedIcon style={{fill: '#2D3B4C'}} />,
    iconStyle: {
      width: '22px',
      height: '22px',
      marginRight: '12px'
    },
    style: {marginBottom: '6px'},
    labelStyle: {
      fontSize: '0.7em', lineHeight: '1.75em',
      color: themeColors.white,
      fontFamily: '"AvenirLTStd-Roman", Roboto, sans-serif',
    }
  },
  radio: {
    uncheckedIcon: <RadioUncheckedIcon />,
    checkedIcon: <RadioCheckedIcon />,
    iconStyle: {marginRight: '9px'},
    labelStyle: {
      width: 'auto',
      fontSize: '0.7em', lineHeight: '2.35em',
      color: themeColors.white,
      fontFamily: '"AvenirLTStd-Roman", Roboto, sans-serif',
    }
  },
  textField: {
    underlineShow: false,
    className: 'pnaTextField',
    style: {
      height: '37px',
      padding: '0 20px',
      border: '0.5px solid #4F596A',
      borderRadius: '2px',
      boxSizing: 'border-box'
    }
  },
  snackbar: {
    style: {bottom: 'auto', top: '45px'},
    bodyStyle: {minHeight: '38px', height: 'auto', lineHeight: '38px', borderRadius: '25px'},
    className: 'pna-Snackbar'
  },
  dropdown: {
    iconButton: <NavigationArrowDropDown />,
    labelStyle: {fontFamily: '"AvenirLTStd-Roman", Roboto, sans-serif', fontSize: '12px', color: themeColors.lightestGray},
    underlineStyle: {display: 'none'},
    autoWidth: false,
    style: {width: '100%', border: '1px solid #FFFFFF', height: 37},
    className: 'pna-light-dropdown'
  }
}

export const dashboardWidgetDropdownProps = {
  iconButton: <NavigationArrowDropDown className="dropdown-arr" />,
  iconStyle: {left: '16px', right: 'auto'},
  labelStyle: {paddingRight: '24px', paddingLeft: '56px', color: '#FFF', fontSize: '10px', textTransform: 'uppercase'},
  style: {marginRight: 0, height: '56px'}
}

export const themeTabStyle = {
  width: 'auto', height: 'auto',
  padding: '10px 0', margin: '0px 20px 0',
  textTransform: 'none', letterSpacing: '1px', fontSize: '12px', fontFamily: "'AvenirLTStd-Roman', sans-serif", fontWeight: '100'
}
export const getCurrentTabStyle = (currentTab, tabValue, mobView = false) => !mobView ? 
  Object.assign({}, themeTabStyle, {
    borderBottom: '2px solid ' + (tabValue === currentTab ? themeColors.gold : 'transparent'),
    borderTop: 'none'
  }) : 
  ({
    borderTop: '2px solid ' + (tabValue === currentTab ? themeColors.gold : 'transparent'), height: 70,
    borderBottom: 'none'
  })


export const customFlatButton = (() => {
  const base = {
    minWidth: 140,
    border: '1px solid transparent',borderRadius: 100,
    height: 40, lineHeight: '38px', margin: 5,
    color: themeColors.darkGray, fontSize: '10px', fontFamily: '"AvenirLTStd-Heavy", sans-serif', letterSpacing: 1, textTransform: 'uppercase',
    boxSizing: 'border-box'
  };
  return ({
    primary: Object.assign({}, base, {backgroundColor: themeColors.gold}),
    secondary: Object.assign({}, base, {borderColor: themeColors.lightGray, minWidth: 110}),
    info: Object.assign({}, base, {backgroundColor: themeColors.blue, color: themeColors.white}),
    warn: Object.assign({}, base, {backgroundColor: themeColors.red, color: themeColors.white})
  })
})();


const pnaTheme = getMuiTheme({
  spacing,
  fontFamily: '"AvenirLTStd-Roman", Roboto, Helvetica, sans-serif',
  appBar: {
    color: themeColors.white,
    textColor: themeColors.white,
  },
  avatar: {
    color: themeColors.lightGray,
    backgroundColor: themeColors.gray,
    style: {fontFamily: 'Roboto, Helvetica, sans-serif'}
  },
  drawer: {
    color: themeColors.darkGray
  },
  toolbar: {
    color: '#FFFFFF',
    backgroundColor: themeColors.contactToolbarBg
  },
  checkbox: {
    checkedColor: themeColors.blue,
    boxColor: '#DDE1E7',
    checkedIcon: <CheckboxCheckedIcon />,
    uncheckedIcon: <CheckboxUncheckedIcon />
  },
  flatButton: {
    fontSize: '10px'
  },
  floatingActionButton: {
    secondaryColor: themeColors.gold,
    secondaryIconColor: themeColors.darkGray
  },
  tabs: {
    backgroundColor: themeColors.darkGray,
    textColor: themeColors.mediumGray,
    selectedTextColor: themeColors.white
  },
  tableRow: {
    stripeColor: 'rgba(79,89,106,0.35)',
    textColor: themeColors.white
  },
  listItem: {
    secondaryTextColor: themeColors.mediumGray
  },
  snackbar: {
    textColor: themeColors.darkGray,
    backgroundColor: themeColors.gold,
    actionColor: themeColors.mediumGray
  },
  datePicker: {
    selectColor: themeColors.blue
  }
});

export default pnaTheme;