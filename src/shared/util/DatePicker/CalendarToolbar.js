import React, {Component} from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import SlideInTransitionGroup from 'material-ui/internal/SlideIn';

import { TriangleNext, TrianglePrev } from '../svg-icons/triangle-down';
import { themeColors } from '../../../theme.config';

const styles = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: 'inherit',
    height: 48,
  },
  titleDiv: {
    fontSize: 14,
    fontWeight: '500',
    textAlign: 'center',
    width: '100%',
  },
  titleText: {
    height: 'inherit',
    paddingTop: 12,
  },
};

class CalendarToolbar extends Component {
  static propTypes = {
    DateTimeFormat: PropTypes.func.isRequired,
    displayDate: PropTypes.object.isRequired,
    locale: PropTypes.string.isRequired,
    nextMonth: PropTypes.bool,
    onMonthChange: PropTypes.func,
    prevMonth: PropTypes.bool,
  };

  static defaultProps = {
    nextMonth: true,
    prevMonth: true,
  };

  static contextTypes = {
    muiTheme: PropTypes.object.isRequired,
  };

  state = {
    transitionDirection: 'up',
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.displayDate !== this.props.displayDate) {
      const nextDirection = this.context.muiTheme.isRtl ? 'right' : 'left';
      const prevDirection = this.context.muiTheme.isRtl ? 'left' : 'right';
      const direction = nextProps.displayDate > this.props.displayDate ? nextDirection : prevDirection;
      this.setState({
        transitionDirection: direction,
      });
    }
  }

  handleTouchTapPrevMonth = () => {
    if (this.props.onMonthChange) {
      this.props.onMonthChange(-1);
    }
  };

  handleTouchTapNextMonth = () => {
    if (this.props.onMonthChange) {
      this.props.onMonthChange(1);
    }
  };

  render() {
    const {DateTimeFormat, locale, displayDate} = this.props;

    const dateTimeFormatted = new DateTimeFormat(locale, {
      month: 'long',
      year: 'numeric',
    }).format(displayDate);

    const triangleStyle = {width: '8px', height: '8px', marginTop: '-5px', color: themeColors.blue};

    const nextButtonIcon = this.context.muiTheme.isRtl ? <TrianglePrev /> : <TriangleNext />;
    const prevButtonIcon = this.context.muiTheme.isRtl ? <TriangleNext /> : <TrianglePrev />;


    return (
      <div style={styles.root}>
        <IconButton
          iconStyle={triangleStyle}
          disabled={!this.props.prevMonth}
          onClick={this.handleTouchTapPrevMonth}
        >
          {prevButtonIcon}
        </IconButton>
        <SlideInTransitionGroup
          direction={this.state.transitionDirection}
          style={styles.titleDiv}
        >
          <div key={dateTimeFormatted} style={styles.titleText}>
            {dateTimeFormatted}
          </div>
        </SlideInTransitionGroup>
        <IconButton
          iconStyle={triangleStyle}
          disabled={!this.props.nextMonth}
          onClick={this.handleTouchTapNextMonth}
        >
          {nextButtonIcon}
        </IconButton>
      </div>
    );
  }
}

export default CalendarToolbar;
