import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let TriangleDownIcon = props => (
  <SvgIcon {...props}>
    <path d="M24.008,1.558H-0.007L12,22.442L24.008,1.558z"/>
  </SvgIcon>
);

let TriangleNext = props => (
  <SvgIcon {...props}>
    <path d="M1.554-0.016v24.031L22.446,12L1.554-0.016z"/>
  </SvgIcon>
);

let TrianglePrev = props => (
  <SvgIcon {...props}>
    <path d="M22.446,24.016V-0.016L1.554,12L22.446,24.016z"/>
  </SvgIcon>
);

TriangleDownIcon = pure(TriangleDownIcon);
TriangleDownIcon.displayName = 'TriangleDownIcon';
TriangleDownIcon.muiName = 'SvgIcon';

TriangleNext = pure(TriangleNext);
TriangleNext.displayName = 'TriangleNext';
TriangleNext.muiName = 'SvgIcon';

TrianglePrev = pure(TrianglePrev);
TrianglePrev.displayName = 'TrianglePrev';
TrianglePrev.muiName = 'SvgIcon';

export {TriangleNext, TrianglePrev};

export default TriangleDownIcon;