import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let OutlineCircle = props => (
  <SvgIcon {...props}>
    <g>
      <path fill="#FFFFFF" d="M11.972,20.858c-4.901,0-8.889-3.986-8.889-8.887c0-4.901,3.988-8.889,8.889-8.889	c4.9,0,8.887,3.988,8.887,8.889C20.858,16.872,16.872,20.858,11.972,20.858z"/>
      <path d="M11.972,5.883c3.356,0,6.087,2.731,6.087,6.088c0,3.356-2.73,6.087-6.087,6.087c-3.357,0-6.088-2.73-6.088-6.087 C5.883,8.614,8.614,5.883,11.972,5.883 M11.972,0.283c-6.456,0-11.688,5.233-11.688,11.688c0,6.455,5.233,11.687,11.688,11.687 c6.455,0,11.687-5.231,11.687-11.687C23.658,5.516,18.427,0.283,11.972,0.283L11.972,0.283z"/>
    </g>
  </SvgIcon>
);

OutlineCircle = pure(OutlineCircle);
OutlineCircle.displayName = 'OutlineCircle';
OutlineCircle.muiName = 'SvgIcon';

export default OutlineCircle;