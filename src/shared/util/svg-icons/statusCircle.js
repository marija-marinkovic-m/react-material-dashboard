import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let StatusCircle = props => (
  <SvgIcon {...props}>
    <path transform="translate(5 5)" d="M 6 12C 9.31371 12 12 9.31371 12 6C 12 2.68629 9.31371 0 6 0C 2.68629 0 0 2.68629 0 6C 0 9.31371 2.68629 12 6 12Z"/>
  </SvgIcon>
);

StatusCircle = pure(StatusCircle);
StatusCircle.displayName = 'StatusCircle';
StatusCircle.muiName = 'SvgIcon';

export default StatusCircle;