import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let PinIcon = props => (
  <SvgIcon {...props}>
    <path d="M18.22,3.959c-3.433-3.434-9.003-3.434-12.438,0c-3.435,3.434-3.435,9.003,0,12.438l6.22,6.219l6.219-6.219 C21.655,12.963,21.655,7.393,18.22,3.959z M10.224,11.954c-0.983-0.979-0.983-2.572,0-3.554c0.981-0.981,2.571-0.981,3.553,0 c0.982,0.981,0.982,2.573,0,3.554C12.795,12.936,11.206,12.936,10.224,11.954z"/>
  </SvgIcon>
);

PinIcon = pure(PinIcon);
PinIcon.displayName = 'PinIcon';
PinIcon.muiName = 'SvgIcon';

export default PinIcon;