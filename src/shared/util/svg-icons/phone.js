import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let PhoneIcon = props => (
  <SvgIcon {...props}>
    <path d="M15.875,14.583l-2.712,1.292c-1.033-0.516-1.938-1.292-2.842-2.195c-0.904-0.904-1.68-1.809-2.325-2.842l1.421-2.712 L8.125,1.666H1.667V2.57c0,5.167,2.067,10.205,5.813,13.951c3.746,3.746,8.784,5.813,13.951 5.813h0.903v-6.459L15.875,14.583z"/>
  </SvgIcon>
);

PhoneIcon = pure(PhoneIcon);
PhoneIcon.displayName = 'PhoneIcon';
PhoneIcon.muiName = 'SvgIcon';

export default PhoneIcon;