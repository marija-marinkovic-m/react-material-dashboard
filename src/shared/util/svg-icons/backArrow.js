import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let BackArrowIcon = props => (
  <SvgIcon {...props}>
    <path d="M9.005,5.816l-8.986,5.999l8.986,5.997v-4.499h14.978v-2.998H9.005V5.816z"/>
  </SvgIcon>
);

BackArrowIcon = pure(BackArrowIcon);
BackArrowIcon.displayName = 'BackArrowIcon';
BackArrowIcon.muiName = 'SvgIcon';

export default BackArrowIcon;