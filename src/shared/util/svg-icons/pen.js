import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let PenIcon = props => (
  <SvgIcon {...props}>
    <path d="M0.875,23.017l12.875-2.787l8.761-8.762c0.53-0.53,0.53-1.327,0-1.857l-8.229-8.23c-0.531-0.531-1.328-0.531-1.859,0 l-8.76,8.76L0.875,23.017L0.875,23.017z M12.555,17.972l-5.574,1.195l-2.39-2.257l1.195-5.574L12.555,17.972L12.555,17.972z" />
  </SvgIcon>
);

PenIcon = pure(PenIcon);
PenIcon.displayName = 'PenIcon';
PenIcon.muiName = 'SvgIcon';

export default PenIcon;