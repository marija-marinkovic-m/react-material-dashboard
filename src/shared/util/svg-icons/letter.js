import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let LetterIcon = props => (
  <SvgIcon {...props}>
    <path id="Canvas" transform="translate(-5815 -1105)" d="M5824.401,1105.48l-8.801,6.6l0.001,9.9h17.6v-9.9L5824.401,1105.48z M5824.401,1108.23l5.132,3.85l-5.132,3.85l-5.134-3.85L5824.401,1108.23z"/>
  </SvgIcon>
);

LetterIcon = pure(LetterIcon);
LetterIcon.displayName = 'LetterIcon';
LetterIcon.muiName = 'SvgIcon';

export default LetterIcon;