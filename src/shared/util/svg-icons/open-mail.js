import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let MailIcon = props => (
  <SvgIcon {...props}>
    <path d="M12,2.079L1.417,10.015v11.907h21.166V10.015L12,2.079z M12,14.646l-6.218-4.63L12,5.386l6.218,4.629L12,14.646L12,14.646z" />
  </SvgIcon>
);

MailIcon = pure(MailIcon);
MailIcon.displayName = 'MailIcon';
MailIcon.muiName = 'SvgIcon';

export default MailIcon;