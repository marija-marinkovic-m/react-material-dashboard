import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let ReminderIcon = props => (
  <SvgIcon {...props}>
    <path fillRule="evenodd" transform="translate(6 6)" d="M 12 8.8L 12 7.2C 12 4.664 10.3128 2.524 8 1.8352L 8 1.6C 8 0.7168 7.2832 0 6.4 0C 5.516 0 4.8 0.7168 4.8 1.6L 4.8 1.8352C 2.4872 2.524 0.8 4.664 0.8 7.2L 0.8 8.8L 0 8.8L 0 10.4L 4.8 10.4C 4.8 11.284 5.516 12 6.4 12C 7.2832 12 8 11.284 8 10.4L 12.8 10.4L 12.8 8.8L 12 8.8Z"/>
  </SvgIcon>
);

ReminderIcon = pure(ReminderIcon);
ReminderIcon.displayName = 'ReminderIcon';
ReminderIcon.muiName = 'SvgIcon';

export default ReminderIcon;