import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let CustomAddIcon = props => (
  <SvgIcon {...props}>
    <path fillRule="evenodd" d="M 6.76724 5.23276L 6.76724 0.460346C 6.76724 0.0364247 6.42392 0 6 0C 5.57608 0 5.23276 0.0364247 5.23276 0.460346L 5.23276 5.23276L 0.460346 5.23276C 0.0364247 5.23276 0 5.57608 0 6C 0 6.42392 0.0364247 6.76724 0.460346 6.76724L 5.23276 6.76724L 5.23276 11.5397C 5.23276 11.9636 5.57608 12 6 12C 6.42392 12 6.76724 11.9636 6.76724 11.5397L 6.76724 6.76724L 11.5397 6.76724C 11.9636 6.76724 12 6.42392 12 6C 12 5.57608 11.9636 5.23276 11.5397 5.23276L 6.76724 5.23276Z"/>
  </SvgIcon>
);

CustomAddIcon = pure(CustomAddIcon);
CustomAddIcon.displayName = 'CustomAddIcon';
CustomAddIcon.muiName = 'SvgIcon';

export default CustomAddIcon;