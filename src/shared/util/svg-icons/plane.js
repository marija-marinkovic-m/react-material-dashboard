import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let PlaneIcon = props => (
  <SvgIcon {...props}>
    <path transform="translate(-165 -1960)" d="M168.47,1975.351l6.618-2.648l-6.618-2.647l-2.646-9.265 L187,1972.702l-21.177,11.913L168.47,1975.351z"/>
  </SvgIcon>
);

PlaneIcon = pure(PlaneIcon);
PlaneIcon.displayName = 'PlaneIcon';
PlaneIcon.muiName = 'SvgIcon';

export default PlaneIcon;