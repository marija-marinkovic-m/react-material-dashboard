import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let CheckboxUnchecked = props => (
  <SvgIcon {...props}>
    <path fillRule="evenodd" d="M 0 0L 18 0L 18 18L 0 18L 0 0Z"/>
  </SvgIcon>
);

CheckboxUnchecked = pure(CheckboxUnchecked);
CheckboxUnchecked.displayName = 'CheckboxUncheckedIcon';
CheckboxUnchecked.muiName = 'SvgIcon';

let CheckboxChecked = props => (
  <SvgIcon {...props}>
    <g transform="translate(-2141 415)">
      <path transform="translate(2142 -415)" fill="#2D3B4C" fillRule="evenodd" d="M 0 0L 18 0L 18 18L 0 18L 0 0Z"/>
      <path transform="translate(2146 -409)" fill="#FBDF30" fillRule="evenodd" d="M 9.8355 1.60732L 5.23836 6.42765L 3.70598 8.03456L 2.1736 6.42765L 0 4.10579L 1.53278 2.49888L 3.71449 4.81223L 8.30312 0L 9.8355 1.60732Z"/>
    </g>
  </SvgIcon>
);

CheckboxChecked = pure(CheckboxChecked);
CheckboxChecked.displayName = 'CheckboxCheckedIcon';
CheckboxChecked.muiName = 'SvgIcon';


let CheckboxCheckedLight = props => (
  <SvgIcon {...props}>
    <g transform="translate(-2716 1598)">
      <path fillRule="evenodd" fill="#FFFFFF" d="M 0 0L 18 0L 18 18L 0 18L 0 0Z"/>
      <path transform="translate(2716 -1598)" d="M 0 0L 0 -0.5L -0.5 -0.5L -0.5 0L 0 0ZM 18 0L 18.5 0L 18.5 -0.5L 18 -0.5L 18 0ZM 18 18L 18 18.5L 18.5 18.5L 18.5 18L 18 18ZM 0 18L -0.5 18L -0.5 18.5L 0 18.5L 0 18ZM 0 0.5L 18 0.5L 18 -0.5L 0 -0.5L 0 0.5ZM 17.5 0L 17.5 18L 18.5 18L 18.5 0L 17.5 0ZM 18 17.5L 0 17.5L 0 18.5L 18 18.5L 18 17.5ZM 0.5 18L 0.5 0L -0.5 0L -0.5 18L 0.5 18Z"/>
      <path transform="translate(2720.08 -1593.02)" fillRule="evenodd" fill="#4A90E2" d="M 9.8355 1.60732L 5.23836 6.42765L 3.70598 8.03456L 2.1736 6.42765L 0 4.10579L 1.53278 2.49888L 3.71449 4.81223L 8.30312 0L 9.8355 1.60732Z"/>
    </g>
  </SvgIcon>
);

CheckboxCheckedLight = pure(CheckboxCheckedLight);
CheckboxCheckedLight.displayName = 'CheckboxCheckedLightIcon';
CheckboxCheckedLight.muiName = 'SvgIcon';


let CheckboxCheckedBlue = props => (
  <SvgIcon {...props}>
    <g transform="translate(-2406 -1189)">
      <path transform="translate(2406 1189)" fill="#1875F0" d="M 0 0L 24 0L 24 24L 0 24L 0 0Z"/>
      <path transform="translate(2411.44 1195.64)" fill="#FFFFFF" d="M 13.114 2.14309L 6.98448 8.5702L 4.94131 10.7127L 2.89814 8.5702L 0 5.47438L 2.04371 3.33183L 4.95265 6.41631L 11.0708 0L 13.114 2.14309Z"/>
    </g>
  </SvgIcon>
);

CheckboxCheckedBlue = pure(CheckboxCheckedBlue);
CheckboxCheckedBlue.displayName = 'CheckboxCheckedBlue';
CheckboxCheckedBlue.muiName = 'SvgIcon';


export {CheckboxChecked as CheckboxCheckedIcon, CheckboxUnchecked as CheckboxUncheckedIcon, CheckboxCheckedLight as CheckboxCheckedLightIcon, CheckboxCheckedBlue};