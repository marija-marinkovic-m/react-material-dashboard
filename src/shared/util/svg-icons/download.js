import React from 'react';
import pure from 'recompose/pure';
import SvgIcon from 'material-ui/SvgIcon';

let DownloadIcon = props => (
  <SvgIcon {...props}>
    <path transform="translate(-677 -1415)" d="M686.996,1427.27v1.555h-9.333v-1.555H686.996L686.996,1427.27z M686.441,1419.496l-3.34,3.339v-7.455h-1.555v7.455l-3.339-3.339l-1.1,1.1l5.216,5.217l5.217-5.217L686.441,1419.496 L686.441,1419.496z"/>
  </SvgIcon>
);

DownloadIcon = pure(DownloadIcon);
DownloadIcon.displayName = 'DownloadIcon';
DownloadIcon.muiName = 'SvgIcon';

export default DownloadIcon;