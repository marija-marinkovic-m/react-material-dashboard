import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import TooltipTrigger from './Tooltip';
import cn from 'classnames';

import './SpeedDial.css';

export class SpeedDial extends Component {
  static propTypes = {
    onOpenCloseRequest: PropTypes.func,
    effect: PropTypes.oneOf(['none', 'fade-staggered', 'fade', 'slide']),
    open: PropTypes.bool,
    style: PropTypes.object,
    fabProps: PropTypes.object,
    fabContentOpen: PropTypes.node,
    fabContentClose: PropTypes.node,
    direction: PropTypes.oneOf(['vertical', 'horizontal']),
    hasOverlay: PropTypes.bool
  }

  static defaultProps = {
    open: false,
    effect: 'fade-staggered',
    fabContentOpen: () => false,
    direction: 'vertical',
    hasOverlay: false
  };

  constructor(props) {
    super(props);
    this.state = {
      internalOpen: false
    }
  }

  handleFabTouchTap = () => {
    this.setState({internalOpen: !this.state.internalOpen});
    this.props.onOpenCloseRequest && this.props.onOpenCloseRequest();
  }

  handleCloseRequest = () => this.handleFabTouchTap();

  componentDidMount() {
    if (this.props.open !== 'undefined') this.setState({internalOpen: this.props.open});
  }

  render() {
    const { effect, style, fabProps, fabContentOpen, fabContentClose, direction, hasOverlay } = this.props;
    const { internalOpen } = this.state;

    const enhancedChildren = React.Children.map(this.props.children,
      (child, index) => React.cloneElement(child, {
        effect,
        index,
        visible: internalOpen,
        direction,
        onCloseRequest: this.handleCloseRequest
      })
    );

    return (<div>
      
      { hasOverlay && internalOpen && <div className="speed-dial-overlay" /> }

      <div style={{...styles.container}} className={cn({isActive: internalOpen})}>

        <FloatingActionButton
          {...fabProps}
          onTouchTap={this.handleFabTouchTap}
        >
          <FabSpinner
            aContent={fabContentOpen}
            bContent={fabContentClose || fabContentOpen}
            showB={internalOpen}
            style={style}
          />
        </FloatingActionButton>

        {enhancedChildren}

      </div>
    </div>);
  }
}

export class SpeedDialItem extends Component {
  state = {
    hovered: false
  }
  static propTypes = {
    onCloseRequest: PropTypes.func,
    onTouchTap: PropTypes.func,
    index: PropTypes.number,
    visible: PropTypes.bool,
    fabContent: PropTypes.node,
    effect: PropTypes.oneOf(['none', 'fade-staggered', 'fade', 'slide']),
    direction: PropTypes.oneOf(['vertical', 'horizontal']),
    label: PropTypes.string,
    labelTrigger: PropTypes.oneOf(['hover', 'click', 'always'])
  }
  static defaultProps = {
    onCloseRequest: () => false,
    onTouchTap: e => false,
    effect: 'fade-staggered',
    labelTrigger: 'hover'
  }

  handleTouchTap = event => {
    this.props.onCloseRequest();
    this.props.onTouchTap(event);
  }

  handleHover = (hovered) => {
    this.setState({hovered})
  }

  render() {
    const { index, visible, effect, fabContent, label, direction, labelTrigger } = this.props;
    const directionProps = direction === 'vertical' ? {right: 0, bottom: getYPos(index)} : {top: 0, right: getYPos(index)};
    const containerProps = {
      className: effect,
      style: Object.assign({
        pointerEvents: visible ? '' : 'none',
        position: 'absolute',
        whiteSpace: 'nowrap',
        ...directionProps,
      }, effects[effect](visible, index))
    };

    const fab = <FloatingActionButton
      mini={false} iconStyle={{fill: '#222B3A'}} backgroundColor={this.state.hovered ? '#F4F5F6' : '#FFFFFF'}
      onMouseEnter={this.handleHover.bind(null, true)}
      onMouseLeave={this.handleHover.bind(null, false)}
      onTouchTap={this.handleTouchTap}>
      { fabContent }
    </FloatingActionButton>;
    return (
      <div {...containerProps}>
        { label ? <TooltipTrigger tooltip={label} placement={direction === 'vertical' ? 'left' : 'bottom'} className="text-uppercase" trigger={labelTrigger}>{fab}</TooltipTrigger> : fab }
      </div>
    );
  }
}

const FabSpinner = ({bContent, aContent, showB, style}) => {
  let ac = React.cloneElement(aContent, {style: {...style, ...aContent.style}});
  let bc = React.cloneElement(bContent, {style: {...style, ...bContent.style}});

  return (<div style={styles.fabSpinnerContainer}>
    <div style={{
      ...styles.fabSpinnerOverlap,
      transform: 'translateY(-50%) rotate(' + (showB ? '90deg' : 0) + ')',
      opacity: showB ? 0 : 1
    }}>
      {ac}
    </div>

    <div style={{
      ...styles.fabSpinnerOverlap,
      transform: 'translateY(-150%) rotate(' + (showB ? 0 : '-90deg') + ')',
      opacity: showB ? 1 : 0
    }}>
      {bc}
    </div>
  </div>);
}

const styles = {
  container: {
    position: 'relative', zIndex: 2502,
    display: 'inline-block'
  },
  fabSpinnerContainer: {
    position: 'relative',
    width: '100%',
    height: 0,
    transform: 'translate(0,2px)'
  },
  fabSpinnerOverlap: {
    position: 'relative',
    transition: 'all 450ms'
  }
}

const effects = {

  'none': (visible, index) => ({
    display: visible ? '' : 'none'
  }),

  'fade-staggered': (visible, index) => ({
    transition: visible ? '450ms' : '300ms',
    transitionDelay: visible ? (index * 0.025) + 's' : '',
    transform: 'translateY(' + (visible ? 0 : '5px') + ')',
    opacity: visible ? 1 : 0
  }),

  'fade':  (visible, index) => ({
    transition: visible ? '450ms' : '300ms',
    opacity: visible ? 1 : 0
  }),

  'slide': (visible, index) => ({
    transition: visible ? '250ms' : '300ms',
    transform: 'translateY(' + (visible ? 0 : getYPos(index) + 'px') + ')',
    opacity: visible ? 1 : 0
  })

};

const getYPos = index => 70 + index * 70;