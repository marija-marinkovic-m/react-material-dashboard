import React, { Component } from 'react';
import PropTypes from 'prop-types';

export const Layer = ({fill, points, onMouseOut = () => false, onMouseOver = () => false}) => {
  return (
    <g className="layer" style={{fill}}>
      { points.map((p,i) => {
        const props = Object.assign({}, {
          onMouseOut,
          onMouseOver: onMouseOver.bind(null, p.title),
          stroke: fill,
          style: {strokeDasharray: `0 ${p.width + .75} ${p.height - 1.5} ${p.width + 1.5} ${p.height - 1.5} 0`}
        }, p);
        return <rect key={i} {...props} />
      }) }
    </g>
  );
}

export const LayerHorizontal = ({fill, points, onMouseOut = () => false, onMouseOver = () => false}) => {
  return (
    <g className="layer" style={{fill}}>
      { points.map((p,i) => {
        const props = Object.assign({}, {
          onMouseOut,
          onMouseOver: onMouseOver.bind(null, p.title),
          stroke: fill
        }, p);
        return <Bar key={i} {...props} />
      }) }
    </g>
  );
}

export const Bar = ({x, y,...other}) => {
  return (
    <g transform={`translate(${x}, ${y})`}>
      <rect 
        className="bar"
        stroke={other.fill}
        style={{strokeDasharray: `${other.width} ${other.height} ${other.width} ${other.height} 0`}}
        {...other} />
    </g>
  );
}

export const Tooltip = ({data, style}) => {
  const tooltipStyle = Object.assign({}, {
    position: 'fixed', zIndex: 99999, top: -999, left: -999,
    width: 'auto', height: 'auto',
    padding: '5px 10px',
    font: '12px sans-serif', color: 'white',
    background: 'rgba(0,0,0,.7)',
    borderRadius: '8px', pointerEvents: 'none', transition: 'opacity 300ms ease-in-out' 
  }, style);
  return <div className="tooltips tooltip-stacked-cart"
    style={tooltipStyle}
    dangerouslySetInnerHTML={{__html: data}} />
}

export const Axis = (renderer) => {

  return class extends Component {
    static propTypes = {
      axis: PropTypes.func.isRequired,
      height: PropTypes.number,
      transform: PropTypes.string,
      timeParseFormat: PropTypes.string
    }
    static defaultProps = {
      transform: 'translate(0,0)'
    }
    componentDidMount() { renderer.call(this) }
    componentDidUpdate() { renderer.call(this) }

    render() {
      return (
        <g ref={axis => this.axisRef = axis} />
      );
    }
  }
}

export const GraphHoc = function(WrappedComponent) {
  return class extends WrappedComponent {
    state = {
      DOM: null,
      tooltipData: '',
      tooltipStyle: {}
    }
    componentWillMount() {
      if (!super.calculate) {
        throw new Error('calculate method required');
      }
      super.calculate(this.props);
    }
    componentWillReceiveProps(nextProps) {
      if (!super.calculate) {
        throw new Error('calculate method required');
      }
      super.calculate(nextProps)
    }
    handleMouseOver = (tooltipData, e) => {
      this.setState({tooltipData, tooltipStyle: {
        opacity: .9, left: `${e.pageX}px`, top: `${e.pageY}px`
      }})
    }
    handleMouseOut = () => this.setState({tooltipData: '', tooltipStyle: {opacity: 0}})
    render() {
      const { SvgDOM, tooltipData, tooltipStyle } = this.state;
      return(
        <div>
          { SvgDOM }
          <Tooltip data={tooltipData} style={tooltipStyle} />
        </div>
      );
    }
  }
}