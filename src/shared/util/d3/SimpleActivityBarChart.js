import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { scaleBand, scaleLinear, scaleOrdinal } from 'd3-scale';
import { max } from 'd3-array';

import { GraphHoc } from './general';

class SimpleChartComponent extends Component {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    data: PropTypes.arrayOf(PropTypes.shape({label: PropTypes.string, count: PropTypes.number})),
    colors: PropTypes.arrayOf(PropTypes.string)
  }
  static defaultProps = {
    width: 800,
    height: 600,
    data: [
      {label: 'Active Contacts', count: 112},
      {label: 'Passive Contact', count: 31}
    ],
    colors: ['#FBDF30', '#F25967']
  }

  calculate(props) {
    const { width, height, data, colors } = props;

    const xScale = scaleBand()
      .domain(data.map(d => d.label))
      .rangeRound([0, width])
      .paddingInner(.5);
    const yScale = scaleLinear()
      .domain([0, max(data.map(d => d.count))])
      .range([height, 0]);
    const cScale = scaleOrdinal().range(colors);

    const SvgDOM = (
      <svg className="activity-chart" width={width} height={height}>
        { data.map((d, i) => (
          <rect key={i}
            fill={cScale(i)}
            x={xScale(d.label)}
            y={yScale(d.count)}
            width={xScale.bandwidth()}
            height={height - yScale(d.count)} />
        )) }
      </svg>
    );
    this.setState({SvgDOM});
  }
}

export const SimpleActivityBarChart = GraphHoc(SimpleChartComponent);