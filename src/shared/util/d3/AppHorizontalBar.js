import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Bar, GraphHoc } from './general';

import { select } from 'd3-selection';
import { scaleBand, scaleLinear, scaleOrdinal } from 'd3-scale';
import { axisBottom, axisLeft } from 'd3-axis';
import { format } from 'd3-format';
import { max } from 'd3-array';

import { themeColors } from '../../../theme.config';
import './AppHorizontalBar.css';

export class AxisLeft extends Component {
  static propTypes = {
    scale: PropTypes.any.isRequired,
    maxWidth: PropTypes.number,
    barInterspace: PropTypes.number
  }
  static defaultProps = {
    maxWidth: 50,
    barInterspace: 20
  }
  componentDidMount() { this.renderAxis() }
  componentDidUpdate() { this.renderAxis() }

  renderAxis = () => {
    if (!this.axisLeft || !this.props.scale) return;
    const leftAxis = axisLeft(this.props.scale);
    const g = select(this.axisLeft);
    const { maxWidth } = this.props;
    g.call(leftAxis);
    g.select('.domain').remove();

    // remove ticks
    g.selectAll('line').remove();
    // apply ellipsis
    g.selectAll('text')
      .each(function() {
        let self = select(this),
          textLength = self.node().getComputedTextLength(),
          text = self.text();
        while (textLength > maxWidth-20 && text.length > 0) {
          text = text.slice(0, -1);
          self.text(text + '...');
          textLength = self.node().getComputedTextLength();
        }
      })
      .attr('stroke', themeColors.white)
      .attr('text-anchor', 'start')
      .attr('font-size', '12px');
  }

  render() {
    return (
      <g className="axisLeft" transform={`translate(-${this.props.maxWidth+10}, -${this.props.barInterspace/2})`} ref={axisLeft => this.axisLeft = axisLeft} />
    );
  }
}

export class AxisBottom extends Component {
  static propTypes = {
    scaleValue: PropTypes.any,
    height: PropTypes.number
  }
  componentDidMount() {
    this.renderAxis();
  }
  componentDidUpdate() {
    this.renderAxis();
  }
  renderAxis = () => {
    if (!this.axisBottom || !this.props.scaleValue) return;

    this.props.scaleValue.ticks(4)

    const bottomAxis = axisBottom(this.props.scaleValue).tickSize(this.props.height+25).tickFormat(format('$.2s'));
    const g = select(this.axisBottom);
    g.call(bottomAxis);
    g.select('.domain').remove();
    g.selectAll('.tick line')
      .attr('stroke', themeColors.mediumGray);
    g.selectAll('.tick text')
      .attr('stroke', themeColors.mediumGray)
      .attr('y', this.props.height+40)
      .attr('dx', 0);
  }

  render() {
    const { width, height, style } = this.props;
    return (
      <svg width={width} height={height+40} style={Object.assign({}, {position: 'absolute', top: 0, left: 0, right: 0, zIndex: '-1', overflow: 'visible'}, style)}>
        <g ref={axisBottom => this.axisBottom = axisBottom} />
      </svg>
    );
  }
}

export const SvgGraph = ({children, mainWrapStyles = {}, ...other}) => (
  <div style={Object.assign({}, mainWrapStyles, {overflowX: 'hidden', overflowY: 'auto'})}>
    <svg
      viewBox={`0 0 ${other.width} ${other.height}`}
      style={{overflow: 'visible'}}
      preserveAspectRatio="xMinYMid"
      {...other}>
      <g transform="translate(0 3.5)">
        { children }
      </g>
    </svg>
  </div>
);

export class PNAHorizontalBarComponent extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    barWidth: PropTypes.number,
    barInterspace: PropTypes.number,
    labelsBarWidth: PropTypes.number,
    colors: PropTypes.arrayOf(PropTypes.string)
  }
  static defaultProps = {
    width: 520,
    height: 250,
    barWidth: 18,
    barInterspace: 25,
    labelsBarWidth: 150,
    colors: ['#95D9EE', '#3DA3C0', '#FEA171', '#FD6E69', '#7585AD']
  }
  calculate(props) {

    const { width, labelsBarWidth, height, data, barWidth, barInterspace, colors } = props;

    const xScale = scaleLinear()
      .domain([0, max(data.map(d => d.value))])
      .range([0, width-labelsBarWidth])

    const yScale = scaleBand()
      .domain(data.map(d => d.title))
      .rangeRound([0, (data.length * (barWidth+barInterspace))]);

    const cScale = colorScale(colors);
    
    const SvgDOM = (
      <div className="horizontal-bar-chart">
        <div className="main-holder" style={{width, height}}>
          <SvgGraph
            width={width-labelsBarWidth}
            height={data.length * (barWidth+barInterspace)}
            mainWrapStyles={{height, width: width-labelsBarWidth, paddingLeft: labelsBarWidth}}>
            { this.renderBars.call(this, xScale, yScale, cScale, props) }
            <AxisLeft
              scale={yScale}
              maxWidth={labelsBarWidth-20}
              barInterspace={barInterspace} />
          </SvgGraph>
          <AxisBottom
            scaleValue={xScale}
            style={{marginLeft: labelsBarWidth}}
            width={width-labelsBarWidth}
            height={height} />
        </div>
      </div>
    );
    this.setState({SvgDOM});
  }
  renderBars(xScale, yScale, cScale, props) {

    if (!xScale || !yScale || !cScale) return;
    const { data, barWidth } = props;

    return data.map((d,i) => <Bar
      key={i}
      width={xScale(d.value)}
      height={barWidth}
      fill={cScale(d.value)}
      y={yScale(d.title)}
      x="0"
      onMouseOver={this.handleMouseOver.bind(null, d.tooltip)}
      onMouseOut={this.handleMouseOut} />)
  }
}

export const formatValue = (value) => format('$.0s')(value);
export const colorScale = (colors) => scaleOrdinal(colors);

export const PNAHorizontalBar = GraphHoc(PNAHorizontalBarComponent);
