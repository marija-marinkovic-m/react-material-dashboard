import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { scaleOrdinal, scaleBand } from 'd3-scale';
import { axisBottom } from 'd3-axis';
import { line } from 'd3-shape';
import { select } from 'd3-selection';

import FunnelCoordinatesCalculator from './funnelCalculator';
import { GraphHoc, Axis } from './general';

import './Funnel.css';

class SalesFunnelComponent extends Component {
  static propTypes = {
    data: PropTypes.array,
    height: PropTypes.number,
    width: PropTypes.number,
    colors: PropTypes.array
  }

  static defaultProps = {
    data: [
      {process: 'Invitations sent', value: 400000, desc: ''},
      {process: 'Started Sales', value: 150000, desc: 'Invitation to Deals Started'},
      {process: 'Closed Sales', value: 50000, desc: 'Deals Started to Closed sales'}
    ],
    height: 200,
    width: 800,
    colors: ['#2EA1F8', '#FDC018', '#2BB415']
  }

  calculate(props) {
    const { width, height, data, colors } = props;
    const marginY = 90;

    const cScale = scaleOrdinal(colors);
    const xScale = scaleBand()
      .domain(data.map(d => d.process))
      .range([0,width]).padding([0]);
    const currentAxisBottom = axisBottom(xScale)
      .tickFormat(function(d) {return ''})
      .tickSize(height+marginY*2);

    const funnelData = new FunnelCoordinatesCalculator({
      data,
      size: [width,height]
    });

    const currentLine = line()
      .x(function(d,i) {return d.x})
      .y(function(d,i) {return d.y});

    const cellWidth = width/data.length;

    const SvgDOM = (
      <div style={{display: 'block', position: 'relative', width, height: height+marginY*2, margin: '0 auto'}}>

        { funnelData.map((d,i) => <DataCell
          key={i}
          style={{width: cellWidth, top: 0, left: i*cellWidth}}
          children={<p>{d.process}<br /><strong>$ {d.value}</strong></p>} />) }

        <svg width={width} height={height + marginY*2}>
          <AxisBottom axis={currentAxisBottom} transform={`translate(${(width/data.length)/2},0)`} />
          <g transform={`translate(0,${marginY})`}>
            { funnelData.map((d,i) => <g key={i} className="funnel-group">
              <path
                onMouseOver={d.deals && d.deals.length ? this.handleMouseOver.bind(null, this.formatTooltipData(d.deals)) : null}
                onMouseOut={this.handleMouseOut}
                d={currentLine(d.coordinates.values)} fill={cScale(i)} />
            </g>) }
          </g>
        </svg>

        { funnelData.map((d,i) => <DataCell
          key={i}
          style={{width: cellWidth, bottom: 0, left: i*cellWidth}}
          children={i===0 ? null : <p><strong>{funnelData[i-1].coordinates.percentage}%</strong><small>{d.desc}</small></p>} />) }
      </div>
    );
    this.setState({SvgDOM});
  }

  formatTooltipData = deals => {
    return `<table class="funnel-deals-table">
      <tbody>
        ${deals.map(d => `<tr><th>${d.name}</th><td>$${d.value}</td></tr>`).reduce((p,n) => p+n, '')}
      </tbody>
    </table>`;
  }
}

const AxisBottom = Axis(function() {
  if (!this.axisRef) return;
  const ref = select(this.axisRef);

  ref
    .attr('transform', this.props.transform)
    .call(this.props.axis)

  ref
    .select('.domain').remove();
  ref
    .selectAll('.tick line')
    .attr('stroke', '#4F596A')
    .attr('stroke-width', '0.5px');
  ref
    .selectAll('.tick:last-of-type line')
    .remove();
});

const DataCell = ({children, style}) => (
  <div
    className="funnel-info-cell"
    style={style}>
    { children }
  </div>
);

export default GraphHoc(SalesFunnelComponent);