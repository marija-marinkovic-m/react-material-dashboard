import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { scaleBand, scaleLinear, scaleOrdinal } from 'd3-scale';
import { stack, stackOffsetNone, area, stackOrderReverse, line } from 'd3-shape';
import { axisBottom, axisRight } from 'd3-axis';
import { timeParse, timeFormat } from 'd3-time-format';
import { max, min } from 'd3-array';
import { format } from 'd3-format';

// import { LineChart } from './LineChart';
import { AxisBottom, AxisLeft } from './AppBarStacked';

import { themeColors } from '../../../theme.config';

import { GraphHoc } from './general';

import './AppLineChart.css';

const D3Gradent = ({id, color}) => (
  <defs>
    <linearGradient is id={id} x1="0%" y1="100%" x2="0%" y2="0%" spreadMethod="pad">
      <stop is offset="0%" stop-color={color} stop-opacity={0} />
      <stop is offset="100%" stop-color={color} stop-opacity={.6} />
    </linearGradient>
  </defs>
);


export class PNASalesInvestorsChartComponent extends Component {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    data: PropTypes.array.isRequired,
    keys: PropTypes.array, // stack keys
    margin: PropTypes.shape({top: PropTypes.number, right: PropTypes.number, bottom: PropTypes.number, left: PropTypes.number}),
    colors: PropTypes.arrayOf(PropTypes.string)
  }
  static defaultProps = {
    width: 800,
    height: 600,
    margin: {top: 20, right: 10, bottom: 35, left: 50},
    colors: ['#3366cc', '#dc3912', '#ff9900', '#109618']
  }
  calculate(props) {
    const { width, height, data, margin } = props;

    const keys = ['contacts', 'investors'];

    const svgWidth = width - margin.left - margin.right;
    const svgHeight = height - margin.top - margin.bottom;

    const parsedTime = date => timeParse('%m/%Y')(date);

    const xScale = scaleBand()
      .domain(data.map(d => timeParse('%m/%Y')(d.date)))
      .range([0, width]);

    const xQScale = scaleBand()
      .domain(['Q1', 'Q2', 'Q3', 'Q4'])
      .range([0, svgWidth]);

    const cScale = scaleOrdinal().range([themeColors.gold, themeColors.blue]).domain(keys);

    const currentStack = stack()
      .keys(keys)
      .order(stackOrderReverse)
      .offset(stackOffsetNone)
    const layers = currentStack(data);
    const yScale = scaleLinear()
      .range([svgHeight, 0])
      .domain([min(layers, layer => min(layer, d => d[0])), max(layers, layer => max(layer, d => d[1]))])
      .nice();

    const xQAxis = axisBottom(xQScale)
      .ticks(4);
    const yAxis = axisRight(yScale)
      .tickFormat(format('$.2s'))
      .tickSize(width);


    const currentArea = area(layers)
      .x(function(d,i) {return xScale(parsedTime(d.data.date))})
      .y0(function(d) {return yScale(d[0])})
      .y1(function(d) {return yScale(d[1])});

    const currentLine = line(layers)
      .x(function(d) {return xScale(parsedTime(d.data.date))})
      .y(function(d) {return yScale(d[1])});

    const AxisDOM = (<g>
      <AxisBottom
        axis={xQAxis}
        transform={`translate(0, ${height - margin.top - margin.bottom})`} />
      <AxisLeft
        axis={yAxis}
        transform={`translate(-${margin.left}, 0)`} />
    </g>);

    const LayersDOM = (
      <g className="sales-investors-data">
        {/* AREAS */}
        {layers.map((layer, i) => <path key={i} fill={`url(#${layer.key})`} d={currentArea(layer)} />)}

        {/* LINES */}
        {layers.map((layer, i) => <path key={i} strokeWidth="2" stroke={cScale(layer.key)} d={currentLine(layer)} fill="transparent" />)}

        {/* DOTS */}
        {layers.map((layer) => layer.map((p,i) => <circle
          key={i}
          style={{stroke: cScale(layer.key)}}
          r={4}
          cx={xScale(parsedTime(p.data.date))}
          cy={yScale(p[1])}
          onMouseOver={this.handleMouseOver.bind(null, this.formatTooltip(p.data, layer.key))}
          onMouseOut={this.handleMouseOut} />))}

        {/* Gradients */}
        { keys.map((key, i) => <D3Gradent key={i} id={key} color={cScale(key)} />) }
      </g>
    );

    const SvgDOM = (
      <svg width={width}
        height={height}
        viewBox={`0 0 ${width} ${height}`}
        preserveAspectRatio="xMinYMid">
        <g transform={`translate(${margin.left}, ${margin.top})`}>
          { AxisDOM }
          { LayersDOM }
        </g>
      </svg>
    );

    this.setState({SvgDOM});
  }

  formatTooltip(data, key) {
    const parsedTime = timeParse('%m/%Y')(data.date);
    return `<div>
      <br />
      <h3>${timeFormat('%b %Y')(parsedTime)}</h3>
      <p>${data[key]} ${key}</p>
      <p>${data.new[key]} ${data.new[key] >= 0 ? 'new' : 'lost'}</p>
    </div>`
  }
}

export const PNASalesInvestorsChart = GraphHoc(PNASalesInvestorsChartComponent);


// const dummyData = [
//   {
//     investors: 45,
//     contacts: 300,
//     date: '1/2016',
//     new: {
//       investors: 14,
//       contacts: 45
//     }
//   }
// ];

// const stackKeys = ['contacts', 'investors']