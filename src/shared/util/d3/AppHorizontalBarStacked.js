import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { scaleBand, scaleLinear } from 'd3-scale';
import { max } from 'd3-array';
import { stack, stackOrderNone, stackOffsetNone } from 'd3-shape';

import { LayerHorizontal, GraphHoc } from './general';
import { colorScale, AxisLeft, AxisBottom, SvgGraph } from './AppHorizontalBar';

export class PNAHorizontalBarStackedComponent extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    barWidth: PropTypes.number,
    barInterspace: PropTypes.number,
    labelsBarWidth: PropTypes.number,
    colors: PropTypes.arrayOf(PropTypes.string)
  }
  static defaultProps = {
    width: 520,
    height: 250,
    barWidth: 18,
    barInterspace: 25,
    labelsBarWidth: 150,
    colors: ['#95D9EE', '#3DA3C0', '#FEA171', '#FD6E69', '#7585AD']
  }
  calculate(props) {
    const { data, width, height, labelsBarWidth, barWidth, barInterspace, colors } = props;
    
    const xScale = scaleLinear()
      .domain([0, max(data.map(d => d.sales.reduce((p,n) => p + n.value, 0)))])
      .range([0, width-labelsBarWidth]);

    const yScale = scaleBand()
      .domain(data.map(d => d.user.fullName))
      .rangeRound([0, (data.length * (barWidth+barInterspace))]);

    const cScale = colorScale(colors);

    const currentStack = stack()
      .keys(Array.from({length: max(data.map(d => d.sales.length))}, (v,i) => i))
      .value((d, key) => {
        return (d && d.sales && d.sales[key]) ? d.sales[key].value : ''
      })
      .order(stackOrderNone)
      .offset(stackOffsetNone);

    const layers = currentStack(data);

    const SvgDOM = (
      <div className="horizontal-bar-chart">
        <div className="main-holder" style={{width, height}}>
          <SvgGraph
            width={width-labelsBarWidth}
            height={data.length * (barWidth+barInterspace)}
            mainWrapStyles={{height, width: width-labelsBarWidth, paddingLeft: labelsBarWidth}}>
            { this.renderBars.call(this, xScale, yScale, cScale, layers, props) }
            <AxisLeft
              scale={yScale}
              maxWidth={labelsBarWidth-20}
              barInterspace={barInterspace} />
          </SvgGraph>
          <AxisBottom
            scaleValue={xScale}
            style={{marginLeft: labelsBarWidth}}
            width={width-labelsBarWidth}
            height={height} />
        </div>
      </div>
    );

    this.setState({SvgDOM});
  }
  renderBars(xScale, yScale, cScale, layers, props) {
    if (!xScale || !yScale || !cScale) return;
    const { data, barWidth } = props;

    return (<g>
      { layers.map((layer, key) => {
        const points = layer.map((p,i) => {
          const item = data[i];

          if (!item.sales[key]) return false;

          return({
            x: xScale(p[0]),
            y: yScale(item.user.fullName),
            height: barWidth,
            width: xScale(p[1]) - xScale(p[0]),
            onMouseOut: this.handleMouseOut,
            onMouseOver: this.handleMouseOver.bind(null, item.sales[key].name)
          });
        })
        .filter(i => i)

        if (!points) return '';

        const layerProps = {key, points, fill: cScale(key)};
        return <LayerHorizontal {...layerProps} />
      }) }
    </g>);
  }
}

export const PNAHorizontalBarStacked = GraphHoc(PNAHorizontalBarStackedComponent);