import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { select } from 'd3-selection';
import { scaleBand, scaleLinear, scaleOrdinal } from 'd3-scale';
import { axisBottom, axisRight } from 'd3-axis';
import { stack, stackOrderNone, stackOffsetNone } from 'd3-shape';
import { format } from 'd3-format';
import { timeParse } from 'd3-time-format';
import { max } from 'd3-array';

import { Axis, Layer, GraphHoc } from './general';

import './AppBarStacked.css';

export const AxisBottom = Axis(function() {
  if (!this.axisRef) return;
  const ref = select(this.axisRef);
  const transformation = '%m/%Y' === this.props.timeParseFormat ?
    'translate(15,30)rotate(90)' : 'translate(0,15)';

  ref
    .attr('class', 'axisBottom')
    .attr('transform', this.props.transform)
    .call(this.props.axis)
    .select('.domain').remove();

  ref
    .selectAll('.tick line').remove();

  ref
    .selectAll('.tick text')
      .attr('transform', transformation)
      .attr('fill', '#7F8FA4')
      .attr('style', 'text-transform: uppercase; font-size: 11px')
});
export const AxisLeft = Axis(function() {
  if (!this.axisRef) return;
  const ref = select(this.axisRef);

  this.props.axis.ticks(5)

  ref
    .attr('class', 'axisLeft')
    .attr('transform', this.props.transform)
    .call(this.props.axis)
    .select('.domain').remove();
  
  ref
    .selectAll('.tick text')
      .attr('x', 4)
      .attr('dy', -4)
      .attr('fill', '#7F8FA4');
  ref
    .selectAll('.tick line')
      .attr('stroke', '#4F596A')
      .attr('stroke-width', '0.5px');
});

export class PNABarChartStackedComponent extends Component {
    static propTypes = {
      width: PropTypes.number,
      height: PropTypes.number,
      stackData: PropTypes.array.isRequired,
      stackKeys: PropTypes.array.isRequired,

      timeParseFormat: PropTypes.string,

      barInterspace: PropTypes.number,
      margin: PropTypes.shape({top: PropTypes.number, right: PropTypes.number, bottom: PropTypes.number, left: PropTypes.number}),
      colors: PropTypes.arrayOf(PropTypes.string)
    }
    static defaultProps = {
      width: 800,
      height: 600,
      barInterspace: .6,
      margin: {top: 20, right: 10, bottom: 60, left: 50},
      timeParseFormat: '%m/%Y',
      colors: ['#7ED321', '#4A90E2', '#FCD056']
    }
  calculate(props) {
    const { width, height, margin, stackData, stackKeys, barInterspace, timeParseFormat, colors } = props;

    const svgWidth = width - margin.left - margin.right;
    const svgHeight = height - margin.top - margin.bottom;

    const xScale = scaleBand()
      .domain(stackData.map(d => d.dateFormat))
      .range([0,svgWidth]).padding([barInterspace]);
    const yScale = scaleLinear().range([svgHeight,0]);
    const color = scaleOrdinal(colors);
    const xAxis = axisBottom(xScale).tickFormat(function(d,i) {return stackData[i].dateFormat})
      //.tickFormat(d3.timeFormat('%b'));
    const yAxis = axisRight(yScale).tickFormat(format('$.2s')).tickSize(width);

    const currentStack = stack()
      .keys(stackKeys)
      .order(stackOrderNone)
      .offset(stackOffsetNone);

    const layers = currentStack(stackData);

    // domains
    xScale
      .domain(stackData.map(d => timeParse(timeParseFormat)(d.date)));
    yScale
      .domain([0, max(layers, layer => max(layer, d => d[1]))])
      .nice();

    const SvgDOM = (
      <svg width={width}
        height={height}
        viewBox={`0 0 ${width} ${height}`}
        preserveAspectRatio="xMinYMid">
        <g transform={`translate(${margin.left}, ${margin.top})`}>
          

          <AxisBottom 
            transform={`translate(0, ${height - margin.top - margin.bottom})`}
            timeParseFormat={timeParseFormat}
            axis={xAxis} />
          <AxisLeft
            axis={yAxis}
            transform={`translate(-${margin.left}, 0)`} />

          { layers.map((l,key) => {
            const points = this.createBarProps(l, xScale, yScale, props);
            if (!points) return null;

            const layerProps = {
              fill: color(key),
              points,
              key,
              onMouseOut: this.handleMouseOut,
              onMouseOver: this.handleMouseOver
            }
            return <Layer {...layerProps} />
          }) }
        </g>
      </svg>
    );
    this.setState({SvgDOM});
  }
  createBarProps(layerPoints, xScale, yScale, props) {
    const { stackData, timeParseFormat, stackKeys, colors } = props;
    
    return layerPoints.map((layer, index) => {
      const item = stackData[index];
      const formater = d => "$" + format(",.2f")(d);

      const valueRows = stackKeys.map((k,i) => `<tr><td style="color: ${colors[i]}">${k}</td><td>${formater(item[k])}</td></tr>`).reduce((p,n) => p+n, '');

      return ({
        x: xScale(timeParse(timeParseFormat)(item.date)),
        y: yScale(layer[1]),
        height: yScale(layer[0]) - yScale(layer[1]),
        width: xScale.bandwidth(),
        title: `
          <table>
            <thead>
              <tr><th colspan="2">${item.date}</th></tr>
            </thead>
            <tbody>
              ${valueRows}
            </tbody>
          </table>
        `
      })}
    );
  }
}

export const PNATotalSalesCommissionsGraph = GraphHoc(PNABarChartStackedComponent);
