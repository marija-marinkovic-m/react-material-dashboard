import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { PNABarChartStacked, commissionKeys, salesKeys, commissionsDataAnnual, commissionsDataQuarter, salesDataAnnual, salesDataQuarter } from './AppBarStacked';

class Widget extends Component {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    annual: PropTypes.bool,
    barInterspace: PropTypes.any
  }
  static defaultProps = {
    width: 800,
    height: 600,
    annual: false,
    barInterspace: .6
  }
  constructor(props) {
    super(props);
    this.state = Object.assign({}, props);
  } 
  handleInputChange = e => {
    const name = e.target.name;
    const value = parseFloat(e.target.value);
    this.setState({[name]: !value || value < 0 ? 1 : value});
  }
  toggleDataSource = () => this.setState({annual: !this.state.annual})
}

export class TotalCommissionsWidget extends Widget {
  render() {
    return (
      <div>
        <PNABarChartStacked
          timeParseFormat={!this.state.annual ? '%m-%m/%Y' : '%m/%Y'}
          width={this.state.width}
          height={this.state.height}
          stackData={!this.state.annual ? commissionsDataQuarter : commissionsDataAnnual}
          stackKeys={commissionKeys}
          barInterspace={this.state.barInterspace} />

          <p>Graph width <code>(width)</code>: <input type="number" value={this.state.width} onChange={this.handleInputChange} name="width" /></p>

          <p>Graph height <code>(height)</code>: <input type="number" value={this.state.height} onChange={this.handleInputChange} name="height" /></p>

          <p>Bar width <code>(barInterspace)</code>: <input type="number" value={this.state.barInterspace} onChange={this.handleInputChange} name="barInterspace" /> <small>(range 0.1 - 0.9)</small></p>

          <p>Data switch (Quarters Annual) <code>(stackData)</code>: <input type="checkbox" checked={this.state.annual} onChange={this.toggleDataSource} /></p>
      </div>
    );
  }
}

export class TotalSalesWidget extends Widget {
  render() {
    return (
      <div>
        <PNABarChartStacked
          timeParseFormat={!this.state.annual ? '%m-%m/%Y' : '%m/%Y'}
          width={this.state.width}
          height={this.state.height}
          stackData={!this.state.annual ? salesDataQuarter : salesDataAnnual}
          stackKeys={salesKeys}
          barInterspace={this.state.barInterspace}
          colors={['#7ED321', '#FCD056']} />

        <p>Graph width <code>(width)</code>: <input type="number" value={this.state.width} onChange={this.handleInputChange} name="width" /></p>

        <p>Graph height <code>(height)</code>: <input type="number" value={this.state.height} onChange={this.handleInputChange} name="height" /></p>

        <p>Bar width <code>(barInterspace)</code>: <input type="number" value={this.state.barInterspace} onChange={this.handleInputChange} name="barInterspace" /> <small>(range 0.1 - 0.9)</small></p>

        <p>Data switch (Quarters Annual) <code>(stackData)</code>: <input type="checkbox" checked={this.state.annual} onChange={this.toggleDataSource} /></p>
      </div>
    );
  }
}