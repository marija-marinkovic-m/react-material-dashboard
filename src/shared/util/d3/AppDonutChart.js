import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { scaleOrdinal } from 'd3-scale';
import { arc, pie } from 'd3-shape';

import { GraphHoc } from './general';
import './AppDonutChart.css';

export class DonutChartComponent extends Component {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    data: PropTypes.array
  }
  static defaultProps = {
    width: 800,
    height: 600,
    data: [
      { label: 'New Contact', value: 35, color: '#BFCCDC' },
      { label: 'Contacting', value: 340, color: '#4A90E2' },
      { label: 'Nurture', value: 125, color: '#FBC738' },
      { label: 'Interested', value: 52, color: '#7ED321' },
      { label: 'Not Interested', value: 74, color: '#F25967' }
    ]
  }

  calculate(props) {
    const { width, height, data } = props;

    const totalContacts = data.reduce((p,n) => p + n.value, 0);

    const radius = Math.min(width, height) / 2;
    const cScale = scaleOrdinal().range(data.map(d => d.color));
    const pScale = scaleOrdinal().range(data.map(d => Math.round(d.value*100 / totalContacts)));
    const currentArc = arc()
      .outerRadius(radius - 10)
      .innerRadius(radius - 35);
    const currentPie = pie()
      .sort(null)
      .value(function(d) {return d.value});

    const pieData = currentPie(data);

    const SvgDOM = (<svg width={width} height={height}>
      <g className="arc" transform={`translate(${width/2}, ${height/2})`}>
         { pieData.map((item,i) => (<g className="arc" key={i}>
            <path
              d={currentArc(item)}
              fill={cScale(i)}
              stroke={cScale(i)}
              onMouseOver={this.handleMouseOver.bind(null, this.formatedTooltip(item, pScale(i)))}
              onMouseOut={this.handleMouseOut} />
          </g>)) } 
      </g>
    </svg>);

    this.setState({SvgDOM});
  }

  formatedTooltip = (item, percent) => `<table class="donut-tooltip"><tbody><tr>
    <td>
      <h5 style="color: ${item.data.color}">${item.data.label}</h5>
      <p>${item.value} Contacts</p>
    </td>
    <td>
      <span class="percent">${percent} %</span>
    </td>
  </tr></tbody></table>`;

}

export const PNAContactStatusDonutChart = GraphHoc(DonutChartComponent)