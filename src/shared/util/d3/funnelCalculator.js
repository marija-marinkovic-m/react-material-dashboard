import { max } from 'd3-array';

export default class FunnelCoordinatesCalculator {
  size = [100, 100];
  data = null;

  constructor({data, size}) {
    const coordinates = this
      .setSize(size)
      .setData(data)
      .setPercentages()
      .getCoordinates();

    if (!coordinates) return data;

    return data
      .map((item, index) => Object.assign({}, item, {
        coordinates: coordinates[index]
      }));
  }

  setSize = (s) => {
    if (s && typeof s === 'object' && s.length === 2) {
      this.size = s;
    }
    return this;
  }

  setData = (data) => {
    // some data validation should occur here
    if (data && data.length) {
      this.data = data
        .sort((a, b) => b.value - a.value);
    }
    return this;
  }

  setPercentages = () => {
    if (this.data) {
      const maxHeight = max(this.data, function(d) {return d.value});
      const p = this.data
        .map(d => Math.round(d.value/maxHeight*100))
        .sort((a, b) => b - a);
      this.percentages = [...p.slice(1,p.length), p[p.length-1]];
    }
    return this;
  }

  getCoordinates = () => {
    if (!this.data) return null;

    const width = this.size[0];
    const height = this.size[1];

    const rectWidth = width/this.data.length;
    const normalizePercentage = index => this.percentages[index] || 5;

    const rbY = (p,h) => p === 100 ? h : p+((h-p)/2);
    const rtY = (p,h) => p === 100 ? 0 : (h-p)/2;

    return this.data.map((d, i) => {
      const cp = normalizePercentage(i);
      const lbY = i === 0 ? height : rbY(normalizePercentage(i-1), height);
      const ltY = i === 0 ? 0 : rtY(normalizePercentage(i-1), height);

      const values = [
        {x: rectWidth * i + 2, y: lbY},
        {x: rectWidth * (i + 1) - 2, y: rbY(cp, height)},
        {x: rectWidth * (i+1) - 2, y: rtY(cp, height)},
        {x: rectWidth * i + 2, y: ltY},
      ];
      return ({values, percentage: this.percentages[i]});
    });
  }
}