import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ProfileAvatarItem from '../components/ProfileAvatarTableListItem';

import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import { Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import {ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import Checkbox from 'material-ui/Checkbox';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import ChipInput from '../util/ChipInput';
import Popover from 'material-ui/Popover';

import CircularProgress from 'material-ui/CircularProgress';
import LinearProgress from 'material-ui/LinearProgress';

// icons
import ToggleStar from 'material-ui/svg-icons/toggle/star';
import StatusCircle from '../util/svg-icons/statusCircle';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import WarningIcon from 'material-ui/svg-icons/alert/warning';

import { themeColors, themeInputProps } from '../../theme.config';

import './Sales.css';

class DealsToolbar extends Component {
  static propTypes = {
    currentDealType: PropTypes.string.isRequired,
    settingsOpen: PropTypes.bool,
    handleFilterDeals: PropTypes.func.isRequired,
    handleSettingsDrawerToggle: PropTypes.func.isRequired,
    toolbarTotalLabel: PropTypes.string
  }
  static defaultProps = {
    toolbarTotalLabel: ''
  }
  render() {
    const { currentDealType, settingsOpen, handleFilterDeals, handleSettingsDrawerToggle, toolbarTotalLabel } = this.props;
    return (
      <Toolbar
        style={styles.toolbar}>
        <ToolbarGroup firstChild={true}>
          <DropDownMenu
            iconButton={<NavigationArrowDropDown color="#fff" />}
            iconStyle={{left: '16px', right: 'auto'}}
            labelStyle={{paddingRight: '24px', paddingLeft: '56px', color: '#fff', fontSize: '10px', textTransform: 'uppercase'}}
            value={currentDealType}
            onChange={handleFilterDeals}>
            {
              filteringData.dealTypes.map((m,i) => {
                const curr = m === currentDealType;
                const itemProps = {
                  value: m,
                  primaryText: m,
                  key: i,
                  insetChildren: !curr,
                  checked: curr
                };
                return <MenuItem {...itemProps} />;
              })
            }
          </DropDownMenu>
          { toolbarTotalLabel.length > 0 && <ToolbarSeparator style={{backgroundColor: themeColors.mediumGray, height:'12px'}} /> }
          { toolbarTotalLabel.length > 0 && <ToolbarTitle style={{marginLeft: '20px', color: themeColors.mediumGray, fontSize: '10px', lineHeight: '35px'}} text={toolbarTotalLabel} /> }
        </ToolbarGroup>
        <ToolbarGroup>
          <FontIcon
            className="pna icon-filter_settings"
            color={settingsOpen ? themeColors.gold : 'white'}
            style={{fontSize: '17px'}}
            onTouchTap={handleSettingsDrawerToggle} />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

class DealsDrawer extends Component {
  static propTypes = {
    drawerStyle: PropTypes.object.isRequired,
    drawerWidth: PropTypes.number.isRequired,
    settingsOpen: PropTypes.bool,
    mobileCloseBtn: PropTypes.node,
    currentFilterAmount: PropTypes.string
  }

  render() {
    const { drawerStyle, drawerWidth, settingsOpen, currentFilterAmount, mobileCloseBtn } = this.props;

    return(
      <Drawer
        width={drawerWidth}
        docked={true}
        openSecondary={true}
        open={settingsOpen}
        className="filter-drawer"
        containerStyle={drawerStyle}>

        { mobileCloseBtn }

        <a href="#reset" className="filters-reset">Reset</a>

        <h4 className="filters-title">Filter By</h4>

        <div className="widget">
          <Checkbox
            {...themeInputProps.checkbox}
            label={<span>Favorites&nbsp;<ToggleStar color={themeColors.gold} style={{marginLeft: '10px', display: 'inline-block', verticalAlign: 'middle', top: '-2px', position: 'relative'}} /></span>} />
        </div>

        <div className="widget">
          <h5>Status</h5>
          { filteringData.statuses.map((s,i) => <Checkbox
            {...Object.assign({}, themeInputProps.checkbox, {labelStyle: {fontSize: '10px', textTransform: 'uppercase', letterSpacing: '1px', color: '#FFFFFF', lineHeight: '20px'}})}
            key={i}
            label={<span><Avatar size={15} style={{marginRight: '15px', display: 'inline-block', verticalAlign: 'middle', position: 'relative', top: '-2px'}} backgroundColor={s.color} />{s.label}</span>} />) }
        </div>

        <div className="widget">
          <h5>Amount</h5>
          <DropDownMenu
            value={currentFilterAmount}
            autoWidth={false}
            menuStyle={{width: '100%'}}
            style={{width: '100%', marginBottom: '20px'}}
            labelStyle={{color: '#7F8FA4', paddingLeft: 0}}
            iconStyle={{right: '-5px', color: '#7F8FA4'}}
            underlineStyle={{margin: '-1px 0px', borderTopColor: '#2D3B4C'}}
            onChange={this.handleFilterAmounts}>
            { filteringData.amounts.map((a,i) => {
              const curr = a === currentFilterAmount;
              const itemProps = {
                value: a,
                primaryText: a,
                key: i,
                insetChildren: !curr,
                checked: curr
              };
              return <MenuItem {...itemProps} />;
            }) }
          </DropDownMenu>

          <RadioButtonGroup name="amount" defaultSelected={0}>
            { filteringData.amountDirection.map((a,i) => <RadioButton
              {...themeInputProps.radio}
              style={{display: 'inline-block', width: 'auto', marginRight: '17px'}}
              name="amountDirection"
              value={i}
              key={i}
              label={a} />) }
          </RadioButtonGroup>
        </div>
        

        <div className="widget">
          <h5>Offerings</h5>
          { filteringData.offerings.map((o,i) => <Checkbox
            {...Object.assign({}, themeInputProps.checkbox, {fontWeight: 'normal', fontSize: '12px', letterSpacing: '0px'})}
            key={i}
            label={o} />) }
        </div>

        <div className="widget">
          <h5>Tags</h5>
          <ChipInput
            defaultValue={filteringData.tags}
            fullWidth={true} />
        </div>

      </Drawer>
    );
  }
}

class DealsTable extends Component {
  static propTypes = {
    deals: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired,
    mainWrapStyle: PropTypes.object,
    screenWidth: PropTypes.number
  }

  static defaultProps = {
    mainWrapStyle: {}
  }

  openContact = (slug, rowNumber, aSlug) => {
    let contactSlug = typeof slug === 'boolean' ? aSlug : slug;
    this.props.history.push(`/contacts/${contactSlug}`);
  }

  render() {
    const {deals, mainWrapStyle} = this.props;
    return (
      <div style={mainWrapStyle}>
      <Table style={{width: 'auto', minWidth: '100%', color: 'blue'}} bodyStyle={{overflowX: 'auto', color: 'red'}} >
        <TableBody displayRowCheckbox={false} showRowHover={false}>
          <TableRow className="head-row">
            <TableRowColumn style={{paddingLeft: '100px'}}>
              <ListItem style={styles.headRowItem} disabled={true} primaryText="Contact" />
            </TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem}>Subscription <br/>Date</ListItem></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Document Status" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Offering" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Amount" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText={<span>Funds<br /> Received</span>} /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Commission" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText={<span>Commission<br />Payment Date</span>} /></TableRowColumn>
          </TableRow>

          { deals.map((deal,i) => {
            const rowNumber = i+1;

            return (
              <TableRow key={i} className="pna-rows">
                <TableRowColumn>
                  <ProfileAvatarItem
                    hovered={true}
                    screenWidth={this.props.screenWidth}
                    contact={deal}
                    rowNumber={rowNumber}
                    handleAvatarClick={this.openContact}
                    openContact={this.openContact} />
                </TableRowColumn>
                <TableRowColumn><ListItem style={{fontSize: '12px'}} disabled={true} primaryText={deal.subscriptionDate} /></TableRowColumn>
                <TableRowColumn>
                  <SaleProgressBar deal={deal} />
                </TableRowColumn>
                <TableRowColumn>
                  <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={deal.offering} />
                </TableRowColumn>
                <TableRowColumn>
                  <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={`$ ${deal.amount}`} />
                </TableRowColumn>
                <TableRowColumn>
                  <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={`$ ${deal.fundsReceived}`} />
                </TableRowColumn>
                <TableRowColumn>
                  <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={`$ ${deal.commission}`} />
                </TableRowColumn>
                <TableRowColumn>
                    { renderCommissionStatus(deal) }
                </TableRowColumn>
              </TableRow>
            );
          }) }

        </TableBody>
      </Table>
      </div>
    );
  }
}

export class SaleProgressBar extends Component {
  state = {
    isOpenPopover: false,
    anchorEl: null
  }
  static propTypes = {
    deal: PropTypes.object.isRequired
  }

  handleRequestClose = () => this.setState({isOpenPopover: false})
  handleMouseOver = (event) => {
    console.log('mosuse over');
    if (this.state.isOpenPopover) return;
    this.setState({isOpenPopover: true, anchorEl: event.currentTarget})
  }

  render () {
    const { deal } = this.props;
    const progressBarColor = deal.documentStatus < 50 ? deal.documentStatus < 35 ? themeColors.red : themeColors.orange : themeColors.green;
    return (
      <div className="timeline-cell-data">
        <ListItem
          disabled={true}
          onMouseOver={this.handleMouseOver}
          onMouseLeave={this.handleRequestClose}>
          <LinearProgress
            mode="determinate"
            value={deal.documentStatus}
            color={progressBarColor}
            style={{backgroundColor: '#DDE1E7', borderRadius: '3px'}} />
          { progressBarColor === themeColors.red && 
            <WarningIcon
              color={progressBarColor}
              style={{position: 'absolute', right: '-12px', top: '5px', width: '18px'}} /> }
        </ListItem>

        { progressBarColor === themeColors.red && <Popover
          open={this.state.isOpenPopover}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}>
          <div className="timeline-activity">
            <h4><i className="icon"><WarningIcon color={progressBarColor} /></i>Fact And Truth</h4>
            <p>Cambridge amico dit me que Occidental es.Li Europan lingues es membres del sam familie.</p>
          </div>
        </Popover> }
      </div>
    );
  }
}

export default class SalesScreenBlockComponent extends Component {
  static propTypes = {
    apiFetchPropertyName: PropTypes.string,
    toolbarTotalLabel: PropTypes.string,
    tableWrapStyle: PropTypes.object,

    // redux / router
    mediaQueries: PropTypes.object.isRequired,
    fetchData: PropTypes.func.isRequired,
    apiState: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  static defaultProps = {
    apiFetchPropertyName: 'salesFetch',
    toolbarTotalLabel: '',
    tableWrapStyle: null
  }

  constructor(props) {
    super(props);
    this.state = {
      dealsType: 'All Sales',
      filterAmount: '$100.000',
      settingsOpen: false
    }
  }

  handleFilterDeals = (event, index, dealsType) => this.setState({dealsType});
  handleFilterAmounts = (event, index, filterAmount) => this.setState({filterAmount});
  handleSettingsDrawerToggle = e => {
    if (e) e.preventDefault();
    this.setState({settingsOpen: !this.state.settingsOpen})
    if (this.props.mediaQueries.screenWidth <= 1440) {
      this.props.updateSideNav({expanded: false});
    }
  };

  render() {
    const { apiState, mediaQueries, history, apiFetchPropertyName, toolbarTotalLabel, tableWrapStyle } = this.props;
    const isMobileView = mediaQueries.screenWidth <= 768;

    // if screen width <= 768, apply handheld styles
    const mainWrapperStyle = Object.assign({}, styles.mainWrapper, {
      paddingRight: this.state.settingsOpen && !isMobileView ? '240px' : 0,
    });
    const drawerStyle = Object.assign({}, styles.filterDrawer, {
      position: isMobileView ? 'fixed' : 'absolute',
      padding: isMobileView ? '40px' : '40px 20px'
    });
    const drawerWidth = isMobileView ? '100%' : 240;

    const salesFetch = apiState && apiState[apiFetchPropertyName];

    return (
      <div>
        <DealsToolbar
          currentDealType={this.state.dealsType}
          settingsOpen={this.state.settingsOpen}
          handleFilterDeals={this.handleFilterDeals}
          handleSettingsDrawerToggle={this.handleSettingsDrawerToggle}
          toolbarTotalLabel={salesFetch && salesFetch.response && salesFetch.response.data ? `${salesFetch.response.data.length} ${toolbarTotalLabel}` : ''} />

        <div className="contacts-listing-wrapper" style={mainWrapperStyle}>

          <DealsDrawer
            drawerStyle={drawerStyle}
            drawerWidth={drawerWidth}
            settingsOpen={this.state.settingsOpen}
            mobileCloseBtn={isMobileView && <a href="#done" onClick={e => {e.preventDefault(); this.handleSettingsDrawerToggle()}}>Done</a>}
            currentFilterAmount={this.state.filterAmount} />

          { salesFetch && salesFetch.isFetching && <CircularProgress size={80} thickness={5} /> }
          { salesFetch && salesFetch.response && salesFetch.response.data && <DealsTable screenWidth={mediaQueries.screenWidth} deals={salesFetch.response.data} history={history} mainWrapStyle={tableWrapStyle} /> }
        </div>
      </div>
    );
  }
}

export const renderCommissionStatus = (deal, color = 'rgba(0, 0, 0, 0.87)') => {
  const currStatus = commissionStatuses.hasOwnProperty(deal.commissionStatus) ? commissionStatuses[deal.commissionStatus] : {label: '', color: themeColors.lightGray};
  const circleIconStyle = {
    marginTop: ['overdue', 'paid'].indexOf(deal.commissionStatus) > -1 ? '-13px' : '-8px', marginLeft: '5px',
    width: '36px', height: '36px'
  }
  let props = {
    style: {paddingLeft: '55px', color, paddingTop: 0, paddingBottom: 0},
    disabled: true,
    leftIcon: <StatusCircle color={currStatus.color} style={circleIconStyle} />,
    primaryText: <h4 className="uppercase" style={{fontSize: '10px', fontWeight: 900, color: 'overdue' === deal.commissionStatus ? 'red' : 'inherit'}}>{['paid', 'pending', 'lost'].indexOf(deal.commissionStatus) > -1 ? currStatus.label : deal.commissionPaymentDate}</h4>
  }
  
  if (['paid', 'overdue'].indexOf(deal.commissionStatus) > -1) {
    props.secondaryText = (<span style={{fontSize: '10px', color: '#7F8FA4'}}>
      { deal.commissionStatus === 'overdue' ?  currStatus.label : deal.commissionPaymentDate }
    </span>);
  }
  

  return <ListItem {...props} />
};

const filteringData = {
  dealTypes: ['All Sales', 'Pending', 'Paid', 'Failed'],
  amounts: ['$100.000', '$1.000.000', '$10.000.000'],
  amountDirection: ['More than', 'Less than'],
  offerings: ['Park Avenue South', 'Nomad', 'Corporate Band', '84 William', 'AKA UN Recap', '17 John', '46th Street', 'AKA UN', 'AKA Wall Street Recap', '25th Street'],
  tags: ['urgent', 'family'],
  statuses: [
    {
      label: 'Contacting',
      color: themeColors.statusBlue
    },
    {
      label: 'Nurture',
      color: themeColors.statusGold
    },
    {
      label: 'Interested',
      color: themeColors.green
    },
    {
      label: 'Not interested',
      color: themeColors.statusRed
    }
  ]
};

export const commissionStatuses = {
  paid: {label: 'Paid', color: themeColors.green},
  pending: {label: 'Pending', color: themeColors.gold},
  scheduled: {label: '', color: themeColors.blue},
  overdue: {label: 'Date Overdue', color: themeColors.blue},
  lost: {label: 'Lost', color: themeColors.red}
}

const styles = {
  toolbar: {color: '#fff', fontSize: '14px', height: '50px'},
  mainWrapper: {position: 'relative'},
  filterDrawer: {
    zIndex: 8999,
    top: 0, right: 0,
    backgroundColor: themeColors.contactFiltersDrawerBg,
    color: '#fff',
    overflowX: 'hidden'
  },
  headRowItem: {
    paddingTop: '7px', paddingBottom: '7px',
    textTransform: 'uppercase', color: "#7F8FA4", lineHeight: '18px', fontSize: '10px', letterSpacing: '1px'
  }
}