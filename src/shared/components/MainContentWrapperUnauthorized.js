import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { noAuthRoutes } from '../../routes';

import bgrImg from '../assets/images/dashboard-bgr.png';

import './MainContentWrapperUnauthorized.css';

class MainContentWrapper extends Component {
  static PropTypes = {
    mediaQueries: PropTypes.object
  }
  render() {
    return <div className="no-auth-wrap" style={{backgroundImage: `url(${bgrImg})`}}>{noAuthRoutes}</div>
  }
}

export default withRouter(connect(state => ({
  mediaQueries: state.mediaQueryTracker
}))(MainContentWrapper));