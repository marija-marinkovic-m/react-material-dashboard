import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Avatar from 'material-ui/Avatar';

import { themeColors } from '../../theme.config';

export default class ProfileAvatar extends Component {
  static propTypes = {
    userData: PropTypes.object,
    avatarProps: PropTypes.object
  }
  defaultAvatarProps = {
    size: 86,
    color: themeColors.lightGray,
    backgroundColor: themeColors.gray,
    style: {boxShadow: '0 8px 16px 0 rgba(34,43,58,0.5)', fontFamily: 'Roboto, Arial, Helvetica, sans-serif'}
  }

  render() {
    const { userData, avatarProps } = this.props;

    if (!userData) return '';

    let props = Object.assign({}, this.defaultAvatarProps, avatarProps);
    if (userData.avatarUrl && userData.avatarUrl !== '') {
      props.src = process.env.PUBLIC_URL + '/' + userData.avatarUrl;
    } else {
      props.children = userData.firstName.slice(0,1) + userData.lastName.slice(0,1);
    }

    return <Avatar {...props} />
  }
}