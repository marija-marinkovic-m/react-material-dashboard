import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { triggeredSearch } from '../../redux/actions';

import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import CancelIcon from 'material-ui/svg-icons/navigation/cancel';

import { EscButton } from '../screens/dashboard/SearchResults';

import { themeColors } from '../../theme.config';

import './SearchForm.css';

export const isSearchPage = (route) => route && route.pathname && route.pathname.indexOf('/search/') === 0 && route.state;

class SearchForm extends Component {
  static propTypes = {
    mediaQueries: PropTypes.object,
    route: PropTypes.object,
    history: PropTypes.object,
    style: PropTypes.object
  }

  static defaultProps = {
    style: {}
  }

  constructor(props) {
    super(props);
    let term = '';
    let isInputActive = false;
    if (isSearchPage(props.route)) {
      term = props.route.state;
      isInputActive = true;
      props.triggeredSearch(false);
    }
    this.state = {
      isInputActive,
      term
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.route.state && !nextProps.route.state) {
      this.setState({isInputActive: false, term: ''});
    }
    if (nextState.isInputActive !== this.state.isInputActive) {
      this.props.triggeredSearch(nextState.isInputActive);
    }
  }

  handleButtonClick = e => {
    e.preventDefault();
    if (this.state.isInputActive) {
      // submit the form if field not empty or close if field empty
      const term = encodeURIComponent(this.state.term.trim());
      if (term !== '') {
        this.props.triggeredSearch(false);
        return this.props.history.push(`/search/${term}`, term);
      }

      this.textInput.blur();
      
      return this.setState({isInputActive: false});
    }
    this.setState({isInputActive: true}, () => this.textInput.focus());
  }
  handleInputChage = e => {
    this.setState({term: e.target.value})
  }

  handleInputKeyDown = e => {
    const event = e || window.event;
    if (event.keyCode === 27) {
      this.handleCancel();
    }
  }

  handleInputOnBlur = e => {
    if (this.state.term === '') {
      this.setState({isInputActive: false});
    }
  }

  handleOpenSearchBar = () => {
    if (this.state.isInputActive) return;
    this.setState({isInputActive: true}, () => this.textInput.focus());
  }
  handleCancelButtonClick = () => {
    if (this.state.term !== '') {
      this.setState({term: ''})
    } else {
      this.handleCancel();
    }
  }

  handleCancel = () => {
    this.setState({term: ''}, () => {

      if (this.props.route.pathname.indexOf('/search/') === 0) {
        this.props.history.goBack();
      }
      this.setState({isInputActive: false})
    })
  }

  render() {
    const {mediaQueries, isTriggeredSearch, style} = this.props;
    const { isInputActive } = this.state;

    const isMobileView = mediaQueries.screenWidth <= 1280;
    const iconColor = themeColors[isMobileView ? isTriggeredSearch ? 'blue' : 'white' : isInputActive ? 'blue' : 'darkGray'];

    return (
      <div className="search-form-bar" style={Object.assign({}, {width: isTriggeredSearch && isMobileView ? '100vw' : 'inherit', color: isTriggeredSearch ? themeColors.darkGray : 'inherit', marginLeft: isMobileView && isTriggeredSearch ? '-48px' : 'auto'}, style)} onClick={this.handleOpenSearchBar}>
        <IconButton
          onClick={this.handleButtonClick}
          iconStyle={{width: 24, height: 24}}
          style={{width: 48, height: 48, padding: 0}}>
          <FontIcon className="pna icon-search_alt" color={iconColor} fill={iconColor} />
        </IconButton>
        { 
          isInputActive &&
          <form className="search-input-form" onSubmit={this.handleButtonClick}>
            <input
              type="text"
              value={this.state.term}
              onChange={this.handleInputChage}
              onBlur={this.handleInputOnBlur}
              onKeyDown={this.handleInputKeyDown}
              style={isMobileView && isTriggeredSearch ? {color: '#000'} : {color: 'inherit'}}
              ref={input => this.textInput = input} />
          </form>
        }

        { isMobileView && isInputActive && <IconButton
          onClick={this.handleCancelButtonClick}
          iconStyle={{width: 18, height: 18}}
          style={{position: 'absolute', top: 8, right: 25, zIndex: 999, width: 48, height: 48, padding: 0}}>
          <CancelIcon color={themeColors.lightGray} fill={themeColors.lightGray} />
        </IconButton> }


        { !isMobileView && isInputActive && <EscButton onTouchTap={this.handleCancel} /> }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  route: state.router.location,
  isTriggeredSearch: state.searchController.triggered
});
const mapDispatchToProps = dispatch => ({
  triggeredSearch: bindActionCreators(triggeredSearch, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchForm))