import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import ToggleStar from 'material-ui/svg-icons/toggle/star';
import InvestorIcon from '../util/svg-icons/investor';

import { themeColors } from '../../theme.config';

export const getProfileAvatarProps = (profile) =>  {
   let props = {
    size: 37
  };

  if (profile.avatarUrl && profile.avatarUrl !== '') {
    props.src = process.env.PUBLIC_URL + '/' + profile.avatarUrl;
  } else {
    props.children = profile.firstName.substr(0,1) + profile.lastName.substr(0,1);
  }
  return props;
};

export default class extends Component {
  static propTypes = {
    contact: PropTypes.object.isRequired,
    rowNumber: PropTypes.number,
    placeholderStyle: PropTypes.object,
    handleAvatarClick: PropTypes.func.isRequired,
    onToggleFeatured: PropTypes.func,
    openContact: PropTypes.func.isRequired,
    hovered: PropTypes.bool,
    screenWidth: PropTypes.number
  }
  defaultProps = {
    placeholderStyle: {},
    rowNumber: 0,
    hovered: false,
    screenWidth: 0
  }

  constructor(props) {
    super(props);
    this.state = {
      featured: props.contact.featured
    }
  }

  handleToggleFavorite = () => {
    this.setState({featured: !this.state.featured})
    if (this.props.onToggleFeatured) {
      this.props.onToggleFeatured();
    }
  }

  renderToggleStar() {
    const isMobileView = this.props.screenWidth <= 768;
    return (
      <IconButton
        onClick={this.handleToggleFavorite}
        style={styles.toggleStar}>
        <ToggleStar
          className="contact-favorite-indicator"
          color={themeColors[this.state.featured ? 'gold' : this.props.hovered || isMobileView ? 'lightestGray' : 'white']} 
          style={styles.toggleStar} />
      </IconButton>
    )
  }
  renderAvatar() {
    const { handleAvatarClick, rowNumber, contact } = this.props;
    const avatarProps = getProfileAvatarProps(contact);
    return (
      <Avatar
        className="avatar-tag"
        onClick={handleAvatarClick.bind(null, true, rowNumber, contact.slug)}
        {...avatarProps} />
    );
  }
  renderPrimaryText() {
    const { contact, openContact } = this.props;
    const linkStyle = Object.assign({}, styles.primaryText, {
      bottom: contact.type === 'investor' ? 0 : '-7px'
    });
    return(
      <span className="link-el"
        onClick={openContact.bind(null, contact.slug)}
        style={linkStyle}>
        <h3>{ `${contact.firstName} ${contact.lastName}` }</h3>
      </span>
    );
  }
  renderSecondaryText() {
    const { contact, openContact } = this.props;
    const props = {
      className: 'd-flex link-el',
      onClick: openContact.bind(null, contact.slug),
      style: {marginTop: 0}
    };
    return (
      <div {...props}>
        {
          contact.type === 'investor' &&
          <InvestorIcon
            color={themeColors.blue} />
        }
        <span className="secondary-text">
          {
            contact.type === 'investor' ?
            'INVESTOR' :
            <span>&nbsp;</span>
          }
        </span>
      </div>
    );
  }

  render () {
    const mainStyle = Object.assign(
      {},
      styles.placeholderStyle, 
      this.props.placeholderStyle
    );
    return (
      <div style={mainStyle}>
        { this.renderToggleStar() }
        <ListItem
          className="avatarItem"
          disabled={true}
          leftAvatar={this.renderAvatar()}
          primaryText={this.renderPrimaryText()}
          secondaryText={this.renderSecondaryText()} />
      </div>
    );
  }
}

const styles = {
  toggleStar: {
    position: 'absolute', zIndex: 15, top: '50%', left: '-12px',
    marginTop: '-24px'
  },
  primaryText: {
    position: 'relative'
  },
  placeholderStyle: {
    position: 'relative', zIndex: 10,
    paddingLeft: '25px',
    paddingBottom: '10px', paddingTop: '10px' // these are responsible for the whole row height
  }
}