import React from 'react';

import FontIcon from 'material-ui/FontIcon';

export default () => (
  <footer>
    <FontIcon className="pna icon-prodigy-associates-logo" color="#7F8FA4" />
    <p>For any questions or problems please email us at: <br />
      <a target="_blank" rel="noopener noreferrer" href="mailto:support@prodigynetwork.com">support@prodigynetwork.com</a>
    </p>
  </footer>
);