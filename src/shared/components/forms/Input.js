// customized version of http://www.material-ui.com/#/components/text-field
import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

export default class extends Component {
  render() {
    return (
      <div>
        <TextField {...this.props} />
      </div>
    );
  }
}