import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import routes from '../../routes';

import cn from 'classnames';

import './MainContentWrapper.css';

class MainContentWrapper extends Component {
  static propTypes = {
    mediaQueries: PropTypes.object,
    expanded: PropTypes.bool.isRequired
  }
  render() {
    const { expanded, mediaQueries } = this.props;
    const isMobileView = mediaQueries.screenWidth <= 1280;
    const elementProps = {
      className: cn('app-content', {expanded}),
      style: Object.assign({}, appContentStyle, {
        paddingLeft: expanded ? '255px' : isMobileView ? 0 : '73px'
      })
    }

    return(
      <div {...elementProps}>
        {routes}
      </div>
    );
  }
}

export default withRouter(connect(state => ({
  mediaQueries: state.mediaQueryTracker,
  expanded: state.sideNavController.expanded
}))(MainContentWrapper));

const appContentStyle = {
  paddingRight: 0,
  paddingTop: '64px'
};