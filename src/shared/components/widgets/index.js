import ContactsActivityTable from  './ContactsActivityTable';
import ContactsInvestors from './ContactsInvestors';
import ContactsStatus from './ContactsStatus';
import OpportunitiesPerOffering from './OpportunitiesPerOffering';
import PendingDocuments from './PendingDocuments';
import PendingFunds from './PendingFunds';
import RecentOpportunitiesTable from './RecentOpportunitiesTable';
import SalesActivityTable from './SalesActivityTable';
import SalesFunnel from './SalesFunnel';
import SalesPerInvestor from './SalesPerInvestor';
import SalesPerOffering from './SalesPerOffering';
import { TotalCommissionsWidget, TotalSalesWidget } from './TotalSalesCommissions';
import ContactsActivityBars from './ContactsActivityBars';
import TheMostActiveContactWidget from './TheMostActiveContact';


export { ContactsActivityTable, ContactsInvestors, ContactsStatus, OpportunitiesPerOffering, PendingDocuments, PendingFunds, RecentOpportunitiesTable, SalesActivityTable, SalesFunnel, SalesPerInvestor, SalesPerOffering, TotalCommissionsWidget, TotalSalesWidget, ContactsActivityBars, TheMostActiveContactWidget }