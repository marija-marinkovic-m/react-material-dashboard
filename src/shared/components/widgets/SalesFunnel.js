import React from 'react';
import PropTypes from 'prop-types';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import Funnel from '../../util/d3/Funnel';

import Base from './BaseWidget';

import { dashboardWidgetDropdownProps } from '../../../theme.config';

export default class extends Base {
  state = {
    width: 300,
    data: null,
    currentPeriod: 'all'
  }
  static propTypes = {
    screenWidth: PropTypes.number,
    data: PropTypes.array.isRequired,
    period: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func.isRequired
  }
  componentWillMount() {
    this.setState({data: this.props.data});
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(this.props.data) !== JSON.stringify(nextProps.data)) {
      this.setState({data: nextProps.data});
    }
    if (nextProps.period !== this.props.period) {
      this.setState({currentPeriod: nextProps.period});
    }
  }
  handlePeriodChange = (event, key, value) => {
    this.props.onChange(value);
  }
  render() {
    const { data, width, currentPeriod } = this.state;
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Sales Funnel</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
            <DropDownMenu
              value={currentPeriod}
              onChange={this.handlePeriodChange}
              {...dashboardWidgetDropdownProps}>
                <MenuItem value={2014} primaryText="2014" />
                <MenuItem value="2015" primaryText="2015" />
                <MenuItem value="2016" primaryText="2016" />
                <MenuItem value="all" primaryText="All time" />
              </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>

        <div style={{margin: '30px 0'}}>
          { data && data.length && <Funnel width={width} height={170} data={data} /> }
        </div>
      </div>
    );
  }
}


const styles = {
  toolbar: {background: 'transparent', paddingRight: 0}
}