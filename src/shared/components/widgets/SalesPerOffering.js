import React from 'react';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import Base from './BaseWidget';

import { PNAHorizontalBar, formatValue, colorScale } from '../../util/d3/AppHorizontalBar';

import { dashboardWidgetDropdownProps } from '../../../theme.config';

const colors = ['#95D9EE', '#3DA3C0', '#FEA171', '#FD6E69', '#7585AD'];

export default class extends Base {
  state = {
    width: 300
  }
  formatDataTooltips(data) {
    const cScale = colorScale(colors);
    return data.map((d,i) => {
      const value = formatValue(d.value);
      const color = cScale(d.value);
      return Object.assign({}, d, {
        tooltip: `<p><span style="color: ${color}">${d.title}</span><br /><strong>${value}</strong></p>`
      })
    })
  }

  render() {
    const formatedData = this.formatDataTooltips(this.props.data);
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Sales per Offering</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
             <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="All Time" />
              </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>

       <PNAHorizontalBar width={this.state.width} data={formatedData} />

      </div>
    );
  }
}


const styles = {
  toolbar: {background: 'transparent', paddingRight: 0}
}
