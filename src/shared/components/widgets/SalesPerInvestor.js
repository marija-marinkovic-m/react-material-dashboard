import React from 'react';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import Base from './BaseWidget';

import { formatValue, colorScale } from '../../util/d3/AppHorizontalBar';
import { PNAHorizontalBarStacked } from '../../util/d3/AppHorizontalBarStacked';

import { dashboardWidgetDropdownProps } from '../../../theme.config';


const colors = ['#FD6C67', '#95D9EE', '#AF74A3', '#FEA171', '#01C0B3'];

export default class extends Base {
  state = {
    width: 300
  }
  formatDataTooltips(data) {
    const cScale = colorScale(colors);
    return data.map((d,i) => {
      const value = formatValue(d.value);
      const color = cScale(d.value);
      return Object.assign({}, d, {
        tooltip: `<p><span style="color: ${color}">${d.title}</span><br /><strong>${value}</strong></p>`
      })
    })
  }
  render() {
    // const formatedData = this.formatDataTooltips(dummyData);
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Sales per Investor</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
            <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="All Offerings" />
              </DropDownMenu>
             <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="All Time" />
              </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>


          <PNAHorizontalBarStacked
            width={this.state.width}
            data={this.props.data}
            colors={colors} />
      </div>
    );
  }
}


const styles = {
  toolbar: {background: 'transparent', paddingRight: 0}
}