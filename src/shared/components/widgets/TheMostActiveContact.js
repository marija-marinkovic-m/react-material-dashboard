import React from 'react';
import ProfileAvatar from '../ProfileAvatar';
import './TheMostActiveContact.css';

const TheMostActiveContactWidget = ({data}) => (
  <div className="the-most-active-contact">
    <h4>{"The Most Active \nContact"}</h4>
    <figure>
      <ProfileAvatar avatarProps={{size: 48}} userData={data} />
      <h3>{data.firstName + ' ' + data.lastName}</h3>
      <p>{data.totalActivities} Activities This Week</p>
    </figure>
  </div>
);

export default TheMostActiveContactWidget;