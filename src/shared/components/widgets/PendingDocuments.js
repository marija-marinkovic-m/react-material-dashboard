import React from 'react';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import { getProfileAvatarProps } from '../ProfileAvatarTableListItem';
import IconButton from 'material-ui/IconButton';
import LinearProgress from 'material-ui/LinearProgress';

import InfoIcon from 'material-ui/svg-icons/action/info';
import WarningIcon from 'material-ui/svg-icons/alert/warning';

import Base from './BaseWidget';

import { dashboardWidgetDropdownProps, themeColors } from '../../../theme.config';

export default class extends Base {

  render() {
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Pending Documents</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
            <DropDownMenu {...dashboardWidgetDropdownProps}>
              <MenuItem primaryText="2014" />
              <MenuItem primaryText="2015" />
              <MenuItem primaryText="View All" />
            </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>

        <div style={styles.tableWrap}>
          <Table
            className="dashboard-table"
            style={styles.table}
            selectable={false}>
            <TableBody
              displayRowCheckbox={false}
              showRowHover={false}
              stripedRows={true}>
              { this.props.data.map((item, index) => {
                const avatarProps = getProfileAvatarProps(item);
                return (
                  <TableRow
                    key={index}
                    style={{border: 'none'}}>
                    <TableRowColumn style={{paddingLeft: 0}}>
                      <ListItem
                        primaryText={item.firstName + ' ' + item.lastName}
                        leftAvatar={<Avatar className="avatar-tag" {...avatarProps} />}
                        disabled={true}
                        style={styles.listItem} />
                    </TableRowColumn>
                    <TableRowColumn>
                      <LinearProgress
                        mode="determinate"
                        value={item.documentStatus}
                        color={item.documentState === 'danger' ? themeColors.red : themeColors.gold}
                        style={{backgroundColor: '#4F596A', borderRadius: '3px'}} />
                    </TableRowColumn>
                    <TableRowColumn style={{textAlign: 'right'}}>
                      <IconButton>
                        { 
                          item.documentState === 'danger' ? 
                          <WarningIcon color={themeColors.red} /> :
                          <InfoIcon color={themeColors.gold} />
                         }
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                );
              }) }
            </TableBody>
          </Table>
        </div>

      </div>
    );
  }
}


const styles = {
  toolbar: {background: 'transparent', paddingRight: 0},
  listItem: {color: themeColors.white, fontSize: '12px'},
  tableWrap: {height: 280, overflow: 'auto', margin: '35px 0 20px'},
  table: {background: 'transparent'}
}