import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import { getProfileAvatarProps } from '../ProfileAvatarTableListItem';

import { timelineFilters } from '../Timeline';
import { dashboardWidgetDropdownProps, themeColors } from '../../../theme.config';

export default class extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  }
  openContact = () => false;
  render() {
    return (
      <div>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Sales With Recent Activity</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
            <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="View All" />
              </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>

        <div style={styles.tableWrap}>
          
          <Table
            className="dashboard-table"
            style={styles.table}
            selectable={false}>
            <TableBody displayRowCheckbox={false} showRowHover={false} stripedRows={true}>
              { this.props.data.map((item, index) => {
                const avatarProps = getProfileAvatarProps(item);
                const currentTimelineEvent = timelineFilters[item.lastActivity.type] || timelineFilters.generic;
                return (<TableRow
                  key={index}
                  style={{border: 'none'}}>
                  <TableRowColumn style={{paddingLeft: 0}}>
                    <ListItem
                      primaryText={`${item.firstName} ${item.lastName}`}
                      leftAvatar={<Avatar className="avatar-tag" {...avatarProps} />}
                      disabled={true} style={styles.listItem} />
                  </TableRowColumn>
                  <TableRowColumn>
                    <ListItem
                      primaryText={item.sale.title}
                      secondaryText={<p style={{fontSize: '12px'}}>{item.sale.price}</p>}
                      disabled={true} style={Object.assign({}, styles.listItem, {paddingTop: 0, paddingBottom: 0})} />
                  </TableRowColumn>
                  <TableRowColumn>
                    <ListItem
                      leftIcon={currentTimelineEvent.icon}
                      primaryText={currentTimelineEvent.label}
                      disabled={true} style={Object.assign({}, styles.listItem, {
                        color: currentTimelineEvent.color,
                        paddingLeft: '50px'
                      })} />
                  </TableRowColumn>
                  <TableRowColumn>
                    <p style={{textAlign: 'right'}}>{ item.lastActivity.time }</p>
                  </TableRowColumn>
                </TableRow>)
              }) }
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

const styles = {
  toolbar: {background: 'transparent', paddingRight: 0},
  listItem: {color: themeColors.white, fontSize: '12px'},
  tableWrap: {height: 330, overflow: 'auto', margin: '35px 0 20px'},
  table: {background: 'transparent'}, // {background: 'transparent', width: 'auto', minWidth: '100%'}
  tableBody: {} // {overflowX: 'auto'}
}
