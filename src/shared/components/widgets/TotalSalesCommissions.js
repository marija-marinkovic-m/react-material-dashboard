import React from 'react';
import cn from 'classnames';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { GridList } from 'material-ui/GridList';
import {ListItem} from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';

import Base from './BaseWidget';

import { dashboardWidgetDropdownProps } from '../../../theme.config'

import TooltipTrigger from '../../util/Tooltip';

import { PNATotalSalesCommissionsGraph } from '../../util/d3/AppBarStacked';


const commissionColors = ['#7ED321', '#4A90E2', '#FCD056'];
const saleColors = ['#7ED321', '#FCD056'];

export class TotalCommissionsWidget extends Base {
  render() {
    const {width, height, dataType, barInterspace, keys, annualData, quartersData} = this.state;
    const chartProps = {
      width, height, barInterspace,
      timeParseFormat: dataType !== 'annual' ? '%m-%m/%Y' : '%m/%Y',
      stackKeys: keys,
      stackData: dataType !== 'annual' ? quartersData : annualData,
      colors: commissionColors
    }

    const isMobileView = this.props.screenWidth <= 768;

    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Total Commissions</h3></ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>


        <GridList style={styles.gridList} cellHeight="auto" cols={isMobileView ? 1 : 2}>
          <ListItem
            className="stats"
            style={{color: '#FFF'}}
            primaryText="$&nbsp;21,000.00"
            secondaryText={<p className="secondary" style={{color: commissionColors[0]}}>Total Commission Paid to Date</p>}
            leftIcon={<FontIcon className="pna-icon pna icon-commissionsTotal" color={commissionColors[0]} />}
            disabled={true} />
        </GridList>

        <GridList style={styles.gridList} cellHeight="auto" cols={isMobileView ? 1 : 2}>
          <ListItem
            className="stats"
            style={{color: '#FFF'}}
            primaryText="$&nbsp;7,750.00"
            secondaryText={<div className="secondary" style={{color: commissionColors[1]}}>Pending Commissions <TooltipTrigger tooltip="Commissions you will earn once the deal is completed." placement="bottom"><FontIcon className="sup pna icon-question-mark" /></TooltipTrigger></div>}
            leftIcon={<FontIcon className="pna-icon pna icon-commissionsPending" color={commissionColors[1]} />}
            disabled={true} />
          <ListItem
            className="stats"
            style={{color: '#FFF'}}
            primaryText="$&nbsp;3,950.00"
            secondaryText={<div className="secondary" style={{color: commissionColors[2]}}>Payable Commission <TooltipTrigger
             tooltip="Commissions you earned but have not yet received." placement="bottom" size="md"><FontIcon className="sup pna icon-question-mark" /></TooltipTrigger></div>}
            leftIcon={<FontIcon className="pna-icon pna icon-commissionsPayable" color={commissionColors[2]} style={{fontSize: '14px', textAlign: 'center'}} />}
            disabled={true} />
        </GridList>


        <div>

          <h5>Commissions Chart</h5>

          <Toolbar style={styles.toolbar}>
            <ToolbarGroup firstChild={true}>
              <div className="chart-types">
                <a
                  href="#quarters"
                  onClick={this.handleDataChange.bind(null, 'quarters')}
                  className={cn({active: 'quarters' === dataType})}>Quarters
                </a>
                <a
                  href="#annual"
                  onClick={this.handleDataChange.bind(null, 'annual')}
                  className={cn({active: 'annual' === dataType})}>Annual
                </a>
              </div>
            </ToolbarGroup>
            <ToolbarGroup lastChild={true}>
              <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="2016" />
              </DropDownMenu>
            </ToolbarGroup>
          </Toolbar>
          
          <PNATotalSalesCommissionsGraph {...chartProps} />

        </div>
      </div>
    );
  }
}

export class TotalSalesWidget extends Base {
  render() {
    const {width, height, dataType, barInterspace, keys, annualData, quartersData} = this.state;
    const chartProps = {
      width, height, barInterspace,
      timeParseFormat: dataType !== 'annual' ? '%m-%m/%Y' : '%m/%Y',
      stackKeys: keys,
      stackData: dataType !== 'annual' ? quartersData : annualData,
      colors: saleColors
    }

    const isMobileView = this.props.screenWidth <= 768;
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Total Sales</h3></ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>


        <GridList style={styles.gridList} cellHeight="auto" cols={isMobileView ? 1 : 2}>
          <ListItem
            className="stats"
            primaryText="32"
            style={{color: saleColors[0]}}
            secondaryText={<p className="secondary" style={{color: '#fff'}}>Closed Sales</p>}
            leftIcon={<FontIcon className="pna-icon pna icon-deal" color={saleColors[0]} style={{fontSize: '18px'}} />}
            disabled={true} />
          <ListItem
            className="stats"
            primaryText="$&nbsp;499,983.00"
            style={{color: '#FFF'}}
            secondaryText={<p className="secondary" style={{color: saleColors[0]}}>Total Sales</p>}
            leftIcon={<FontIcon className="pna-icon pna icon-roundedDollar" color={saleColors[0]} />}
            disabled={true} />
        </GridList>

        <GridList style={styles.gridList} cellHeight="auto" cols={isMobileView ? 1 : 2}>
          <ListItem
            className="stats"
            primaryText="9"
            style={{color: saleColors[1]}}
            secondaryText={<p className="secondary" style={{color: '#fff'}}>Pending Sales</p>}
            leftIcon={<FontIcon className="pna-icon pna icon-pendingDollar" color={saleColors[1]} />}
            disabled={true} />
          <ListItem
            className="stats"
            primaryText="$&nbsp;155,950.00"
            style={{color: '#fff'}}
            secondaryText={<p className="secondary" style={{color: saleColors[1]}}>Total Pending</p>}
            leftIcon={<FontIcon className="pna-icon pna icon-roundedTime" color={saleColors[1]} />}
            disabled={true} />
        </GridList>


        <div>

          <h5>Sales Chart</h5>

          <Toolbar style={styles.toolbar}>
            <ToolbarGroup firstChild={true}>
              <div className="chart-types">
                <a
                  href="#quarters"
                  onClick={this.handleDataChange.bind(null, 'quarters')}
                  className={cn({active: 'quarters' === dataType})}>Quarters
                </a>
                <a
                  href="#annual"
                  onClick={this.handleDataChange.bind(null, 'annual')}
                  className={cn({active: 'annual' === dataType})}>Annual
                </a>
              </div>
            </ToolbarGroup>
            <ToolbarGroup lastChild={true}>
              <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="2016" />
              </DropDownMenu>
            </ToolbarGroup>
          </Toolbar>
          
          <PNATotalSalesCommissionsGraph {...chartProps} />

        </div>
      </div>
    );
  }
}

const styles = {
  toolbar: {background: 'transparent', paddingRight: 0},
  dropDownMenu: {margin: 0},
  gridList: {margin:0, height: 'auto', color: '#FFFFFF'}
}