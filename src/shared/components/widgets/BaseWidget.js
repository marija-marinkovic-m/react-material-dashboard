import { Component } from 'react';
import PropTypes from 'prop-types';

export default class Base extends Component {
  static propTypes = {
    screenWidth: PropTypes.number,
    sideNavController: PropTypes.bool,
    width: PropTypes.number,
    height: PropTypes.number,
    barInterspace: PropTypes.any
  }
  static defaultProps = {
    width: 0,
    height: 320,
    dataType: 'annual',
    barInterspace: .7
  }
  constructor(props) {
    super(props);
    this.state = Object.assign({}, props);
  }
  componentDidMount() {
    setTimeout(this.resizeGraph.bind(this), 500)
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.screenWidth !== this.props.screenWidth || prevProps.sideNavController !== this.props.sideNavController) {
      setTimeout(this.resizeGraph.bind(this), 500)
    }
  }

  resizeGraph() {
    if (!this.wrapRef || !this.wrapRef.offsetWidth) return;
    this.setState({width: this.wrapRef.offsetWidth})
  }
  handleDataChange = (dataType, e) => {
    e.preventDefault();
    this.setState({dataType});
  }
}
