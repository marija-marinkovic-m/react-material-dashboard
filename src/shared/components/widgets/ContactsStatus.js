import React from 'react';

import Base from './BaseWidget';
import { PNAContactStatusDonutChart } from '../../util/d3/AppDonutChart';

import StatusCircle from '../../util/svg-icons/statusCircle';

import './ContactsStatus.css';

export default class extends Base {
  render() {
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <div className="legend">
          <h4>Contacts Status</h4>
          
          <ul className="legend">
            { this.props.data.map((d,i) => (<li key={i}>
              <StatusCircle color={d.color} /> {d.label}
            </li>)) }
          </ul>
        </div>
        <PNAContactStatusDonutChart
          width={this.state.width}
          height={this.state.width}
          data={this.props.data}
           />
      </div>
    );
  }
}