import React from 'react';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import Base from './BaseWidget';

import { PNASalesInvestorsChart } from '../../util/d3/AppLineChart';

import { dashboardWidgetDropdownProps } from '../../../theme.config';

export default class extends Base {
  render() {
    const {width, height} = this.state;
    const chartProps = {
      width, height,
      keys: this.props.keys,
      data: this.props.data
    }
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true}><h3>Contacts And Investors</h3></ToolbarGroup>
          <ToolbarGroup lastChild={true}>
             <DropDownMenu {...dashboardWidgetDropdownProps}>
                <MenuItem primaryText="2014" />
                <MenuItem primaryText="2015" />
                <MenuItem primaryText="2016" />
              </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        <p>Aliuam ticinudn lorem sit amet nisil voluptat<br />eget egestas diam placerat.</p>
          
        <PNASalesInvestorsChart {...chartProps} />
      </div>
    );
  }
}


const styles = {
  toolbar: {background: 'transparent', paddingRight: 0}
}
