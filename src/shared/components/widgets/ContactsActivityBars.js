import React from 'react';
import Base from './BaseWidget';
import { SimpleActivityBarChart } from '../../util/d3/SimpleActivityBarChart';

import './ContactsActivityBars.css';

export default class extends Base {
  render() {
    const barColors = ['#FBDF30', '#F25967'];
    return (
      <div ref={wrapEl => this.wrapRef = wrapEl} className="active-vs-passive">
        <h4>{"Active vs. \nPassive Contacts"}</h4>

        <div className="flex-wrapper">
          <ul>
          { this.props.data.map((d,i) => (
            <li key={i}>
              <span className="count" style={{color: barColors[i]}}>{d.count}</span>
              <p>{d.label}</p>
            </li>
          )) }
          </ul>

          <SimpleActivityBarChart
            width={this.state.width/5}
            height={120}
            data={this.props.data}
            colors={barColors} />
        </div>
      </div>
    );
  }
}