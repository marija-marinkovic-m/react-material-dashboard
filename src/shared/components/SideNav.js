import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateSideNav } from '../../redux/actions';
import { NavLink, withRouter } from 'react-router-dom';

// utils
import cn from 'classnames';

// ui components
import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Avatar from 'material-ui/Avatar';
import TooltipTrigger from '../util/Tooltip';

// icons
import ActionDashboard from 'material-ui/svg-icons/action/dashboard';
import SocialGroup from 'material-ui/svg-icons/social/group';
// import DealsIcon from '../util/svg-icons/deals';
import SocialLocationCity from 'material-ui/svg-icons/social/location-city';
import ReferralsIcon from '../util/svg-icons/referrals';
import ActionQuestionAnswer from 'material-ui/svg-icons/action/question-answer';
import ActionWork from 'material-ui/svg-icons/action/work';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
import SupportIcon from '../util/svg-icons/support';
import RoundedDollar from '../util/svg-icons/roundedDollar';

// assets
import logoSvg from '../assets/images/logo.png';
import userPic from '../assets/images/Pic.png'; // @TEMP

import { themeColors } from '../../theme.config';
import './SideNav.css';

class SideNav extends Component {
  static propTypes = {
    // injected by react redux
    mediaQueries: PropTypes.object,
    expanded: PropTypes.bool.isRequired,
    onRequestChange: PropTypes.func.isRequired,
    location: PropTypes.object,
    history: PropTypes.object
  }

  toggleExpanded = () => {
    this.props.onRequestChange.apply(null, [{expanded: !this.props.expanded}])
  }

  handleNavLinkClick = () => {
    if (this.props.mediaQueries.screenWidth <= 1280) this.toggleExpanded();
  }

  getNavLinks = (item, index) => {
    const listItemIconButtonProps = {
      // tooltip: !this.props.expanded && item.label,
      // tooltipPosition: 'top-right',
      style: navListIconStyle
    }
    const listItemIconButton = (
      <IconButton {...listItemIconButtonProps}>
        { item.icon }
      </IconButton>
    );
    const listItemProps = {
      style: navListItemStyle,
      primaryText: <div className="primaryLabel">{item.label}</div>,
      leftIcon: listItemIconButton
    };

    // child routes check
    const selected = item.path !== '/' && this.props.location.pathname.substr(0, item.path.length) === item.path;

    return(
      <NavLink key={index}
        to={item.path}
        exact
        activeClassName="selected"
        className={cn({selected})}
        onClick={this.handleNavLinkClick}>
        {this.props.expanded ? <ListItem {...listItemProps} /> : <TooltipTrigger className="sideNav-tooltip" tooltip={item.label} placement="right"><ListItem {...listItemProps} /></TooltipTrigger>}
      </NavLink>
    );
  }

  getSubheader = () => {
    let breakPoint = this.props.mediaQueries.screenWidth;

    if (breakPoint <= 1280) {
      return (
        <div className="app-drawer-subheader">
          <ListItem
            style={{
              marginTop: '20px',
              color: 'white',
              fontSize: '18px',
              fontFamily: '"Avenir LT 45 Book", sans-serif'
            }}
            primaryText="Jesse Bradley"
            leftAvatar={<Avatar src={userPic} />} />

          <FontIcon
            className="pna icon-sideNavCollapse"
            onClick={this.toggleExpanded}
            color={themeColors.gold}
            style={{
              position: 'absolute',
              top: '19px',
              right: '-48px'
            }} />
        </div>
      );
    }
    return (
      <ListItem
        className="app-drawer-subheader"
        style={{marginTop: '30px', marginBottom: '20px'}}
        onClick={this.toggleExpanded}>
        {
          !this.props.expanded ? 
          <FontIcon
            className="pna icon-sideNavExpand"
            color="white"
            style={{fontSize: '18px', paddingLeft: '10px'}} /> :
          <img 
            src={logoSvg} 
            alt="React D3 Dashboard" 
            style={{cursor: 'pointer', paddingLeft: '10px'}} /> 
        }
      </ListItem>
    );
  }

  render() {
    const { mediaQueries, expanded, onRequestChange } = this.props;
    const subheader = this.getSubheader();
    const navLinks = navListItems.map(this.getNavLinks);

    const isMobileView = mediaQueries.screenWidth <= 1280;

    const drawerProps = {
      docked: !isMobileView,
      open: isMobileView ? expanded : true,
      containerClassName: cn('app-drawer', {isMobileView}, {expanded}),
      containerStyle: {
        overflow: 'visible',
        // overflowX: 'hidden',
        // overflowY: 'scroll',
        position: mediaQueries.screenWidth > 1920 ? 'absolute' : 'fixed',
        width: expanded ? '255px' : isMobileView ? 0 : '73px'
      },
      onRequestChange: expanded => onRequestChange({expanded})
    }

    return (
      <Drawer {...drawerProps}>
        {subheader}
        <List style={{overflowX: 'visible', overflowY: 'auto', display: 'block', height: '100%'}} children={navLinks} />
      </Drawer>
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  expanded: state.sideNavController.expanded,
  location: state.router.location
});
const mapDispatchToProps = dispatch => ({
  onRequestChange: bindActionCreators(updateSideNav, dispatch)
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SideNav));

//////
const navListItemStyle = {
  lineHeight: '32px',
  color: themeColors.sidebarNavText,
  textTransform: 'uppercase',
  fontSize: '12px',
  textDecoration: 'none'
}
const navListIconStyle = {
  margin: '19px 20px',
  padding: 0,
}



const navListItems = [
  {
    path: '/',
    label: 'Dashboard',
    icon: <ActionDashboard />
  },
  {
    path: '/contacts',
    label: 'Contacts',
    icon: <SocialGroup />
  },
  {
    path: '/sales',
    label: 'Sales',
    icon: <RoundedDollar style={{width: '20px'}} /> //<MonetizationOn />
  },
  {
    path: '/projects',
    label: 'Projects',
    icon: <SocialLocationCity />
  },
  {
    path: '/referrals',
    label: 'Referrals',
    icon: <ReferralsIcon />
  },
  {
    path: '/messages',
    label: 'Messages',
    icon: <ActionQuestionAnswer />
  },
  {
    path: '/toolkit',
    label: 'Toolkit',
    icon: <ActionWork />
  },
  {
    path: '/expenses',
    label: 'Expenses',
    icon: <ActionTimeline />
  },
  {
    path: '/support',
    label: 'Support',
    icon: <SupportIcon />
  }
];