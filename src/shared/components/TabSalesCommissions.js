import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { GridList, GridTile } from 'material-ui/GridList';

import {
  TotalCommissionsWidget,
  TotalSalesWidget,
  SalesActivityTable,
  SalesFunnel,
  SalesPerInvestor,
  SalesPerOffering
} from '../components/widgets';

import '../components/widgets/dashboardWidgets.css';

export default class extends Component {
  static propTypes = {
    screenWidth: PropTypes.number,
    sideNavController: PropTypes.bool
  }
  componentWillMount() {
    // fetch the widgets data
    fetchTabData.apply(this);
  }

  handleSalesFunnelChange = (period) => {
    this.setState({
      salesFunnelPeriod: period
    })
  }

  render() {
    const isMobileView = this.props.screenWidth <= 768;
    const { totalCommissionsData, totalSalesData, salesActivityTableData, salesPerInvestorData, salesPerOfferingData, salesFunnelData, salesFunnelPeriod } = this.state;
    const gridListProps = {
      cellHeight: 'auto',
      cols: isMobileView ? 1 : 2,
      padding: isMobileView ? 0 : 20,
      style: styles.gridList,
      className: 'dashboard-widget'
    }
    return (
      <div>
        <GridList {...gridListProps}>
          <GridTile style={styles.gridTile}>
            { totalCommissionsData && <TotalCommissionsWidget
              keys={totalCommissionsData.keys}
              annualData={totalCommissionsData.annual}
              quartersData={totalCommissionsData.quarters}
              {...this.props} /> }
          </GridTile>
          <GridTile style={styles.gridTile}>
            { totalSalesData && <TotalSalesWidget
              keys={totalSalesData.keys}
              annualData={totalSalesData.annual}
              quartersData={totalSalesData.quarters}
              dataType="quarters"
              {...this.props} /> }
          </GridTile>
        </GridList>

        <GridList {...gridListProps}>
          <GridTile cols={isMobileView ? 1 : 1.2} style={styles.gridTile}>
            { salesActivityTableData && <SalesActivityTable
              data={salesActivityTableData} /> }
          </GridTile>
          <GridTile cols={isMobileView ? 1 : 0.8} style={styles.gridTile}>
             { salesFunnelData && <SalesFunnel
              period={salesFunnelPeriod}
              data={salesFunnelData[salesFunnelPeriod]}
              onChange={this.handleSalesFunnelChange}
              {...this.props} /> }
          </GridTile>
        </GridList>

        <GridList {...gridListProps}>
          <GridTile style={styles.gridTile}>
            { salesPerInvestorData && <SalesPerInvestor
              data={salesPerInvestorData}
              {...this.props} /> }
          </GridTile>
          <GridTile style={styles.gridTile}>
            { salesPerOfferingData && <SalesPerOffering 
              data={salesPerOfferingData}
              {...this.props} /> }
          </GridTile>
        </GridList>
      </div>
    );
  }
}

const styles = {
  gridList: {width: '100%', overflow: 'hidden', marginBottom: '20px'},
  gridTile: {background: 'rgba(26,32,42,.4)', padding: '25px 30px'}
}

function fetchTabData() {
  this.setState({
    totalCommissionsData: {
      keys: ["paid", "payable", "pending"],
      annual: [
        {
          "pending": "584.81",
          "payable": "1161.69",
          "paid": "1214.31",
          "date": "1/2016",
          "dateFormat": "Jan"
        },
        {
          "pending": "958.97",
          "payable": "554.57",
          "paid": "3564.44",
          "date": "2/2016",
          "dateFormat": "Feb"
        },
        {
          "pending": "142.28",
          "payable": "1175.60",
          "paid": "1035.67",
          "date": "3/2016",
          "dateFormat": "Mar"
        },
        {
          "pending": "671.60",
          "payable": "624.09",
          "paid": "2409.47",
          "date": "4/2016",
          "dateFormat": "Apr"
        },
        {
          "pending": "228.64",
          "payable": "1643.01",
          "paid": "3554.08",
          "date": "5/2016",
          "dateFormat": "May"
        },
        {
          "pending": "346.60",
          "payable": "1433.70",
          "paid": "3287.25",
          "date": "6/2016",
          "dateFormat": "Jun"
        },
        {
          "pending": "445.72",
          "payable": "1700.09",
          "paid": "1988.81",
          "date": "7/2016",
          "dateFormat": "Jul"
        },
        {
          "pending": "298.32",
          "payable": "622.34",
          "paid": "2997.87",
          "date": "8/2016",
          "dateFormat": "Aug"
        },
        {
          "pending": "675.14",
          "payable": "777.19",
          "paid": "4884.27",
          "date": "9/2016",
          "dateFormat": "Sep"
        },
        {
          "pending": "626.48",
          "payable": "1209.87",
          "paid": "2528.08",
          "date": "10/2016",
          "dateFormat": "Oct"
        },
        {
          "pending": "191.66",
          "payable": "1031.62",
          "paid": "2496.04",
          "date": "11/2016",
          "dateFormat": "Nov"
        },
        {
          "pending": "143.57",
          "payable": "948.63",
          "paid": "2085.55",
          "date": "12/2016",
          "dateFormat": "Dec"
        }
      ],
      quarters: [
        {
          "pending": "942.04",
          "payable": "1349.44",
          "paid": "3418.55",
          "date": "1-3/2016",
          "dateFormat": "Q1"
        },
        {
          "pending": "650.65",
          "payable": "1786.94",
          "paid": "2969.77",
          "date": "4-6/2016",
          "dateFormat": "Q2"
        },
        {
          "pending": "768.95",
          "payable": "1267.35",
          "paid": "2868.06",
          "date": "7-9/2016",
          "dateFormat": "Q3"
        },
        {
          "pending": "868.40",
          "payable": "1867.07",
          "paid": "3826.76",
          "date": "10-12/2016",
          "dateFormat": "Q4"
        }
      ]
    },
    totalSalesData: {
      keys: ["closed", "pending"],
      annual: [
        {
          "pending": "4239.77",
          "closed": "8366.01",
          "date": "1/2016",
          "dateFormat": "Jan"
        },
        {
          "pending": "455.55",
          "closed": "1310.35",
          "date": "2/2016",
          "dateFormat": "Feb"
        },
        {
          "pending": "4434.99",
          "closed": "10121.27",
          "date": "3/2016",
          "dateFormat": "Mar"
        },
        {
          "pending": "4151.83",
          "closed": "11020.36",
          "date": "4/2016",
          "dateFormat": "Apr"
        },
        {
          "pending": "4033.79",
          "closed": "8705.73",
          "date": "5/2016",
          "dateFormat": "May"
        },
        {
          "pending": "1749.43",
          "closed": "14006.58",
          "date": "6/2016",
          "dateFormat": "Jun"
        },
        {
          "pending": "4113.65",
          "closed": "7951.99",
          "date": "7/2016",
          "dateFormat": "Jul"
        },
        {
          "pending": "2054.05",
          "closed": "11988.90",
          "date": "8/2016",
          "dateFormat": "Aug"
        },
        {
          "pending": "4966.08",
          "closed": "10404.00",
          "date": "9/2016",
          "dateFormat": "Sep"
        },
        {
          "pending": "999.65",
          "closed": "7600.62",
          "date": "10/2016",
          "dateFormat": "Oct"
        },
        {
          "pending": "2574.24",
          "closed": "6308.94",
          "date": "11/2016",
          "dateFormat": "Nov"
        },
        {
          "pending": "750.80",
          "closed": "13068.62",
          "date": "12/2016",
          "dateFormat": "Dec"
        }
      ],
      quarters: [
        {
          "pending": "735.25",
          "closed": "17151.36",
          "date": "1-3/2016",
          "dateFormat": "Q1"
        },
        {
          "pending": "7508.81",
          "closed": "7275.37",
          "date": "4-6/2016",
          "dateFormat": "Q2"
        },
        {
          "pending": "5772.10",
          "closed": "15006.34",
          "date": "7-9/2016",
          "dateFormat": "Q3"
        },
        {
          "pending": "4718.14",
          "closed": "17098.28",
          "date": "10-12/2016",
          "dateFormat": "Q4"
        }
      ]
    },
    salesPerInvestorData: [
      {
        user: {
          fullName: 'Ronnie Chambers',
          avatarUrl: 'static/img/temp/avatar4.jpg'
        },
        sales: [
          {name: 'Park Avenue South', value: 2450000, date: '12 Nov 2016'},
          {name: 'Mountain Inc.', value: 1345000, date: '11 May 2017'}
        ]
      },
      {
        user: {
          fullName: 'Caleb Castillio',
          avatarUrl: 'static/img/temp/avatar3.jpg'
        },
        sales: [
          {name: 'TA / Park Corporate Band', value: 345000, date: '5 May 2017'},
          {name: 'Park Avenue South', value: 2450000, date: '12 Nov 2016'},
          {name: '84 William Inc.', value: 1345000, date: '11 May 2017'}
        ]
      }
    ],
    salesPerOfferingData: [
      { title: 'Spark Aurelius South Very long Company Investment Title Here', value: 2000000 },
      { title: 'Nomad Lengthy', value: 388000 },
      { title: 'Corporate Band', value: 4000000 },
      { title: '84 William', value: 2000012 },
      { title: 'AKA UN Recap', value: 358444 },
      { title: 'Solomonium', value: 500558},
      { title: 'Nomad', value: 105880 },
      { title: 'NYI Mountain Inc Band', value: 2585550 },
      { title: '1484 Bursac', value: 885921 },
      { title: 'Park Avenue South', value: 3055280 }
    ],
    salesActivityTableData: [
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'prospect',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        sale: {title: 'Mountain Inc.', price: '$ 45,560.20'},
        lastActivity: {
          time: '29 Jun, 9:30AM',
          type: 'saleStatusChange'
        }
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers-3',
        type: 'investor',
        avatarUrl: null,
        sale: {title: 'The Assemblage/Park Avenue South', price: '$ 120,000.00'},
        lastActivity: {
          time: '27 May, 4:45PM',
          type: 'statusChange'
        }
      },
      {
        firstName: 'Lawrence',
        lastName: 'Cooper',
        slug: 'lawrence-cooper-2',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar3.jpg',
        sale: {title: 'TA / Park Avenue South', price: '$ 35,352.45'},
        lastActivity: {
          time: '2 days ago',
          type: 'messageSent'
        }
      },
      {
        firstName: 'Josephine',
        lastName: 'Gomez',
        slug: 'josephine-gomez-2',
        status: 'Not interested',
        type: 'prospect',
        sale: {title: 'Corporate Band', price: '$ 20,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'dealStatusChanged'
        }
      },
      {
        firstName: 'Bernie',
        lastName: 'Donnan',
        slug: 'bernie-donnan-2',
        type: 'prospect',
        avatarUrl: null,
        sale: {title: 'Siettle University Camp Park', price:'120,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'invitationSent'
        }
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'prospect',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        sale: {title: 'Mountain Inc.', price: '$ 45,560.20'},
        lastActivity: {
          time: '29 Jun, 9:30AM',
          type: 'saleStatusChange'
        }
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers-3',
        type: 'investor',
        avatarUrl: null,
        sale: {title: 'The Assemblage/Park Avenue South', price: '$ 120,000.00'},
        lastActivity: {
          time: '27 May, 4:45PM',
          type: 'statusChange'
        }
      },
      {
        firstName: 'Lawrence',
        lastName: 'Cooper',
        slug: 'lawrence-cooper-2',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar3.jpg',
        sale: {title: 'TA / Park Avenue South', price: '$ 35,352.45'},
        lastActivity: {
          time: '2 days ago',
          type: 'messageSent'
        }
      }
    ],
    salesFunnelData: {
      all: [
        {process: 'Invitations sent', value: 400000, desc: '', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Started Sales', value: 150000, desc: 'Invitation to Deals Started', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Closed Sales', value: 50000, desc: 'Deals Started to Closed sales', deals: [
          {name: '84 William', value: 50000}
        ]}
      ],
      '2016': [
        {process: 'Invitations sent', value: 134098, desc: '', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Started Sales', value: 120000, desc: 'Invitation to Deals Started', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Closed Sales', value: 98765, desc: 'Deals Started to Closed sales', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000}
        ]}
      ],
      '2015': [
        {process: 'Invitations sent', value: 126000, desc: '', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Started Sales', value: 126000, desc: 'Invitation to Deals Started', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Closed Sales', value: 126000, desc: 'Deals Started to Closed sales', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]}
      ],
      '2014': [
        {process: 'Invitations sent', value: 250000, desc: '', deals: [
          {name: 'Park Avenue South', value: 75000},
          {name: 'Nomad', value: 70000},
          {name: 'Corporate Band', value: 35000},
          {name: '84 William', value: 30000},
          {name: 'AKA UN Recap', value: 25000},
          {name: '17 John', value: 15000} 
        ]},
        {process: 'Started Sales', value: 0, desc: 'Invitation to Deals Started', deals: []},
        {process: 'Closed Sales', value: 0, desc: 'Deals Started to Closed sales', deals: []}
      ]
    },
    salesFunnelPeriod: 'all'
  })
}