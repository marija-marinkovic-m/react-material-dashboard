import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cn from 'classnames';

import { SpeedDial, SpeedDialItem } from '../util/SpeedDial';

import ContentAdd from 'material-ui/svg-icons/content/add';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import ReferralsIcon from '../util/svg-icons/referrals';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import ReminderIcon from '../util/svg-icons/reminder';
import ActionTimeline from 'material-ui/svg-icons/action/timeline';
// import CustomAdd from '../util/svg-icons/customAdd';

import { withRouter } from 'react-router-dom';

import { themeColors } from '../../theme.config';

import './FloatingSpeedDial.css';

class FloatingSpeedDial extends Component {
  static propTypes = {
    style: PropTypes.object,
    mobileStyle: PropTypes.object,
    history: PropTypes.object, // react-router-dom
    mediaQueries: PropTypes.object // redux
  }

  onContactClick = () => {
    this.props.history.push('/contacts/add');
  }
  onReferralClick = () => {
    this.props.history.push('/referrals');
  }
  onExpenseClick = () => {
    this.props.history.push('/expenses');
  }
  onReminderClick = () => {
    this.props.history.push('/reminders');
  }

  render() {
    const isMobileView = this.props.mediaQueries.screenWidth <= 768;
    const containerStyle = isMobileView ? Object.assign({}, styles.mobileMainContainer, this.props.mobileStyle) : Object.assign({}, styles.mainContainer, this.props.style);
    const labelTrigger = isMobileView ? 'always' : 'hover';

    const speedDialProps = {
      style: styles.speedDialMain,
      fabContentOpen: <ContentAdd />,
      fabContentClose: <NavigationClose />,
      fabProps: {backgroundColor: themeColors.gold, className: 'Main-Fab'},
      direction: isMobileView ? 'vertical' : 'horizontal',
      hasOverlay: isMobileView
    };
    const speedDialItems = [
      {label: 'Add Contact', fabContent: <PersonAdd />, onTouchTap: this.onContactClick, labelTrigger},
      {label: 'Add Referral', fabContent: <ReferralsIcon />, onTouchTap: this.onReferralClick, labelTrigger},
      {label: 'Add Expenses', fabContent: <ActionTimeline />, onTouchTap: this.onExpenseClick, labelTrigger},
      {label: 'Add Reminder', fabContent: <ReminderIcon />, onTouchTap: this.onReminderClick, labelTrigger}
    ];

    return (<div className={cn(['pna-speed-dial', {mobView: isMobileView}])} style={containerStyle}>
        <SpeedDial {...speedDialProps}>
          { speedDialItems.map((itemProps, index) => <SpeedDialItem key={index} {...itemProps} />) }
        </SpeedDial>
    </div>);
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker
});

export default withRouter(connect(mapStateToProps)(FloatingSpeedDial));

const styles = {
  mainContainer: {
    position: 'absolute', top: '36px',
    right: '190px', 
    zIndex: 2000
  },
  mobileMainContainer: {
    position: 'fixed', bottom: '70px',
    right: '45px', zIndex: 2000
  },
  speedDialMain: {
    zIndex: 9999, fill: '#000000'
  }
}