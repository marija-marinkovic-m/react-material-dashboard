import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { GridList, GridTile } from 'material-ui/GridList';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import { ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';

// icons
import PersonAdd from 'material-ui/svg-icons/social/person-add';

import {
  ContactsInvestors,
  OpportunitiesPerOffering,
  ContactsActivityTable,
  RecentOpportunitiesTable,
  ContactsStatus,
  PendingDocuments,
  PendingFunds,
  ContactsActivityBars,
  TheMostActiveContactWidget
} from '../components/widgets'

import { themeColors } from '../../theme.config';

import '../components/widgets/dashboardWidgets.css';
import './TabContactsOpportunities.css';

export default class extends Component {
  static propTypes = {
    screenWidth: PropTypes.number,
    sideNavController: PropTypes.bool,
    history: PropTypes.object
  }

  async componentWillMount() {
    // fetch data for widgets
    fetchTabData.apply(this);
  }

  handleAddNewContactTap = e => {
    this.props.history.push('/contacts/add')
  }

  render() {
    const isMobileView = this.props.screenWidth <= 768;
    const gridListProps = {
      cellHeight: 'auto',
      padding: isMobileView ? 0 : 20,
      style: styles.gridList,
      className: 'dashboard-widget'
    }

    const { contactActivityData, contactStatusesData, contactsActivityTableData, recentOpportunitiesTableData, pendingFundsData, pendingDocumentsData, theMostActiveContactData, contactsInvestors, opportunitiesPerOfferingData } = this.state;

    return(
      <div>
        <GridList {...Object.assign({}, gridListProps, {cols: isMobileView ? 1 : 4, style: Object.assign({}, styles.gridList, {overflow: 'visible'})})}>
          <GridTile style={Object.assign({}, styles.gridTile, {overflow: 'visible'})}>
            <div className="add-contact-block">
              <FloatingActionButton onTouchTap={this.handleAddNewContactTap} secondary={true}><PersonAdd /></FloatingActionButton>
              <h5>Add new contact</h5>
            </div>
          </GridTile>
          <GridTile style={styles.gridTile}>
            <ListItem
              disabled={true}
              className="stats"
              style={{color: themeColors.gold}}
              primaryText="143"
              secondaryText="Contacts in Total"
              leftIcon={<FontIcon className="pna pna-icon icon-people-icon" color={themeColors.gold} style={{fontSize: '12px'}} />} />
          </GridTile>
          <GridTile style={styles.gridTile}>
            <ListItem
              disabled={true}
              className="stats"
              style={{color: themeColors.blue}}
              primaryText="24"
              secondaryText="Investors"
              leftIcon={<FontIcon className="pna pna-icon icon-roundedDollar" color={themeColors.blue} style={{fontSize: '20px'}} />} />
          </GridTile>
          <GridTile style={styles.gridTile}>
            <ListItem
              disabled={true}
              className="stats"
              style={{color: themeColors.lightGray}}
              primaryText="$&nbsp;127,440.00"
              secondaryText="In 112 Opportunities"
              leftIcon={<FontIcon className="pna pna-icon icon-money-bulb-icon" color={themeColors.lightGray} style={{fontSize: '20px'}} />} />
          </GridTile>
        </GridList>

        <GridList {...Object.assign({}, gridListProps, {cols: isMobileView ? 1 : 3})}>
          <GridTile cols={isMobileView ? 1 : 0.6}>
            <div style={Object.assign({}, styles.gridTile, {marginBottom: 20})}>
              { contactActivityData && <ContactsActivityBars {...this.props} data={contactActivityData} /> }
            </div>
            <div style={styles.gridTile}>
              { theMostActiveContactData && <TheMostActiveContactWidget
                data={theMostActiveContactData} /> }
            </div>
          </GridTile>
          <GridTile style={styles.gridTile} cols={isMobileView ? 1 : 1.2}>
            { contactsInvestors && <ContactsInvestors
              keys={contactsInvestors.keys}
              data={contactsInvestors.data}
              {...this.props} /> }
          </GridTile>
          <GridTile style={styles.gridTile} cols={isMobileView ? 1 : 1.2}>
            { opportunitiesPerOfferingData && <OpportunitiesPerOffering
              data={opportunitiesPerOfferingData}
              {...this.props} /> }
          </GridTile>

          
        </GridList>


        <GridList {...gridListProps}>
          <GridTile style={styles.gridTile}>
            { contactsActivityTableData && <ContactsActivityTable
              data={contactsActivityTableData} /> }
          </GridTile>
          <GridTile style={styles.gridTile}>
            { recentOpportunitiesTableData && <RecentOpportunitiesTable
              data={recentOpportunitiesTableData} /> }
          </GridTile>
        </GridList>

        <GridList {...Object.assign({}, gridListProps, {cols: isMobileView ? 1 : 3})}>
          <GridTile style={styles.gridTile} cols={isMobileView ? 1 : 0.6}>
            { contactStatusesData && <ContactsStatus
              data={contactStatusesData}
              {...this.props} /> }
          </GridTile>
          <GridTile style={styles.gridTile} cols={isMobileView ? 1 : 1.2}>
            { pendingFundsData && <PendingFunds
              data={pendingFundsData} /> }
          </GridTile>
          <GridTile style={styles.gridTile} cols={isMobileView ? 1 : 1.2}>
            { pendingDocumentsData && <PendingDocuments
              data={pendingDocumentsData} /> }
          </GridTile>

          
        </GridList>
      </div>
    );
  }
}

const styles = {
  gridList: {width: '100%', overflow: 'hidden', marginBottom: '20px'},
  gridTile: {background: 'rgba(26,32,42,.4)', padding: '25px 30px', boxSizing: 'border-box'}
}

function fetchTabData() {
  this.setState({
    contactActivityData: [
      {label: 'Active Contacts', count: 112},
      {label: 'Passive Contact', count: 31}
    ],
    contactStatusesData: [
      { label: 'New Contact', value: 35, color: '#BFCCDC' },
      { label: 'Contacting', value: 340, color: '#4A90E2' },
      { label: 'Nurture', value: 125, color: '#FBC738' },
      { label: 'Interested', value: 52, color: '#7ED321' },
      { label: 'Not Interested', value: 74, color: '#F25967' }
    ],
    contactsActivityTableData: [
      {
        firstName: 'Josephine',
        lastName: 'Gomez',
        slug: 'josephine-gomez-2',
        status: 'Not interested',
        type: 'prospect',
        sale: {title: 'Corporate Band', price: '$ 20,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'dealStatusChanged'
        }
      },
      {
        firstName: 'Bernie',
        lastName: 'Donnan',
        slug: 'bernie-donnan-2',
        type: 'prospect',
        avatarUrl: null,
        sale: {title: 'Siettle University Camp Park', price:'120,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'invitationSent'
        }
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers-3',
        type: 'investor',
        avatarUrl: null,
        sale: {title: 'The Assemblage/Park Avenue South', price: '$ 120,000.00'},
        lastActivity: {
          time: '27 May, 4:45PM',
          type: 'statusChange'
        }
      },
      {
        firstName: 'Lawrence',
        lastName: 'Cooper',
        slug: 'lawrence-cooper-2',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar3.jpg',
        sale: {title: 'TA / Park Avenue South', price: '$ 35,352.45'},
        lastActivity: {
          time: '2 days ago',
          type: 'messageSent'
        }
      }
    ],
    recentOpportunitiesTableData: [
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'prospect',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        sale: {title: 'Mountain Inc.', price: '$ 45,560.20'},
        lastActivity: {
          time: '29 Jun, 9:30AM',
          type: 'saleStatusChange'
        }
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers-3',
        type: 'investor',
        avatarUrl: null,
        sale: {title: 'The Assemblage/Park Avenue South', price: '$ 120,000.00'},
        lastActivity: {
          time: '27 May, 4:45PM',
          type: 'statusChange'
        }
      },
      {
        firstName: 'Lawrence',
        lastName: 'Cooper',
        slug: 'lawrence-cooper-2',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar3.jpg',
        sale: {title: 'TA / Park Avenue South', price: '$ 35,352.45'},
        lastActivity: {
          time: '2 days ago',
          type: 'messageSent'
        }
      },
      {
        firstName: 'Josephine',
        lastName: 'Gomez',
        slug: 'josephine-gomez-2',
        status: 'Not interested',
        type: 'prospect',
        sale: {title: 'Corporate Band', price: '$ 20,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'dealStatusChanged'
        }
      },
      {
        firstName: 'Bernie',
        lastName: 'Donnan',
        slug: 'bernie-donnan-2',
        type: 'prospect',
        avatarUrl: null,
        sale: {title: 'Siettle University Camp Park', price:'120,000.00'},
        lastActivity: {
          time: '2 days ago',
          type: 'invitationSent'
        }
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'prospect',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        sale: {title: 'Mountain Inc.', price: '$ 45,560.20'},
        lastActivity: {
          time: '29 Jun, 9:30AM',
          type: 'saleStatusChange'
        }
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers-3',
        type: 'investor',
        avatarUrl: null,
        sale: {title: 'The Assemblage/Park Avenue South', price: '$ 120,000.00'},
        lastActivity: {
          time: '27 May, 4:45PM',
          type: 'statusChange'
        }
      },
      {
        firstName: 'Lawrence',
        lastName: 'Cooper',
        slug: 'lawrence-cooper-2',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar3.jpg',
        sale: {title: 'TA / Park Avenue South', price: '$ 35,352.45'},
        lastActivity: {
          time: '2 days ago',
          type: 'messageSent'
        }
      }
    ],
    pendingFundsData: [
      {
        firstName: 'Josephine',
        lastName: 'Gomez',
        slug: 'josephine-gomez',
        type: 'prospect',
        avatarUrl: '',
        commissionStatus: 'pending',
        commissionPaymentDate: '10 Avg',
        subscriptionDate: '25 Avg'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar2.jpg',
        commissionStatus: 'overdue',
        commissionPaymentDate: 'May 15',
        subscriptionDate: '30 June',
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        commissionPaymentDate: 'May 15',
        subscriptionDate: '12 may',
        commissionStatus: 'pending'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar2.jpg',
        commissionPaymentDate: '10 Avg',
        subscriptionDate: '25 Avg',
        commissionStatus: 'scheduled'
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'investor',
        avatarUrl: '',
        commissionPaymentDate: 'May 15',
        subscriptionDate: '12 may',
        commissionStatus: 'pending'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: '',
        commissionPaymentDate: '10 Avg',
        subscriptionDate: '25 Avg',
        commissionStatus: 'scheduled'
      }
    ],
    pendingDocumentsData: [
      {
        firstName: 'Josephine',
        lastName: 'Gomez',
        slug: 'josephine-gomez',
        type: 'prospect',
        avatarUrl: '',
        documentStatus: 48,
        documentState: 'warning'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar1.jpg',
        documentStatus: 98,
        documentState: 'danger'
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        documentStatus: 59,
        documentState: 'danger'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar2.jpg',
        documentStatus: 12,
        documentState: 'warning'
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'investor',
        avatarUrl: '',
        documentStatus: 85,
        documentState: 'danger'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: '',
        documentStatus: 78,
        documentState: 'warn'
      },
      {
        firstName: 'Ronnie',
        lastName: 'Chambers',
        slug: 'ronnie-chambers',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar1.jpg',
        documentStatus: 39,
        documentState: 'danger'
      },
      {
        firstName: 'Helena',
        lastName: 'Cooper',
        slug: 'helena-cooper',
        type: 'investor',
        avatarUrl: 'static/img/temp/avatar4.jpg',
        documentStatus: 39,
        documentState: 'danger'
      }
    ],
    theMostActiveContactData: {
      firstName: 'Lawrence',
      lastName: 'Cooper',
      slug: 'lawrence-cooper-3',
      status: 'Contacting',
      type: 'investor',
      avatarUrl: 'static/img/temp/avatar1.jpg',
      totalActivities: 9
    },
    contactsInvestors: {
      keys: ['contacts', 'investors'],
      data: [
        {
          investors: 45,
          contacts: 300,
          date: '1/2016',
          new: {
            investors: -35,
            contacts: 34
          }
        },
        {
          investors:120,
          contacts: 455,
          date: '2/2016',
          new: {
            investors: 75,
            contacts: 155
          }
        },
        {
          investors: 154,
          contacts: 499,
          date: '3/2016',
          new: {
            investors: 29,
            contacts: 44
          }
        },
        {
          investors: 400,
          contacts: 698,
          date: '4/2016',
          new: {
            investors: 246,
            contacts: 199
          }
        },
        {
          investors: 430,
          contacts: 780,
          date: '5/2016',
          new: {
            investors: 30,
            contacts: 82
          }
        },
        {
          investors: 567,
          contacts: 786,
          date: '6/2016',
          new: {
            investors: 137,
            contacts: 6
          }
        },
        {
          investors: 690,
          contacts: 790,
          date: '7/2016',
          new: {
            investors: 23,
            contacts: 4
          }
        },
        {
          investors:780,
          contacts: 1400,
          date: '8/2016',
          new: {
            investors: 110,
            contacts: 610
          }
        },
        {
          investors: 897,
          contacts: 1456,
          date: '9/2016',
          new: {
            investors: 117,
            contacts: 56
          }
        },
        {
          investors: 980,
          contacts: 1765,
          date: '10/2016',
          new: {
            investors: 83,
            contacts: 309
          }
        },
        {
          investors: 987,
          contacts: 1800,
          date: '11/2016',
          new: {
            investors: 7,
            contacts: 35
          }
        },
        {
          investors: 1000,
          contacts: 1856,
          date: '12/2016',
          new: {
            investors: 13,
            contacts: 56
          }
        }
      ]
    },
    opportunitiesPerOfferingData: [
      { title: 'AKA UN Recap', value: 50 },
      { title: 'Nomad', value: 258 },
      { title: 'Corporate Band', value: 892 },
      { title: '84 William', value: 115 },
      { title: 'Park Avenue South', value: 620 }
    ]
  });
}