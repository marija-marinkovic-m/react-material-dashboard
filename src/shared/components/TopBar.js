import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { updateSideNav } from '../../redux/actions';

import cn from 'classnames';

import AppBar from 'material-ui/AppBar';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Popover from 'material-ui/Popover';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import { Card, CardHeader, CardText } from 'material-ui/Card';

import userPic from '../assets/images/Pic.png'; // @TEMP

import { TimelineItem } from '../components/Timeline';

// icons
import MonetizationOnIcon from 'material-ui/svg-icons/editor/monetization-on';
import InfoOutline from 'material-ui/svg-icons/action/info-outline';
import PersonIcon from 'material-ui/svg-icons/social/person';
import SettingsInput from 'material-ui/svg-icons/action/settings-input-svideo';
import HelpOutline from 'material-ui/svg-icons/action/help-outline';
import NofificationBell from 'material-ui/svg-icons/social/notifications';
import TimeIcon from 'material-ui/svg-icons/device/access-time';

import LinearProgress from 'material-ui/LinearProgress';

import { themeColors } from '../../theme.config';
import { routesConfig } from '../../routes';
import './TopBar.css';


import SearchForm, { isSearchPage } from '../components/SearchForm';


class AvatarMenu extends Component {
  static propTypes = {
    history: PropTypes.object,
    triggeredSearch: PropTypes.func
  }
  state = {
    isMainOpen: false,
    isNotificationOpen: false,
    isTimeOpen: false
  }
  handleTouchTap = (prop, event) => {
    event.preventDefault();
    this.setState({
      [prop]: true,
      [`${prop}AnchorEl`]: event.currentTarget
    })
  }
  handleRequestClose = (prop) => {
    this.setState({[prop]: false});
  }

  render() {
    return (
      <div className="mainMenu-dropdown">
        <span className="notifications" style={{display: 'inline-block', verticalAlign: 'top'}}>
          <IconButton
            style={{marginRight: 0}}
            onTouchTap={this.handleTouchTap.bind(null, 'isNotificationOpen')}>
            <NofificationBell color="#4F596A" />
          </IconButton>
          <Popover
            open={this.state.isNotificationOpen}
            anchorEl={this.state.isNotificationOpenAnchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            onRequestClose={this.handleRequestClose.bind(null, 'isNotificationOpen')}>
            <Card>
              <CardHeader title="Reminders" />
              <CardText>...</CardText>
            </Card>
          </Popover>

          <IconButton
            style={{marginRight: '15px'}}
            onTouchTap={this.handleTouchTap.bind(null, 'isTimeOpen')}>
            <TimeIcon color="#4F596A" />
          </IconButton>
          <Popover
            style={{width: '370px', height: '490px'}}
            open={this.state.isTimeOpen}
            anchorEl={this.state.isTimeOpenAnchorEl}
            anchorOrigin={{horizontal: 'left', vertical: 'top'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            onRequestClose={this.handleRequestClose.bind(null, 'isTimeOpen')}>
            <Card>
              <CardHeader title="Notifications" />
              <ListItem disabled={true}>
                {  dummyTimelineNotifications.map((n,i) => <TimelineItem key={i} {...n} />) }
              </ListItem>
            </Card>
          </Popover>
        </span>
        <Avatar src={userPic} onTouchTap={this.handleTouchTap.bind(null, 'isMainOpen')} />
        <Popover
          open={this.state.isMainOpen}
          anchorEl={this.state.isMainOpenAnchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose.bind(null, 'isMainOpen')}>
          <List style={{minWidth: '330px'}}>
            <ListItem disabled={true} style={styles.listItem} className="profileMainInfo">
              <h3>Tiago Silva</h3>
              <span className="secondary-text">thiagos@prodigyassociates.net</span>
            </ListItem>
            <Divider style={styles.divider} />

            <ListItem
              disabled={true}
              style={styles.listItem}
              className="justifiedContent text-uppercase level-type">
              <span className="secondary-text">Your Level</span>
              <span className="levelTypeLabel"><MonetizationOnIcon /> Platinum</span>
            </ListItem>
            <ListItem disabled={true} style={Object.assign({}, styles.listItem, {paddingTop: 0, paddingBottom: '10px'})} className="justifiedContent">
              <LinearProgress
                mode="determinate"
                value={70}
                style={styles.progress}
                color="#A5BED6" />
              <span style={{paddingLeft: '10px'}}>70%</span>
            </ListItem>
            <ListItem disabled={true} style={Object.assign({}, styles.listItem, {paddingTop: 0})} className="justifiedContent">
              <IconButton style={{padding: 0, width: 'auto', height: 'auto'}} tooltip="Honors Level"><InfoOutline color="#A5BED6" /></IconButton>
              <em style={{color: '#A5BED6'}}>$80,000 pending for Honors Level</em>
            </ListItem>
            <Divider style={styles.divider} />
            <ListItem
              className="text-uppercase with-spacing"
              style={Object.assign({}, styles.listItemIcon, {fontFamily: '"AvenirLTStd-Heavy"'})}
              primaryText="Account Settings"
              onTouchTap={e => this.props.history.push('/contacts/add')}
              leftIcon={<PersonIcon color="#A5BED6" />} />
            <Divider style={styles.divider} />
            <ListItem
              className="text-uppercase with-spacing"
              style={styles.listItemIcon}
              primaryText="Preferences"
              leftIcon={<SettingsInput color="#A5BED6" />} />
            <ListItem
              className="text-uppercase with-spacing"
              style={styles.listItemIcon}
              primaryText="Help"
              leftIcon={<HelpOutline color="#A5BED6" />} />
          </List>
        </Popover>
      </div>
    );
  }
}

class TopBar extends Component {
  static propTypes = {
    mediaQueries: PropTypes.object,
    route: PropTypes.object,
    expanded: PropTypes.bool.isRequired,
    onRequestChange: PropTypes.func.isRequired,
    triggeredSearch: PropTypes.bool,
    history: PropTypes.object
  }
  
  constructor(props) {
    super(props);
    this.state = {
      isSearch: isSearchPage(props.route)
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.route.pathname !== !nextProps.route.pathname) {
      const isSearch = isSearchPage(nextProps.route);
      if (this.state.isSearch !== isSearch) {
        this.setState({isSearch});
      }
    }
  }

  render() {
    let currentRoute = routesConfig.filter(r => r.path === this.props.route.pathname);

    const { expanded, mediaQueries, triggeredSearch } = this.props;
    const isMobileView = mediaQueries.screenWidth <= 1280;
    const isLargeScreenView = mediaQueries.screenWidth >= 1920;

    const iconElementLeft = isMobileView ?
      <FontIcon
        className={cn(['pna'], {'icon-sideNavCollapse': this.props.expanded, 'icon-sideNavExpand': !this.props.expanded})}
        style={{marginTop: '11px'}}
        color={this.props.expanded ? 'transparent' : themeColors.topBarBg}
        onClick={this.props.onRequestChange.bind(null, {expanded: !this.props.expanded})} /> :
      <SearchForm style={{width: isLargeScreenView ? '1300px' : `calc(100vw - 555px)`}} />;

    const iconElementRight = isMobileView ?
      <SearchForm /> :
      triggeredSearch ? null : <AvatarMenu history={this.props.history} />

    let appBarProps = {
      className: cn('app-bar', {expanded}),
      style: Object.assign({}, styles.appBar, {
          left: isMobileView ? 0 : '73px',
          backgroundColor: isMobileView ? triggeredSearch ? themeColors.white : themeColors.darkGray : themeColors.topBarBg,
          position: isMobileView ? 'fixed' : 'absolute'
      }),
      iconElementLeft,
      iconElementRight
    }

    if (isMobileView) {
      appBarProps = Object.assign({}, appBarProps, {
        title: currentRoute.length ? currentRoute[0].appBarTitle : '',
        titleStyle: styles.appBarTitle
      })
    }

    if (this.state.isSearch) {
      appBarProps = Object.assign({}, appBarProps, {iconElementRight: null});
    }

    return(
      <AppBar {...appBarProps} />
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  expanded: state.sideNavController.expanded,
  route: state.router.location,
  triggeredSearch: state.searchController.triggered
});
const mapDispatchToProps = dispatch => ({
  onRequestChange: bindActionCreators(updateSideNav, dispatch)
});
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(TopBar));

//////
const dummyTimelineNotifications = [
  {
    "createdAt": "20 June",
    "type": "invitationSent",
    "content": "<p class=\"text-oblique p-lg\">Congratulation! Ronnie has been invited to invest.</p>"
  },
  {
    "createdAt": "13 June",
    "type": "dealCreated",
    "content": "<p class=\"text-oblique p-lg\">How Awesome! Ronnie Chamber’s Opportunity: <strong>The Assemblage/Park Avenue South</strong> for <strong>$20.000,00</strong> has been approved as investment.</p>"
  },
  {
    "createdAt": "5 June",
    "type": "dealStatusChanged",
    "content": "<p class=\"text-oblique p-lg\">Looking Good! Ronnie Chamber’s Investment status on <strong>The Assemblage/Park Avenue South</strong> for <strong>$10.000,00</strong> has chaged from <span class=\"text-orange\">On Hold</span> to <span class=\"text-green\">Started Investment Flow</span>.</p>"
  },
  {
    "createdAt": "4 June",
    "type": "generic",
    "content": "<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>"
  },
  {
    "createdAt": "1 June",
    "type": "fundsReceived",
    "content":"<p class=\"text-oblique p-lg\">Yes! The funds has been received.</p>"
  }
];
const styles = {
  appBar: {
    width: 'auto',
    right: 0,
    paddingLeft: '40px',
    paddingRight: '40px',
    left: '73px',
    textTransform: 'uppercase',
    // height: '64px', overflow: 'hidden'
  },
  appBarTitle: {
    textAlign: 'center',
    fontSize: '15px',
    fontFamily: '"Avenir LT 85 Heavy", sans-serif'
  },
  divider: {
    marginTop: '14px',
    marginBottom: '14px'
  },
  listItem: {
    paddingLeft: '35px',
    paddingRight: '35px', fontSize: '10px'
  },
  listItemIcon: {paddingLeft: '15px', paddingRight: '15px', fontSize: '10px'},
  progress: {height: '8px', borderRadius: '100px', background: '#ECEFF2'}
}