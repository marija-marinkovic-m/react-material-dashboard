import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { Link } from 'react-router-dom';

import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import Dialog from 'material-ui/Dialog';

import Snackbar from 'material-ui/Snackbar';

import FileDownload from 'material-ui/svg-icons/file/file-download';
import OutlineCircle from '../util/svg-icons/outline-circle';
import ChevronDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import TollIcon from 'material-ui/svg-icons/action/toll';
import PlaneIcon from '../util/svg-icons/plane';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import FormatQuote from 'material-ui/svg-icons/editor/format-quote';
import ForumIcon from 'material-ui/svg-icons/communication/forum';
import EditorMonetizationOn from 'material-ui/svg-icons/editor/monetization-on';
import DescriptionIcon from 'material-ui/svg-icons/action/description';
import Money from 'material-ui/svg-icons/editor/attach-money';

import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import Info from 'material-ui/svg-icons/action/info-outline';

import { themeColors, themeInputProps, customFlatButton } from '../../theme.config';

import './Timeline.css';

export default class TimelineTab extends Component {
  state = {
    isOpenFilterBar: false,
    isOpenDialog: false,
    isOpenContactAutocomplete: false,
    isOpenSnackbar: false,
    addLogForm: {
      category: '',
      description: '',
      contactSearchText: '',
      contact: null
    },
    snackbarAction: 'undo',
    snackbarMessage: ''
  }
  static propTypes = {
    content: PropTypes.object,
    style: PropTypes.object,
    maxWidth: PropTypes.string,
    onOpenDialog: PropTypes.func
  }

  static defaultProps = {
    content: null,
    style: null,
    maxWidth: '100%',
    onOpenDialog: isOpen => false
  }

  renderItems = (list) => (
    <ul>
      { list.map((item, index) => <li key={index}>
        <TimelineItem {...item} />
      </li>) }
    </ul>
  );

  toggleContactAutocomplete = e => {
    e.preventDefault();
    this.setState({isOpenContactAutocomplete: !this.state.isOpenContactAutocomplete})
  }

  handleFilterToggle = isOpenFilterBar => this.setState({isOpenFilterBar})
  handleOpenDialog = () => this.setState({isOpenDialog: true}, () => this.props.onOpenDialog(true))
  handleCloseDialog = () => this.setState({isOpenDialog: false}, () => this.props.onOpenDialog(false))


  // data form updates 
  handleUpdateContactAutoCompleteInput = contactSearchText => {
    this.setState({addLogForm: Object.assign({}, this.state.addLogForm, {contactSearchText})})
  }
  handleNewContactAutoCompleteRequest = (chosenRequest, index) => {
    if (index > -1) {
      this.setState({addLogForm: Object.assign({}, this.state.addLogForm, {contact: chosenRequest})})
    }
  }
  handleLogContentInputChange = e => this.setState({addLogForm: Object.assign({}, this.state.addLogForm, {description: e.target.value})});
  handleLogCategoryChange = (event, key, payload) => this.setState({addLogForm: Object.assign({}, this.state.addLogForm, {category: key})})

  // snackbar
  handleSnackbarClose = () => this.setState({isOpenSnackbar: false});
  handleSnackbarAction = () => {
    this.setState({
      snackbarMessage: 'Log removed from your timeline',
      snackbarAction: null,
      isOpenSnackbar: true
    });
  }

  handleSubmitLog = e => {
    e.preventDefault();

    this.setState({
      snackbarMessage: 'Log Successfully Created', // JSON.stringify(this.state.addLogForm),
      snackbarAction: 'undo',
      isOpenDialog: false,
      addLogForm: {
        category: '',
        description: '',
        contactSearchText: '',
        contact: null
      }
    }, () => this.setState({isOpenSnackbar: true}))
  }


  render() {
    const { content, maxWidth, style } = this.props;
    const { addLogForm } = this.state;
    if (!content || !Object.keys(content).length) {
        return <div />;
    }

    const apiContacts = contacts.map(i => Object.assign({}, i, {fullName: i.firstName + ' ' + i.lastName}))

    return (
      <div>
        <TimelineFilters
          onToggleVisibility={this.handleFilterToggle}
          onAddLog={this.handleOpenDialog}
          maxWidth={maxWidth} />
        <div style={style} className="main-placeholder timelines">

          <div style={{paddingBottom: this.state.isOpenFilterBar ? '221px' : 0 }}>
          { Object.keys(content).map((key, index) => {
            const data = content[key] && content[key].response && content[key].response.data;
            if (!data || !data.length) return null;
            return (<div key={index} style={{maxWidth, paddingRight: maxWidth !== '100%' ? '75px' : 0, boxSizing: 'border-box'}}>
              <h4 className={cn({'period-title': index !== 0})}>
                <span className="text">{key}</span>
                <span className="line" />
              </h4>
              { this.renderItems(data) }
            </div>);
          })}
          </div>

        </div>

        <Dialog
          title="Add Log to Timeline"
          actions={[
            <FlatButton label="Cancel" onClick={this.handleCloseDialog} style={customFlatButton.secondary} />,
            <FlatButton label="Add" onClick={this.handleSubmitLog} style={customFlatButton.primary} />            
          ]}
          modal={false}
          contentStyle={styles.dialog}
          open={this.state.isOpenDialog}
          onRequestClose={this.handleCloseDialog}>
            <div>
            <SelectField value={addLogForm.category} {...Object.assign({}, themeInputProps.textField, {
              onChange: this.handleLogCategoryChange,
              className: 'pnaTextFieldDark',
              width: '100%',
              id: 'pna-logCategory'
            })}>
              <MenuItem value="" primaryText="Pick a category..." />
              <MenuItem value={1} primaryText="Type1" />
              <MenuItem value={2} primaryText="Type2" />
            </SelectField>
            <TextField {...Object.assign({}, themeInputProps.textField, {
              className: 'pnaTextFieldDark',
              id: 'new-log-content',
              value: addLogForm.description,
              onChange: this.handleLogContentInputChange
            })} />

            { 
              !this.state.isOpenContactAutocomplete ? 
              <a href="#reffer-to-contact" onClick={this.toggleContactAutocomplete}>Reffer to Contact</a> : 
              <AutoComplete {...Object.assign({}, themeInputProps.textField, {
                filter: AutoComplete.caseInsensitiveFilter,
                className: 'pnaTextFieldDark',
                searchText: addLogForm.contactSearchText,
                onUpdateInput: this.handleUpdateContactAutoCompleteInput,
                onNewRequest: this.handleNewContactAutoCompleteRequest,
                id: 'pna-logContact',
                dataSource: apiContacts,
                dataSourceConfig: { text: 'fullName', value: 'slug' },
                fullWidth: true})} />
            }
            </div>
        </Dialog>

        <Snackbar
          open={this.state.isOpenSnackbar}
          message={this.state.snackbarMessage}
          action={this.state.snackbarAction}
          onRequestClose={this.handleSnackbarClose}
          onActionTouchTap={this.handleSnackbarAction}

          {...themeInputProps.snackbar} />
      </div>
    )
  }
}
export class TimelineItem extends Component {
  static propTypes = {
    createdAt: PropTypes.string,
    type: PropTypes.string,
    content: PropTypes.string,
    user: PropTypes.object
  }

  render() {
    const {createdAt, type, content, user} = this.props;

    const isType = timelineFilters[type];
    const itemCircle = <OutlineCircle color={isType ? isType.color : themeColors.blue} viewBox="10 0 24 24" style={{width: '30px', height: '16px', verticalAlign: 'middle'}} />
    const itemTitle = isType ?
      (<span>
        <i className="ico">{ isType.icon }</i>
        { isType.label }
        { user && user.slug && <span className="time">&#8226;&nbsp;<Link to={`/contacts/${user.slug}`}>{user.firstName + ' ' + user.lastName}</Link></span> }
        <span className="time">&#8226;&nbsp;9:20 AM</span>
      </span>)
     : type;
    return (
      <div className="timeline-item">
        <span className="date-time">
          { itemCircle }
          <time dateTime={createdAt}>{ createdAt }</time>
        </span>
        <div className="content">
          <h5>{itemTitle}</h5>
          <div dangerouslySetInnerHTML={{__html: content}} />
        </div>
      </div>
    );
  }
}
class TimelineFilters extends Component {
  static propTypes = {
    onToggleVisibility: PropTypes.func,
    onAddLog: PropTypes.func,
    maxWidth: PropTypes.string
  }
  static defaultProps = {
    onAddLog: () => false
  }
  state = {
    isOpen: false
  }
  toggleIsOpen = () => {
    this.setState({
      isOpen: !this.state.isOpen
    }, () => {
      if (this.props.onToggleVisibility) this.props.onToggleVisibility(this.state.isOpen);
    });
  }
  renderCheckboxes = () => {
    if (!this.state.isOpen) {
      return;
    }
    const { maxWidth } = this.props;
    return (
      <div className="filters-wrapper placeholder" style={{maxWidth}}>
        { Object.keys(timelineFilters).map((type,index) => {
          return <div key={index} className="filter-item"><Checkbox {...themeInputProps.checkbox} label={timelineFilters[type].label} /></div>;
        }) }
      </div>
    );
  }
  render() {
    const { isOpen } = this.state;
    const { maxWidth } = this.props;
    const textColor = isOpen ? '#FFF' : themeColors.contactFiltersDrawerBg;
    return (
      <section
        className={cn(['timeline-filters', {isOpen}])}>

        <div className="placeholder" style={{maxWidth}}>
          <FlatButton
            hoverColor="transparent"
            label="Filter View" icon={<ChevronDown color={textColor} />}
            onClick={this.toggleIsOpen}
            labelStyle={{color: textColor, fontSize: '10px'}} />
          <FlatButton label="Reset" labelStyle={{color: themeColors.blue, fontSize: '10px'}} />
          { !isOpen && <FlatButton
            label="Add Log"
            style={{float: 'right', fontSize: '10px'}}
            icon={<FileDownload />}
            onClick={this.props.onAddLog} /> }
        </div>

        {this.renderCheckboxes()}
      </section>
    );
  }
}

const styles = {
  dialog: {
    width: '100%',
    maxWidth: 375
  }
}

export const timelineFilters = {
  statusChange: {label: 'Status Change', color: themeColors.gold, icon: <TollIcon />},
  invitationSent: {label: 'Invitation Sent', color: themeColors.orange, icon: <PlaneIcon style={{height: '12px'}} />},
  dealCreated: {label: 'Deal Created', color: themeColors.green, icon: <ThumbUp style={{height: '12px'}} />},
  dealStatusChanged: {label: 'Deal Status Change', color: '#3695B2', icon: <ThumbUp style={{height: '12px'}} />},
  generic: {label: 'Generic timeline record', color: themeColors.lightGray, icon: <FormatQuote />},
  messageSent: {label: 'Message Sent', color: '#F46DD8', icon: <ForumIcon />},
  fundsReceived: {label: 'Funds Received', color: '#46D6E0', icon: <EditorMonetizationOn />},
  noteEdited: {label: 'Note Edited', color: '#4ACBFF', icon: <DescriptionIcon />},

  saleStarted: {label: 'Sale Started', color: themeColors.green, icon: <Money />},
  saleStatusChange: {label: 'Sale Status Change', color: themeColors.statusBlue, icon: <Money />},
  commissionPayment: {label: 'Commission Payment', color: themeColors.blue, icon: <Info />}
}



const contacts = [
  {
    firstName: "Helena",
    lastName: "Cooper",
    slug: "helena-cooper",
    otherData: {etc: ''}
  },
  {
    firstName: "Ronnie",
    lastName: "Cambers",
    slug: "ronnie-chambers"
  },
  {
    firstName: "Josephine",
    lastName: "Gomez",
    slug: "josephine-gomez"
  }
]