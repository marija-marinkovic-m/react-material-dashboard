import React, { Component } from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';

import ProfileAvatar from '../components/ProfileAvatarTableListItem';
import {
  Table,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TableBody from '../components/CustomTableBody';
import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator,
  ToolbarTitle
} from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import Popover from 'material-ui/Popover';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import Divider from 'material-ui/Divider';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import Checkbox from 'material-ui/Checkbox';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';

import ChipInput from '../util/ChipInput';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';
import Dialog from 'material-ui/Dialog';
import Snackbar from 'material-ui/Snackbar';

// icons
import StatusCircle from '../util/svg-icons/statusCircle';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ToggleStar from 'material-ui/svg-icons/toggle/star';
import ContentAdd from 'material-ui/svg-icons/content/add';
import NavigationMoreVert from 'material-ui/svg-icons/navigation/more-vert';
import MoreHoriz from 'material-ui/svg-icons/navigation/more-horiz';
import Label from 'material-ui/svg-icons/action/label';
import EditorMonetizationOn from 'material-ui/svg-icons/editor/monetization-on';
import ArrowDownward from 'material-ui/svg-icons/navigation/arrow-downward';
import NoteAdded from 'material-ui/svg-icons/action/note-add';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import ForumIcon from 'material-ui/svg-icons/communication/forum';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import Warning from 'material-ui/svg-icons/alert/warning';

import CircularProgress from 'material-ui/CircularProgress';
import { CheckboxCheckedBlue as CheckboxCheckedIcon, CheckboxUncheckedIcon } from '../util/svg-icons/checkbox';

import { themeColors, themeInputProps, customFlatButton } from '../../theme.config';

import { timelineFilters } from './Timeline';


import './Contacts.css';

export class ContactsToolbar extends Component {
  state = {
    isOpenDeleteDialog: false
  }
  static propTypes = {
    onDeleteContacts: PropTypes.func,
    currentContactType: PropTypes.string.isRequired,
    settingsOpen: PropTypes.bool,
    handleFilterContacts: PropTypes.func.isRequired,
    handleSettingsDrawerToggle: PropTypes.func.isRequired,
    actions: PropTypes.node,
    selectedItems: PropTypes.array,
    totalItemsText: PropTypes.string //ie. 11 total contacts, 8 total results etc.
  }

  static defaultProps = {
    totalItemsText: '',
    onDeleteContacts: contacts => false
  }

  handleOpenDeleteDialog = () => this.setState({isOpenDeleteDialog: true})
  handleCloseDeleteDialog = () => this.setState({isOpenDeleteDialog: false})
  async handleDeleteContacts() {
    await this.props.onDeleteContacts(this.props.selectedItems)
    this.handleCloseDeleteDialog();
  }

  renderToolbarMainContent() {
    const { selectedItems, currentContactType, handleFilterContacts, totalItemsText } = this.props;
    const dropDownMenuStyles = {
      iconButton: <NavigationArrowDropDown color="#FFF" />,
      iconStyle: {left: '16px', right: 'auto'},
      labelStyle: {paddingRight: '24px', paddingLeft: '56px', color: '#FFF', fontSize: '10px', textTransform: 'uppercase'},
      style: {marginRight: 0, height: '56px'}
    }
    return(
      <ToolbarGroup firstChild={true}>
        { (() => {
          if (!selectedItems.length) {
            return(
              <ToolbarGroup firstChild={true}>
                <DropDownMenu
                  value={currentContactType}
                  onChange={handleFilterContacts}
                  selectedMenuItemStyle={{color: 'rgba(0, 0, 0, 0.87)'}}
                  {...dropDownMenuStyles}>
                  {
                    filteringData.contactTypes.map((m,i) => {
                      const curr = m === currentContactType;
                      const itemProps = {
                        value: m,
                        primaryText: m,
                        key: i,
                        insetChildren: !curr,
                        checked: curr,
                        style: {textTransform: 'uppercase', fontSize: '10px', fontFamily: "'AvenirLTStd-Heavy', sans-serif"}
                      };
                      return <MenuItem {...itemProps} />;
                    })
                  }
                </DropDownMenu>
                { totalItemsText.length > 0 && <ToolbarSeparator style={{marginLeft: 0, backgroundColor: themeColors.mediumGray, height: '12px'}} /> }
                { totalItemsText.length > 0 && <ToolbarTitle style={styles.toolbarTitle} text={totalItemsText} /> }
              </ToolbarGroup>
            );
          } else {
            return(
              <ToolbarGroup firstChild={true}>
                <ToolbarTitle style={Object.assign({}, styles.toolbarTitle, {marginLeft: '50px'})} text={`${selectedItems.length} selected`} />
                <DropDownMenu
                  value="Change Status"
                  {...dropDownMenuStyles}>
                  <MenuItem disabled={true} value="Change Status" primaryText="Change Status" />
                  {
                    filteringData.statuses.map((s,i) => {
                      const curr = s === false;
                      const itemProps = {
                        value: s.label, primaryText: s.label, key: i, checked: curr
                      };
                      return <MenuItem {...itemProps} />;
                    })
                  }
                </DropDownMenu>

                <TagsBulkManagement selectedItemsLenght={selectedItems.length} />

                <FlatButton
                  label="Send Message"
                  labelStyle={styles.bulkActionsButton}
                  icon={<ForumIcon color="#FFF" style={{height: '12px'}} />} />

                <FlatButton
                  onClick={this.handleOpenDeleteDialog}
                  label="Delete"
                  labelStyle={styles.bulkActionsButton}
                  icon={<DeleteIcon color="#FFF" style={{height: '13px'}} />} />

                <IconButton tooltip="More">
                  <NavigationMoreVert color="#FFF" />
                </IconButton>


                {/*delete dialog  */}
                <Dialog
                  contentStyle={styles.dialog.warning}
                  title={<h3>Deleting Contacts <Warning style={{float: 'right'}} color={themeColors.red} /></h3>}
                  open={this.state.isOpenDeleteDialog}
                  actions={[<FlatButton
                    label="Cancel"
                    style={customFlatButton.secondary}
                    onClick={this.handleCloseDeleteDialog} />, 
                    <FlatButton
                      label="Yes, Delete"
                      style={customFlatButton.warn}
                      onClick={this.handleDeleteContacts.bind(this)} />
                  ]}
                  modal={true}
                  contentClassName="pna-dialog warn-dialog">
                  <p><strong>Warning: This can not be undone.</strong></p>
                  <p>Are you sure you want to permanently delete <strong>{ this.props.selectedItems.length } selected contacts</strong> with all of their information including messages, notes, and timeline?</p>
                </Dialog>
              </ToolbarGroup>
            );
          }
        })() }
        
      </ToolbarGroup>
    );
  }

  render() {
    const {
      settingsOpen,
      handleSettingsDrawerToggle,
      actions
    } = this.props;
    return (
      <Toolbar
        style={styles.toolbar}>
        { this.renderToolbarMainContent() }
        <ToolbarGroup>
          { actions }
          <FontIcon
            className="goldHover pna icon-filter_settings"
            color={settingsOpen ? themeColors.gold : 'white'}
            style={{fontSize: '17px'}}
            onTouchTap={handleSettingsDrawerToggle} />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

export class ContactsDrawer extends Component {
  static propTypes = {
    drawerStyle: PropTypes.object.isRequired,
    drawerWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    settingsOpen: PropTypes.bool,
    mobileCloseBtn: PropTypes.node,
    currentFilterAmount: PropTypes.string
  }
  render() {
    const { drawerStyle, drawerWidth, settingsOpen, currentFilterAmount, mobileCloseBtn } = this.props;

    return (
      <Drawer
        width={drawerWidth}
        docked={true}
        openSecondary={true}
        open={settingsOpen}
        className="filter-drawer"
        containerStyle={drawerStyle}>

        { mobileCloseBtn }

        <a className="filters-reset" href="#reset">Reset</a>

        <h4 className="filters-title">Filter By</h4>

        <div className="widget">
          <Checkbox
            {...themeInputProps.checkbox}
            label={<span>Favorites&nbsp;<ToggleStar color={themeColors.gold} style={{marginLeft: '10px', display: 'inline-block', verticalAlign: 'middle', top: '-2px', position: 'relative'}} /></span>} />
        </div>

        <div className="widget">
          <h5>Status</h5>
          { filteringData.statuses.map((s,i) => <Checkbox
            {...Object.assign({}, themeInputProps.checkbox, {labelStyle: {fontSize: '10px', textTransform: 'uppercase', letterSpacing: '1px', color: '#FFFFFF', lineHeight: '20px'}})}
            key={i}
            label={<span><Avatar size={15} style={{marginRight: '15px', display: 'inline-block', verticalAlign: 'middle', position: 'relative', top: '-2px'}} backgroundColor={s.color} />{s.label}</span>} />) }
        </div>

        <div className="widget">
          <h5>Amount</h5>
          <DropDownMenu
            value={currentFilterAmount}
            {...themeInputProps.dropdown}
            onChange={this.handleFilterAmounts}>
            { filteringData.amounts.map((a,i) => {
              const curr = a === currentFilterAmount;
              const itemProps = {
                value: a,
                primaryText: a,
                key: i,
                insetChildren: !curr,
                checked: curr
              };
              return <MenuItem {...itemProps} />;
            }) }
          </DropDownMenu>

          <RadioButtonGroup name="amount" defaultSelected={0}>
            { filteringData.amountDirection.map((a,i) => <RadioButton
              {...themeInputProps.radio}
              style={{display: 'inline-block', width: 'auto', marginRight: '17px'}}
              name="amountDirection"
              value={i}
              key={i}
              label={a} />) }
          </RadioButtonGroup>
        </div>
        

        <div className="widget">
          <h5>Offerings</h5>
          { filteringData.offerings.map((o,i) => <Checkbox
            {...Object.assign({}, themeInputProps.checkbox, {fontWeight: 'normal', fontSize: '12px', letterSpacing: '0px'})}
            key={i}
            label={o} />) }
        </div>

        <div className="widget">
          <h5>Tags</h5>
          <ChipInput
            defaultValue={filteringData.tags}
            fullWidth={true} />
        </div>

      </Drawer>
    );
  }
}

export class ContactsTable extends Component {
  state = {
    selectable: false,
    selectedRows: [],
    currentRowHover: null,
    columnOptionsAnchorEl: null,
    columnOptionsOpen: false,

    // table scroll stuff
    tableWidth: 0,
    tableWrapWidth: 0,
    tableScrollLeft: 0
  }
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired,
    selectedItems: PropTypes.array,
    onSelectItem: PropTypes.func,
    mainWrapStyle: PropTypes.object,
    onContactDelete: PropTypes.func,
    sideNavExpanded: PropTypes.bool,
    screenWidth: PropTypes.number
  }
  static defaultProps = {
    onSelectItem: (selectedItems) => selectedItems,
    selectedItems: []
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedItems !== this.props.selectedItems) {
      this.setState({selectable: Boolean(nextProps.selectedItems.length)})
    }
    if (nextProps.screenWidth !== this.props.screenWidth) {
      this.setTableWidth(this.props);
    }
    if (nextProps.sideNavExpanded !== this.props.sideNavExpanded) {
      setTimeout(this.setTableWidth.bind(null, nextProps), 1200);
    }
  }

  componentDidMount() {
    setTimeout(this.setTableWidth.bind(null, this.props), 1200);
  }

  setTableWidth = (props) => {
    if (!this._table || !this._table.refs || !this._tableWrapRef) return;
    const tableWidth = this._table.refs.tableBody.getClientRects()[0].width;
    let tableWrapWidth = this._tableWrapRef.getClientRects()[0].width;

    if (this.props.screenWidth <= 1280 && props.sideNavExpanded) {
      tableWrapWidth += 255;
    }
    this.setState({tableWidth, tableWrapWidth});
  }

  openContact = (slug, e) => this.props.history.push(`/contacts/${slug}`)
  toggleSelectable = (selectable = false, rowNumber = null, e) => {
    if (selectable === false) {
      if (rowNumber === null) this.props.onSelectItem(this.state.selectedRows);
      if (this.state.selectedRows.length > 0) return;
    }
    this.setState({selectable}, () => {
      if (rowNumber) {
        this.setState({selectedRows: [rowNumber]}, () => {
          this.props.onSelectItem(this.state.selectedRows);
        });
      }
    })
  }
  handleRowSelection = (selected) => {
    this.setState({selectedRows: selected
      .filter((item, index) => selected
        .slice(0, index)
        .concat(selected.slice(index + 1))
        .indexOf(item) > -1 ? false : item
      )}, () => {
      this.toggleSelectable();
    })
  }
  handleRowHover = (currentRowHover) => this.setState({currentRowHover})
  handleRowHoverExit = (rowHoverExit) => {
    if (this.state.currentRowHover === rowHoverExit) {
      this.setState({currentRowHover: null});
    }
  }

  getSelectedRows = () => console.log(this._tableBodyRef.state.selectedRows);
  isRowSelected = (rowNumber) => {
    if (this.props.selectedItems.filter(i => i === rowNumber).length) return true;
    if (this.state.selectedRows.filter(i => i === rowNumber).length) return true; 
    return false;
  }
  isRowHovered = (rowNumber) => {
    if (this.state.currentRowHover === rowNumber) return true;
    return false;
  }

  // column options
  handleColumnOptionsTouchTap = (e) => {
    e.preventDefault();
    this.setState({columnOptionsOpen: true, columnOptionsAnchorEl: e.currentTarget})
  }
  handleColumnOptionsRequestClose = () => {
    this.setState({columnOptionsOpen: false})
  }

  handleTableWrapScroll = e => {
    const event = e || window.event;
    this.setState({tableScrollLeft: event.target.scrollLeft})
  }

  render() {
    const { contacts, mainWrapStyle, screenWidth } = this.props;
    const { selectable, tableWidth, tableWrapWidth, tableScrollLeft } = this.state;
    const checkboxProps = {
      className: 'columnOptCheckbox',
      checkedIcon: <CheckboxCheckedIcon style={{width: '12px', height: '12px', fill: themeColors.blue}} />,uncheckedIcon: <CheckboxUncheckedIcon style={{width: '15px', height: '15px', fill: '#BFCCDC'}} />,
      labelPosition: 'left'
    }
    const columnOptListItemProps = {
      style: styles.columnOptionsListItem,
      disabled: true
    }

    const optionsRowTranslateY = tableWidth > tableWrapWidth ? -(tableWidth - tableWrapWidth - tableScrollLeft) : 0;
    return (
      <div
        ref={tableWrapEl => this._tableWrapRef = tableWrapEl}
        style={mainWrapStyle}
        onScroll={this.handleTableWrapScroll}>
      <Table
        ref={table => this._table = table}
        className="contacts-table"
        style={{width: 'auto', minWidth: '100%'}}
        bodyStyle={{overflowX: 'auto', height: '100%'}}
        wrapperStyle={{overflow: 'hidden', height: '100%'}}
        selectable={selectable}
        multiSelectable={true}
        onRowSelection={this.handleRowSelection}
        onRowHover={this.handleRowHover}
        onRowHoverExit={this.handleRowHoverExit}>

        <TableBody
          displayRowCheckbox={selectable} showRowHover={true}>
          <TableRow
            className={cn(['head-row', {selectable, hovered: this.isRowHovered(0)}])}>
            <TableRowColumn style={{paddingLeft: '40px'}}><ListItem style={styles.headRowItem} disabled={true} primaryText="Contact" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Status" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Opportunities" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Last Activity" /></TableRowColumn>
            <TableRowColumn><ListItem disabled={true} style={styles.headRowItem} primaryText="Tags" /></TableRowColumn>
            <TableRowColumn style={{position: 'relative', transform: `translate(${optionsRowTranslateY}px, 0px)`}}>
              <IconButton className="recompose-cols" iconStyle={{width: '17px', color: themeColors.mediumGray}} onTouchTap={this.handleColumnOptionsTouchTap}>
                <ContentAdd />
              </IconButton>
              <Popover
                style={{width: '220px', paddingTop: '20px', paddingBottom: '15px'}}
                open={this.state.columnOptionsOpen}
                anchorEl={this.state.columnOptionsAnchorEl}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                onRequestClose={this.handleColumnOptionsRequestClose}>
                <ListItem
                  primaryText={<span><ToggleStar color={themeColors.gold} style={{position: 'absolute', left: '70px', top: '5px'}} /><Checkbox checked={true} label="Favorite" {...checkboxProps} /></span>}
                  {...columnOptListItemProps} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox checked={true} label="Contacts" {...checkboxProps} />} />
                <ListItem 
                  {...columnOptListItemProps}
                  primaryText={<Checkbox checked={true} label="Status" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Follow Up" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Address" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Email" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Phone" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Skype" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Closed Investments" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox checked={true} label="Opportunities" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Description" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Date Added" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Last Activity" {...checkboxProps} />} />
                <ListItem
                  {...columnOptListItemProps}
                  primaryText={<Checkbox label="Tags" {...checkboxProps} />} />
              </Popover>
            </TableRowColumn>
          </TableRow>
          { contacts.map((contact,i) => {
            const rowNumber = i+1;
            const hovered = this.isRowHovered(rowNumber);
            const selected = this.isRowSelected(rowNumber);

            return (
              <TableRow
                style={{position: 'relative', zIndex: 333}}
                rowNumber={rowNumber}
                key={rowNumber}
                hoverable={true}
                selected={selected}
                hovered={hovered}
                className={cn(['pna-rows'], {selectable, selected, hovered})}>
                <TableRowColumn>
                  <ProfileAvatar
                    contact={contact}
                    rowNumber={rowNumber}
                    handleAvatarClick={this.toggleSelectable}
                    onToggleFeatured={null}
                    openContact={this.openContact}
                    hovered={hovered}
                    screenWidth={screenWidth} />
                </TableRowColumn>
                <TableRowColumn style={{position: 'relative', zIndex: 1}}>
                  <ContactStatus contact={contact} />
                </TableRowColumn>
                <TableRowColumn>
                  <ContactOpportunities items={contact.opportunities} />
                </TableRowColumn>
                <TableRowColumn>
                  <ContactLatestActivity items={contact.lastActivity} />
                </TableRowColumn>
                <TableRowColumn>
                  <ContactsTags items={contact.tags} hovered={hovered} />
                </TableRowColumn>
                <TableRowColumn className="more-vert-cell" style={{position: 'relative', zIndex: 9, transform: `translate(${optionsRowTranslateY}px, 0px)`, padding: 0, textAlign: 'right'}}>
                  <ContactTableMoreVertMenu screenWidth={screenWidth} hovered={hovered} contact={contact} onDelete={this.props.onContactDelete} />
                </TableRowColumn>
              </TableRow>
            )
          }) }
        </TableBody>
      </Table>
      </div>
    );
  }
}

export class ContactTableCellBase extends Component {
  state = {
    popoverOpen: false
  }
  static propTypes = {
    items: PropTypes.any,
    hovered: PropTypes.bool
  }
  static defaultProps = {
    items: null,
    hovered: false
  }
  handleTouchTap = (event) => {
    event.preventDefault();
    this.setState({
      popoverOpen: true,
      anchorEl: event.currentTarget
    })
  }
  handleRequestClose = () => {
    this.setState({popoverOpen: false})
  }
}

export class ContactTableMoreVertMenu extends Component {
  state = {
    dialogOpen: false,
    isOpenDeleteDialog: false
  }
  static propTypes = {
    hovered: PropTypes.bool,
    onDelete: PropTypes.func,
    contact: PropTypes.object.isRequired,
    screenWidth: PropTypes.number
  }
  static defaultProps = {
    hovered: false,
    onDelete: contact => false
  }
  handleOpenDialog = () => this.setState({dialogOpen: true});
  handleCloseDialog = () => this.setState({dialogOpen: false});

  handleOpenDeleteDialog = () => this.setState({isOpenDeleteDialog: true})
  handleCloseDeleteDialog = () => this.setState({isOpenDeleteDialog: false})
  async handleDeleteContact() {
    await this.props.onDelete(this.props.contact);
    this.handleCloseDeleteDialog();
  }

  render() {
    const { hovered, screenWidth } = this.props;
    const dialogActions = [
      <FlatButton label="Cancel" primary={true} onTouchTap={this.handleCloseDialog} />,
      <FlatButton label="Submit" primary={true} keyboardFocused={true} onTouchTap={this.handleCloseDialog} />
    ]

    const isMobileView = screenWidth <= 768;
    return(
      <div style={{display: 'inline-block', width: isMobileView ? '100%' : '56px', backgroundColor: hovered || isMobileView ? themeColors.white : 'transparent', textAlign: 'center', padding: '24px 12px'}}>
        <IconMenu
        style={{opacity: hovered || isMobileView ? 1 : 0, pointerEvents: hovered || isMobileView ? 'auto' : 'none'}}
        iconButtonElement={<IconButton><NavigationMoreVert color="black" /></IconButton>}
        useLayerForClickAway={true}>
          <MenuItem
            onTouchTap={this.handleOpenDialog}
            primaryText="Edit Tags" style={{fontSize: '10px', textTransform: 'uppercase'}} leftIcon={<Label color="#BFCCDC" />} />
          <MenuItem primaryText="Invite to Invest" style={{fontSize: '10px', textTransform: 'uppercase'}} leftIcon={<EditorMonetizationOn color="#BFCCDC" />} />
          <MenuItem primaryText="Add Note" style={{fontSize: '10px', textTransform: 'uppercase'}} leftIcon={<NoteAdded color="#BFCCDC" />} />
          <MenuItem primaryText="Add Log" style={{fontSize: '10px', textTransform: 'uppercase'}} leftIcon={<ArrowDownward color="#BFCCDC" />} />
          <MenuItem primaryText="Send Messages" style={{fontSize: '10px', textTransform: 'uppercase'}} leftIcon={<ForumIcon color="#BFCCDC" />} />
          <Divider />
          <MenuItem
            primaryText="Delete Contact"
            style={{fontSize: '10px', textTransform: 'uppercase', color: '#BFCCDC'}}
            leftIcon={<DeleteIcon color="#BFCCDC" />}
            onClick={this.handleOpenDeleteDialog} />
        </IconMenu>

        {/*popover options  */}
        <Dialog
          title={<h3>Edit Tags</h3>}
          modal={false}
          actions={dialogActions}
          open={this.state.dialogOpen}
          onRequestClose={this.handleCloseDialog}
          contentStyle={{width: '380px'}}>
          <ChipInput
            defaultValue={filteringData.tags}
            fullWidth={true} />
          <p><small>1 contact selected</small></p>
        </Dialog>

        {/*delete dialog  */}
        <Dialog
          contentStyle={styles.dialog.warning}
          title={<h3>Deleting Contact <Warning style={{float: 'right'}} color={themeColors.red} /></h3>}
          open={this.state.isOpenDeleteDialog}
          actions={[<FlatButton
            label="Cancel"
            style={customFlatButton.secondary}
            onClick={this.handleCloseDeleteDialog} />, 
            <FlatButton
              label="Yes, Delete"
              style={customFlatButton.warn}
              onClick={this.handleDeleteContact.bind(this)} />
          ]}
          modal={true}
          contentClassName="pna-dialog warn-dialog">
          <p><strong>Warning: This can not be undone.</strong></p>
          <p>Are you sure you want to permanently delete <strong>{ this.props.contact.firstName + ' ' + this.props.contact.lastName }</strong> with all of its information including messages, notes, and timeline?</p>
        </Dialog>
      </div>
    );
  }
}

export class ContactsTags extends ContactTableCellBase {
  _styles = {
    chip: {
      marginRight: '2px',
      display: 'inline-block'
    },
    chipLabel: {
      fontSize: '8px',
      height: 'auto',
      lineHeight: '15px',
      paddingLeft: '8px', paddingRight: '8px',
      paddingTop: '0', paddingBottom: '0', textTransform: 'uppercase'
    }
  }
  renderMoreTags = () => {
    const otherChips = this.props.items.slice(2);
    if (otherChips.length) {
      const chipStyle = Object.assign({}, this._styles.chip, {backgroundColor: '#4F596A', marginBottom: '3px', marginTop: '3px'})
      return(
        <div style={{display: 'inline-block', verticalAlign: 'middle'}}>
          <span
            className="more-horiz-circle"
            onTouchTap={this.handleTouchTap}>
            <MoreHoriz style={{width: '14px', height: '14px', padding: '2.2px'}} />
          </span>

          <Popover
            style={{backgroundColor: '#2D3B4C', padding: '12px'}}
            open={this.state.popoverOpen}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'middle', vertical: 'bottom'}}
            onRequestClose={this.handleRequestClose}>
            { otherChips.map((t,i) => <Chip
                style={chipStyle}
                labelColor="#FFFFFF"
                labelStyle={this._styles.chipLabel}
                key={i}>
                {t}
              </Chip>) }
          </Popover>
        </div>
      );
    }
  }
  render() {
    const { items, hovered } = this.props;
    if (!items || !items.length) return <div />;

    const chipStyle = Object.assign({}, this._styles.chip, {
      backgroundColor: hovered ? '#4F596A' : '#7F8FA4'
    });

    const firstTwoChips = items.slice(0,2);

    return (<div>
      {firstTwoChips.map((t,i) => <Chip
        style={chipStyle}
        labelStyle={this._styles.chipLabel}
        labelColor="#FFFFFF"
        key={i}>
        {t}
      </Chip>)}

      { this.renderMoreTags() }
    </div>)
  }
}

export class ContactLatestActivity extends ContactTableCellBase {
  _styles = {
    activityTitlePreview: {fontSize: '12px', fontWeight: 900, color: '#222B3A', margin: 0, letterSpacing: 0},
    activityTimePreview: {display: 'block', fontSize: '10px', marginTop: 0}
  }
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      hovered: false
    })
  }
  handleMouseOver = e => {
    this.setState({hovered: true});
  }
  handleMouseLeave = e => {
    this.setState({hovered: false});
  }
  render() {
    const activity = this.props.items;
    const cellHovered = this.state.hovered;
    if (!activity 
        || !activity.time
        || !activity.type
        || !timelineFilters[activity.type]) return <div />;

    const primaryText = !cellHovered ?
      <span className="secondary-text">{ activity.time }</span> :
      <span
        style={this._styles.activityTitlePreview}>
        { timelineFilters[activity.type].label }
      </span>;
    const secondaryText = !cellHovered ?
      <span /> :
      <span
        className="secondary-text"
        style={this._styles.activityTimePreview}>
        { activity.time }
      </span>

    const icon = (<span
      className="link-el"
      style={{top: '5px'}}>
      {timelineFilters[activity.type].icon}
    </span>);

    return (
      <div
        className="timeline-cell-data"
        style={{width: '135px'}}
        onTouchTap={this.handleTouchTap}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseLeave}>
        <ListItem
          style={{fontSize: '12px', paddingLeft: '42px', cursor: 'pointer'}}
          disabled={true}
          primaryText={primaryText}
          secondaryText={secondaryText}
          leftIcon={icon} />
        <Popover
          open={this.state.popoverOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}>
          <div className="timeline-activity">
            <h4>
              <i className="icon">{timelineFilters[activity.type].icon}</i>
              {timelineFilters[activity.type].label}
              <time>{activity.time}</time>
            </h4>
            <p>{activity.desc}</p>
          </div>
        </Popover>
      </div>
    );
  }
}

export class ContactOpportunities extends ContactTableCellBase {
  render() {
    const opportunities = this.props.items;
    if (!opportunities || !opportunities.length || !opportunities[0]) {
      return <ListItem disabled={true} primaryText={<FlatButton className="rounded-expandable" label="Add Opportunity" labelStyle={{fontSize: '10px', fontWeight: 900, lineHeight: '30px'}} icon={<ContentAdd style={{width: '12px', height: '30px'}} />} />} />;
    }
    return (
      <div className="opportunities">
        <ListItem disabled={true} primaryText={<h3>{opportunities[0].title}</h3>} secondaryText={<span className="secondary-text">$ 
          {opportunities[0].price}
          {opportunities.length > 1 && (
            <div>
              <a className="opportunities-trigger" href="#open-more-opportunities" onTouchTap={this.handleTouchTap}>+ {opportunities.length} more</a>
              <Popover
                open={this.state.popoverOpen}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                onRequestClose={this.handleRequestClose}>
                <div style={{padding: '20px'}}>
                  { opportunities.slice(1).map((o,i) => <div className="other-opportunities" key={i}><h5>{o.title}</h5><h6>{o.price}</h6></div>) }
                </div>
              </Popover>
            </div>
          )}
        </span>} />
        
      </div>
    );
  }
}

export class TagsBulkManagement extends ContactTableCellBase {
  static propTypes = {
    selectedItemsLenght: PropTypes.number.isRequired
  }
  render() {
    return(
      <div>
        <FlatButton
          icon={<Label color="#FFF" style={{height: '13px'}} />}
          onTouchTap={this.handleTouchTap}
          label="Edit Tags"
          labelStyle={styles.bulkActionsButton} />
        <Popover
          open={this.state.popoverOpen}
          anchorEl={this.state.anchorEl}
          onRequestClose={this.handleRequestClose}>
          <div style={{padding: '20px 35px'}}>
            <h3>Edit Tags</h3>
            <ChipInput
              defaultValue={filteringData.tags}
              fullWidth={true} />
            <p><small>{this.props.selectedItemsLenght} contact selected</small></p>
          </div>
        </Popover>
      </div>
    );
  }
}

export class ContactStatus extends Component {
  state = {
    _internalState: {},
    cellHovered: false
  }
  static propTypes = {
    contact: PropTypes.object.isRequired,
    hovered: PropTypes.bool
  }
  static defaultProps = {
    contact: {},
    hovered: false
  }
  constructor(props) {
    super(props);
    this.state = {
      _internalState: props.contact
    };
  }
  handleStatusChange = (event, key, value) => {    
    this.setState({
      _internalState: Object.assign({}, this.state._internalState, {status: value})
    })
  }
  handleMouseOver = e => this.setState({cellHovered: true});
  handleMouseLeave = e => this.setState({cellHovered: false});
  render() {
    const contact = this.state._internalState;
    return (
      <div onMouseOver={this.handleMouseOver} onMouseLeave={this.handleMouseLeave}>
        <StatusCircle style={{position: 'absolute', top: '50%', marginTop: '-14px', left: '20px'}} color={filteringData.statuses.filter(s => s.label === contact.status)[0].color} />
        <DropDownMenu
          underlineStyle={{border: 'none'}}
          labelStyle={{fontSize: '10px', lineHeight: '57px', textTransform: 'uppercase', fontFamily: '"AvenirLTStd-Heavy"'}}
          value={contact.status}
          onChange={this.handleStatusChange}
          iconStyle={{display: this.state.cellHovered ? 'initial' : 'none', fill: themeColors.blue, right: 'auto', left: '-43px', top: '3px'}}>
          { filteringData.statuses.map((s,i) => (
            <MenuItem key={i} value={s.label} leftIcon={<StatusCircle color={s.color} />} primaryText={s.label} style={{fontSize: '10px', textTransform: 'uppercase'}}></MenuItem>
          )) }
        </DropDownMenu>
      </div>
    );
  }
}


export default class ContactScreenBlockComponent extends Component {
  state = {
    contactsType: 'All Contacts',
    filterAmount: '$100.000',
    settingsOpen: false,
    selectedItems: [],
    isOpenSnackbar: false,
    snackbarAction: null,
    snackbarMessage: '',
    handleSnackbarAction: () => false
  };
  static propTypes = {
    apiContactPropertyName: PropTypes.string,
    toolbarTotalLabel: PropTypes.string,
    tableWrapStyle: PropTypes.object,

    // redux / router
    mediaQueries: PropTypes.object.isRequired,
    updateSideNav: PropTypes.func.isRequired,
    fetchData: PropTypes.func.isRequired,
    apiState: PropTypes.object.isRequired,
    isSideNavExpanded: PropTypes.bool
  };
  static defaultProps = {
    apiContactPropertyName: 'contacts',
    toolbarTotalLabel: 'Total' // ie. 11 Total
  }
  handleFilterContacts = (event, index, contactsType) => this.setState({contactsType});
  handleFilterAmounts = (event, index, filterAmount) => this.setState({filterAmount});
  handleSettingsDrawerToggle = e => {
    if (e) e.preventDefault();
    this.setState({settingsOpen: !this.state.settingsOpen})
    if (this.props.mediaQueries.screenWidth <= 1440) {
      this.props.updateSideNav({expanded: false});
    }
  };
  handleSelectItem = (selectedItems) => {
    this.setState({selectedItems});
  };

  
  handleContactDelete = (contact) => {
    console.log('contact', contact);
    this.setState({
      snackbarAction: null,
      snackbarMessage: 'Contact deleted sucessfully',
      isOpenSnackbar: true
    })
  }
  handleContactsDelete = (contacts) => {
    console.log('selected contacts ', contacts);
    this.setState({
      selectedItems: [],
      snackbarAction: null,
      snackbarMessage: contacts.length + ' contacts deleted successfully.',
      isOpenSnackbar: true
    })
  }
  handleCloseSnackbar = () => this.setState({isOpenSnackbar: false});

  render() {
    const { apiState, mediaQueries, history, apiContactPropertyName, toolbarTotalLabel, tableWrapStyle, isSideNavExpanded } = this.props;
    const isMobileView = mediaQueries.screenWidth <= 768;
    // if screen width <= 768, apply handheld styles
    const mainWrapperStyle = Object.assign({}, styles.mainWrapper, {
      paddingRight: this.state.settingsOpen && !isMobileView ? '240px' : 0,
    });
    const drawerStyle = Object.assign({}, styles.filterDrawer, {
      position: isMobileView ? 'fixed' : 'absolute',
      padding: isMobileView ? '40px' : '40px 20px'
    });
    const drawerWidth = isMobileView ? '100%' : 240;

    let toolbarActions = [];
    if (this.props.mediaQueries.screenWidth > 1280) {
      toolbarActions = [
        <NavigationMenu key="1" color={themeColors.gold} />,
        <FontIcon key="2" className="goldHover pna icon-grid_view" color={themeColors.mediumGray} style={{fontSize: '17px'}} />,
        <ToolbarSeparator key="3" style={{backgroundColor: themeColors.mediumGray, height: '12px'}} />
      ]
    }

    const contactsFetch = apiState && apiState[apiContactPropertyName];

    return (
      <div>
        <ContactsToolbar
          actions={toolbarActions}
          currentContactType={this.state.contactsType}
          settingsOpen={this.state.settingsOpen}
          handleFilterContacts={this.handleFilterContacts}
          handleSettingsDrawerToggle={this.handleSettingsDrawerToggle}
          selectedItems={this.state.selectedItems}
          totalItemsText={contactsFetch && contactsFetch.response && contactsFetch.response.data && `${contactsFetch.response.data.length} ${toolbarTotalLabel}`}
          onDeleteContacts={this.handleContactsDelete} />

        <div className="contacts-listing-wrapper" style={mainWrapperStyle}>
          
          <ContactsDrawer
            drawerStyle={drawerStyle}
            drawerWidth={drawerWidth}
            settingsOpen={this.state.settingsOpen}
            mobileCloseBtn={isMobileView && <a href="#done" onClick={e => {e.preventDefault(); this.handleSettingsDrawerToggle()}}>Done</a>}
            currentFilterAmount={this.state.filterAmount} />

          { contactsFetch && contactsFetch.isFetching &&
            <CircularProgress
              size={80}
              thickness={5} /> }
          { contactsFetch && contactsFetch.response && contactsFetch.response.data &&
            <ContactsTable
              screenWidth={mediaQueries.screenWidth}
              sideNavExpanded={isSideNavExpanded}
              contacts={contactsFetch.response.data}
              history={history}
              onSelectItem={this.handleSelectItem}
              selectedItems={this.state.selectedItems}
              mainWrapStyle={tableWrapStyle}
              onContactDelete={this.handleContactDelete} /> }

        </div>

        <Snackbar
          open={this.state.isOpenSnackbar}
          message={this.state.snackbarMessage}
          action={this.state.snackbarAction}
          onActionTouchTap={this.state.handleSnackbarAction}
          onRequestClose={this.handleCloseSnackbar}
          {...themeInputProps.snackbar} />
      </div>
    );
  }
}

const styles = {
  toolbar: {color: '#fff', fontSize: '14px', height: '50px', paddingLeft: '30px'},
  toolbarTitle: {marginLeft: '20px', color: themeColors.mediumGray, fontSize: '10px', lineHeight: '35px'},
  mainWrapper: {position: 'relative'},
  filterDrawer: {
    zIndex: 8999,
    top: 0, right: 0,
    backgroundColor: themeColors.contactFiltersDrawerBg,
    color: '#fff',
    overflowX: 'hidden'
  },
  headRowItem: {
    paddingTop: '7px', paddingBottom: '7px',
    textTransform: 'uppercase', color: "#7F8FA4", lineHeight: '18px', fontSize: '10px', letterSpacing: '1px'
  },
  columnOptionsListItem: {padding: '3px 0', margin: 0, textAlign: 'right'},
  bulkActionsButton: {color: '#FFF', fontSize: '10px', letterSpacing: '1px'},
  dialog: {
    warning: {width: '100%', maxWidth: '400px'}
  }
}

const filteringData = {
  contactTypes: ['All Contacts', 'Prospects', 'Investors', 'Favorites'],
  amounts: ['$100.000', '$1.000.000', '$10.000.000'],
  amountDirection: ['More than', 'Less than'],
  offerings: ['Park Avenue South', 'Nomad', 'Corporate Band', '84 William', 'AKA UN Recap', '17 John', '46th Street', 'AKA UN', 'AKA Wall Street Recap', '25th Street'],
  tags: ['urgent', 'family'],
  statuses: [
    {
      label: 'Contacting',
      color: themeColors.statusBlue
    },
    {
      label: 'Nurture',
      color: themeColors.statusGold
    },
    {
      label: 'Interested',
      color: themeColors.green
    },
    {
      label: 'Not interested',
      color: themeColors.statusRed
    }
  ]
};