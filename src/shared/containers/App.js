import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../../redux/actions';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import pnaTheme from '../../theme.config';

import SideNav from '../components/SideNav';
import TopBar from '../components/TopBar';
import MainContentWrapper from '../components/MainContentWrapper';
import MainContentWrapperUnauthorized from '../components/MainContentWrapperUnauthorized';

// @TEMP:
import injectTapEventPlugin from 'react-tap-event-plugin';

import { throttle } from 'lodash';
const throttleInterval = 150;

class App extends Component {
  static propTypes = {
    // Injected by React Redux
    route: PropTypes.object,
    mediaQueries: PropTypes.object,
    mediaChanged: PropTypes.func,
    triggeredSearch: PropTypes.bool
  }

  constructor(props) {
    super(props);
    // store listener reference
    this.throttleResize = throttle(this.handleResize, throttleInterval);
    // state definitio
    this.state = {
      triggeredSearch: false,
      unauthorized: props.route.pathname.indexOf('/noauth/') === 0 // @TEMP
    }
  }

  handleResize = () => {
    this.props.mediaChanged()
  }

  componentWillMount() {
    this.handleResize();
  }

  componentDidMount() {
    window.addEventListener('resize', this.throttleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.throttleResize);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.mediaQueries.screenWidth !== this.props.mediaQueries.screenWidth) {
      this.props.updateSideNav({expanded: nextProps.mediaQueries.screenWidth > 1280});
    }
    if (nextProps.triggeredSearch !== this.props.triggeredSearch) {
      this.setState({triggeredSearch: nextProps.triggeredSearch});
    }
    if (nextProps.route.pathname !== this.props.route.pathname) {
      this.setState({unauthorized: nextProps.route.pathname.indexOf('/noauth') === 0}) // @TEMP
    }
  }

  render() {
    // const isMobileView = this.props.mediaQueries.screenWidth <= 1280;

    if (this.state.unauthorized) {
      return (
        <MuiThemeProvider muiTheme={pnaTheme}><MainContentWrapperUnauthorized /></MuiThemeProvider>
      );
    }
    return(
      <MuiThemeProvider muiTheme={pnaTheme}>
        <div style={placeholderStyle}>
          <TopBar />
          <SideNav />
          { !this.state.triggeredSearch && <MainContentWrapper /> }
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  route: state.router.location,
  mediaQueries: state.mediaQueryTracker,
  triggeredSearch: state.searchController.triggered
});
const mapDispatchToProps = dispatch => ({
  mediaChanged: bindActionCreators(Actions.mediaChanged, dispatch),
  updateSideNav: bindActionCreators(Actions.updateSideNav, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));

// @TEMP
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const placeholderStyle = {
  maxWidth: '1920px',
  minHeight: '100vh',
  height: 'auto',
  width: '100%',
  margin: '0 auto',
  position: 'relative',
  overflow: 'hidden'
};