import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import DevTools from './DevTools';
import { ConnectedRouter } from 'react-router-redux';
import App from './App';


const Root = ({store, history}) => (
  <Provider store={store}>
    <div>
      <ConnectedRouter history={history}><App /></ConnectedRouter>
      <DevTools />
    </div>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}

export default Root;