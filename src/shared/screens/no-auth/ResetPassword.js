import React, { Component } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import Footer from '../../components/forms/dialog-footer';
import { customFlatButton } from '../../../theme.config';

export default class ResetPassword extends Component {
  render() {
    return(
      <Dialog
        title="Reset your password"
        modal={true}
        open={true}
        contentStyle={styles.dialog}
        contentClassName="pna-dialog info-dialog"
        overlayStyle={{background: 'none'}}>
        <p>What would you like your new password to be?</p>
        <form className="form-wrap">
          <div className="form-group">
            <label>Password</label>
            <input type="password" placeholder="Enter your new password..." />
            <p className="desc">Password must be at least 6 alphanumeric characters.</p>
          </div>

          <div className="clearfix">
            <div className="form-group pull-right">
              <FlatButton label="Save" style={customFlatButton.info} />
            </div>
          </div>
        </form>

        <Footer />
      </Dialog>
    );
  }
}

const styles = {
  dialog: {width: '100%', maxWidth: 435}
}