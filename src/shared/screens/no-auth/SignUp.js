import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Footer from '../../components/forms/dialog-footer';

import { customFlatButton } from '../../../theme.config';

export default class SignUp extends Component {
  state = {
    activeScreen: 1
  }
  setActiveScreen = (activeScreen) => {
    this.setState({activeScreen})
  }
  render() {
    const formContent = this.state.activeScreen === 1 ? (
      <div>
        <p>Create an Account and be part of our Associates Community.</p>

        <div className="form-group">
          <input type="text" placeholder="First Name" />
        </div>

        <div className="form-group">
          <input type="text" placeholder="Last Name" />
        </div>

        <div className="form-group">
          <input type="email" placeholder="Enter your email" />
        </div>

        <div className="form-group">
          <input type="password" placeholder="Enter your password" />
        </div>

        <div className="clearfix">
          <div className="form-group pull-right">
            <FlatButton onTouchTap={this.setActiveScreen.bind(null, 2)} label="Next" style={customFlatButton.info} />
          </div>
        </div>
      </div>
    ) : (
      <div>
        <div className="form-group">
          <p>Are you a citizen or legal resident of the United States?</p>
          <label className="inline-label mr-3"><input type="radio" value="yes" /> Yes</label>
          <label className="inline-label"><input type="radio" value="no" /> No</label>
        </div>

        <div className="form-group">
          <label>Country of residence</label>
          <input type="text" />
        </div>

        <div className="form-group">
          <label>Phone</label>
          <input type="text" />
        </div>

        <div className="form-group">
          <label className="inline-label">
            <input type="checkbox" /> I agree to the <a href="#terms">Terms of Use</a> and <a href="#privacy">Privacy Policy</a>
          </label>
        </div>

        <div className="clearfix">
          <div className="form-group pull-right">
            <FlatButton onTouchTap={this.setActiveScreen.bind(null, 1)} label="Back" style={customFlatButton.secondary} />

            <FlatButton label="Sign up" style={customFlatButton.info} />
          </div>
        </div>
      </div>
    );
    return (
      <Dialog
        title="Sign Up"
        modal={true}
        open={true}
        contentStyle={styles.dialog}
        contentClassName="pna-dialog info-dialog"
        overlayStyle={{background: 'none'}}>

        <form className="form-wrap">
          { formContent }
        </form>

        <section className="divider-block">
          <p>Already have an account? <Link to="/noauth/login">Login</Link></p>
        </section>

        <Footer />
      </Dialog>
    );
  }
}

const styles = {
  dialog: {width: '100%', maxWidth: 435}
}