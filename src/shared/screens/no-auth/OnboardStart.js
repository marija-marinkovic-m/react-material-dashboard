import React, { Component } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { customFlatButton } from '../../../theme.config';


export default class OnboardStart extends Component {
  render() {
    return (
      <Dialog
        title="Hello, Thiago!"
        modal={true}
        open={true}
        contentStyle={styles.dialog}
        contentClassName="pna-dialog info-dialog"
        overlayStyle={{background: 'none'}}>
        <p><strong>You are just two steps from being interviewed by Example Company Network.</strong></p>

        <p>Please submit the information scelerisque mollis cras elit donec dolor erat vivamus ultrices, sit pellentesque volutpat purus etima metus...</p>

        <div className="clearfix">
          <div className="form-group pull-right">
            <FlatButton label="Start" style={customFlatButton.info} />
          </div>
        </div>
      </Dialog>
    );
  }
}

const styles = {
  dialog: {width: '100%', maxWidth: 435}
}