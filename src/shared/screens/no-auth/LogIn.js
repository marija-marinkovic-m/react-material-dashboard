import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Footer from '../../components/forms/dialog-footer';

import { customFlatButton } from '../../../theme.config';


export default class LogIn extends Component {
  state = {
    activeScreen: 1
  }
  render() {
    return (<Dialog
      title="Login"
      modal={true}
      open={true}
      contentStyle={styles.dialog}
      contentClassName="pna-dialog info-dialog"
      overlayStyle={{background: 'none'}}>
      <p>Log into your account and get access to your dashboard.</p>

      <form className="form-wrap">
        <div className="form-group">
          <label>Email</label>
          <input type="text" />
        </div>

        <div className="form-group">
          <label>Password</label>
          <input type="password" />
        </div>
        <Link to="/noauth/forgot-password"><em>Forgot your password?</em></Link>

        <div className="clearfix">
          <div className="form-group pull-right">
            <FlatButton label="Login" style={customFlatButton.info} />
          </div>
        </div>
      </form>


      <section className="divider-block">
        <p>Don't have and account? <Link to="/noauth/signup">Sign up</Link></p>
      </section>

      <Footer />
    </Dialog>)
  }
}

const styles = {
  dialog: {width: '100%', maxWidth: 435}
}