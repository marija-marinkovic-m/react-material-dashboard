import React, { Component } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import Footer from '../../components/forms/dialog-footer';
import { customFlatButton } from '../../../theme.config';


export default class ForgotPassword extends Component {
  render() {
    return (
      <Dialog
        title="Forgot your password?"
        modal={true}
        open={true}
        contentStyle={styles.dialog}
        contentClassName="pna-dialog info-dialog"
        overlayStyle={{background: 'none'}}>
        <p>Enter your Email Address and we'll send you a link to reset your password.</p>

        <form className="form-wrap">
          <div className="form-group">
            <label>Email</label>
            <input type="text" placeholder="Enter your Email Address..." />
          </div>

          <div className="clearfix">
            <div className="form-group pull-right">
              <FlatButton label="Cancel" style={customFlatButton.secondary} />
              <FlatButton label="Reset" style={customFlatButton.info} />
            </div>
          </div>
        </form>

        <Footer />
      </Dialog>
    );
  }
}

const styles = {
  dialog: {width: '100%', maxWidth: 435}
}