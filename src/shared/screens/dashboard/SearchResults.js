import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { fetchData, updateSideNav } from '../../../redux/actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import { Tabs, Tab } from 'material-ui/Tabs';

// icons
import CloseIcon from 'material-ui/svg-icons/navigation/close';

import { themeColors, getCurrentTabStyle } from '../../../theme.config';
import ContactsBlock from '../../components/Contacts';
import SalesBlock from '../../components/Sales';

import { throttle } from 'lodash';
const throttleInterval = 100;

const apiContactPropertyName = 'contactsSearchFetch';
const apiSalePropertyName = 'salesSearchFetch';

export const EscButton = props => (
  <div className="go-back-button" {...props}>
    <FloatingActionButton
      mini={true}
      disabled={true}
      disabledColor={themeColors.darkGray}
      iconStyle={{cursor: 'pointer', fill: themeColors.white}}>
      <CloseIcon />
    </FloatingActionButton>
    <p>ESC</p>
  </div>
);

class SearchResults extends Component {
  state = {
    currentTab: 'contacts'
  }
  static propTypes = {
    mediaQueries: PropTypes.object,
    apiState: PropTypes.object,
    fetchData: PropTypes.func,
    history: PropTypes.object
  }

  constructor(props) {
    super(props);
    // listener reference
    this.throttleKeypress = throttle(this.handleKeypress, throttleInterval);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.throttleKeypress);

    // fetch data
    this.props.fetchData('/static/search-contacts.json', apiContactPropertyName);
    this.props.fetchData('/static/search-sales.json', apiSalePropertyName);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.throttleKeypress);
  }

  handleKeypress = e => {
    const event = e || window.event;
    if (event.keyCode === 27) {
      this.goBack();
    }
  }
  handleTabChange = currentTab => this.setState({currentTab});
  goBack = () => this.props.history.goBack();

  renderTabs = (tabs, currentTab) => tabs.map((tab,i) => <Tab key={i} style={getCurrentTabStyle(currentTab, tab.value)} {...tab} />);

  render() {
    const { match } = this.props;
    const { currentTab } = this.state;
    const tabs = [
      {
        label: 'Contacts',
        value: 'contacts',
        children: <ContactsBlock
          apiContactPropertyName={apiContactPropertyName}
          toolbarTotalLabel={`Results for "${match.params.term}"`}
          tableWrapStyle={{height: 'calc(100vh - 184px)', overflow: 'auto', backgroundColor: themeColors.darkGray}}
          {...this.props} />
      },
      {
        label: 'Sales',
        value: 'sales',
        children: <SalesBlock
          apiFetchPropertyName={apiSalePropertyName}
          toolbarTotalLabel={`Results for "${match.params.term}"`}
          tableWrapStyle={{height: 'calc(100vh - 184px)', overflow: 'auto', backgroundColor: themeColors.darkGray}}
          {...this.props} />
      }
    ];
    return <div className="main-wrap" style={{position: 'static'}}>

      <Tabs
        inkBarStyle={{display: 'none'}}
        value={currentTab}
        onChange={this.handleTabChange}>
        { this.renderTabs(tabs, currentTab) }
      </Tabs>
    </div>
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  apiState: state.apiDataController
});
const mapDispatchToProps = dispatchEvent => ({
  fetchData: bindActionCreators(fetchData, dispatchEvent),
  updateSideNav: bindActionCreators(updateSideNav, dispatchEvent)
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchResults));