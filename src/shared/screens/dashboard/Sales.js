import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchData, updateSideNav } from '../../../redux/actions';

import AppSpeedDial from '../../components/FloatingSpeedDial';
import SalesBlock from '../../components/Sales';

const apiFetchPropertyName = 'salesFetch';

class Deals extends Component {
  static propTypes = {
    mediaQueries: PropTypes.object,
    fetchData: PropTypes.func,
    apiState: PropTypes.object,
    history: PropTypes.object
  }

  componentDidMount() {
    this.props.fetchData('/static/deals-mockup.json', apiFetchPropertyName);
  }

  render() {
    return <div>
      <SalesBlock
        {...this.props}
        apiFetchPropertyName={apiFetchPropertyName}
        toolbarTotalLabel="Total"
        tableWrapStyle={{height: 'calc(100vh - 114px)', overflow: 'auto'}} />
      <AppSpeedDial />
    </div>
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  apiState: state.apiDataController
});
const mapDispatchToProps = dispatch => ({
  updateSideNav: bindActionCreators(updateSideNav, dispatch),
  fetchData: bindActionCreators(fetchData, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Deals);
