import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchData } from '../../../redux/actions';
import cn from 'classnames';

import { Tabs, Tab } from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';

import FloatingSpeedDial from '../../components/FloatingSpeedDial';
import TimelineTab from '../../components/Timeline';
import SalesCommissionsTab from '../../components/TabSalesCommissions';
import ContactOpportunitiesTab from '../../components/TabContactsOpportunities';

import { getCurrentTabStyle, themeColors } from '../../../theme.config';

class Dashboard extends Component {
  state = {
    currentTab: 'sales-commissions'
  }
  static propTypes = {
    mediaQueries: PropTypes.shape({screenWidth: PropTypes.number, screenHeight: PropTypes.number}),
    sideNavController: PropTypes.shape({expanded: PropTypes.bool}),
    apiState: PropTypes.object,
    fetchData: PropTypes.func,
    history: PropTypes.object
  };

  componentWillMount() {
    if (this.props.apiState.dashboardTimeline) return;
    this.props.fetchData('/static/timeline-general-mockup-today.json', 'dashboardTimeline', 'Today');
    this.props.fetchData('/static/timeline-general-mockup-week.json', 'dashboardTimeline', 'This Week');
    this.props.fetchData('/static/timeline-general-mockup-month.json', 'dashboardTimeline', 'This Month');
    this.props.fetchData('/static/timeline-mockup-older.json', 'dashboardTimeline', 'May')
  }

  handleTabChange = currentTab => this.setState({currentTab})

  render() {
    const { mediaQueries, sideNavController, history } = this.props;
    const { currentTab } = this.state;
    const isMobileView = mediaQueries.screenWidth <= 768;

    const tabWrapStyle = isMobileView ?
      {minHeight: 'auto', height: 'auto'} :
      {minHeight: 'auto', height: 'calc(100vh - 64px - 70px)', overflow: 'auto'};
    
    let tabs = [
      {
        label: 'Sales and Commissions',
        value: 'sales-commissions',
        children: <div
          className="main-placeholder home pna-bg"
          style={tabWrapStyle}>
          <SalesCommissionsTab
            screenWidth={mediaQueries.screenWidth}
            sideNavController={sideNavController.expanded} />
        </div>
      },
      {
        label: 'Contacts and Opportunities',
        value: 'contacts-opportunities',
        children: <div
          className="main-placeholder home pna-bg"
          style={tabWrapStyle}>
          <ContactOpportunitiesTab
            screenWidth={mediaQueries.screenWidth}
            sideNavController={sideNavController.expanded}
            history={history} />
        </div>
      },
      {
        label: 'Timeline',
        value: 'timeline',
        children: <div className="white-bg">
          <TimelineTab 
            content={this.props.apiState.dashboardTimeline}
            maxWidth={!isMobileView ? '768px' : '100%'}
            style={!isMobileView ? styles.desktopTimelineTab : null} />
        </div>
      }
    ];

    if (isMobileView) {
      tabs = tabs.map(t => ({
          value: t.value,
          children: t.children,
          icon: <FontIcon className={cn(['tabIcon', 'pna'], {
            'icon-roundedDollar': t.value === 'sales-commissions',
            'icon-people-icon': t.value === 'contacts-opportunities',
            'icon-timeline': t.value === 'timeline'
          })} />
        }))
    }

    return <div>
      <Tabs
        className="pna-tabs"
        inkBarStyle={{display: 'none'}}
        tabItemContainerStyle={isMobileView ? styles.mobileTabItemContainer : {padding: '0 30px'}}
        value={currentTab}
        onChange={this.handleTabChange}>
        { tabs.map((tab, i) => {
          return <Tab key={i} style={getCurrentTabStyle(currentTab, tab.value, isMobileView)} {...tab} />
        }) }
      </Tabs>
      <FloatingSpeedDial mobileStyle={{bottom: '95px'}} />
    </div>
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  sideNavController: state.sideNavController,
  apiState: state.apiDataController
});
const mapDispatchToProps = dispatchEvent => ({
  fetchData: bindActionCreators(fetchData, dispatchEvent)
});
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)


const styles = {
  desktopTimelineTab: {minHeight: 'auto', height: 'calc(100vh - 64px - 70px - 61px)', overflow: 'auto', paddingBottom: '150p', boxSizing: 'border-box'},

  mobileTabItemContainer: {position: 'fixed', zIndex: 999, bottom: 0, left: 0, right: 0, backgroundColor: themeColors.darkGray},
  desktopTabItemContainer: {height: 'calc(100vh - 64px - 70px)', overflow: 'auto', paddingBottom: '300px'}
}