import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchData } from '../../../redux/actions';

import { MessagesTab } from './ContactProfile';
import FloatingSpeedDial from '../../components/FloatingSpeedDial';

class Messages extends Component {
  static propTypes = {
    mediaQueries: PropTypes.object,
    apiState: PropTypes.object,
    fetchData: PropTypes.func
  }
  componentWillMount() {
    const {fetchData} = this.props;
    fetchData('/static/contact-messages.json', 'myMessages');
  }
  render() {
    const speedDial = <FloatingSpeedDial />;
    if (!this.props.apiState.myMessages) return speedDial;
    return (
      <div>
        <MessagesTab content={this.props.apiState.myMessages} />
        {speedDial}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  apiState: state.apiDataController
});
const mapDispatchToProps = dispatch => ({
  fetchData: bindActionCreators(fetchData, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Messages);