import React, { Component } from 'react';
import PropTypes from 'prop-types';

import DatePicker from '../../util/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';

import Menu from 'material-ui/Menu';
import Popover from 'material-ui/Popover';

// temp
import Dialog from 'material-ui/Dialog';

// iconst
import CameraIcon from 'material-ui/svg-icons/image/camera-alt';
import CalendarIcon from 'material-ui/svg-icons/action/event';
import CircleIcon from 'material-ui/svg-icons/av/fiber-manual-record';

import { themeColors } from '../../../theme.config';

import './AddContact.css';


import UploadImageBg from '../../assets/images/dummy-avatar.png';

export default class extends Component {
  static propTypes = {
    data: PropTypes.object
  }
  static defaultProps = {
    data: {}
  }
  constructor(props) {
    super(props);
    this.state = {
      calendar: false,
      isDialogOpen: false,
      formData: Object.assign({}, initialFormData, props.data)
    }
  }
  handleSubmit = e => {
    e.preventDefault();
    // @temp dialog to reveal form state
    this.setState({isDialogOpen: true});
  }
  handleCloseDialog = () => this.setState({isDialogOpen: false})

  
  updateFormData = update => this.setState({formData: Object.assign({}, this.state.formData, update)});
  handleInputChange = e => {
    const { name, value } = e.target;
    this.updateFormData({[name]: value});
  }
  handleDateChange = (event, date) => this.updateFormData({dateOfBirth: date})
  handleStatusChange = (event, index, status) => this.updateFormData({status});

  // partialized if validation, formating etc. needed
  handleEmailsChange = emails => this.setState({formData: Object.assign({}, this.state.formData, {emails})});
  handlePhonesChange = phones => this.setState({formData: Object.assign({}, this.state.formData, {phones})});
  handleIMSChange = ims => this.setState({formData: Object.assign({}, this.state.formData, {ims})});

  render() {
    const { formData } = this.state;
    const currentStatus = statusOptions.filter(s => s.label === formData.status);
    return(
      <div>
        <header className="intro-bar">
          <h1>Add New Contact</h1>
          <span className="gold-divider" />
          <p>Add main information now and manage your contact later in the contact profile.</p>
        </header>

        <div className="main-placeholder">
          <section className="content-wrapper">
            <form className="clearfix" onSubmit={this.handleSubmit}>
              <aside className="form-group pull-left w-2">
                <div
                  style={{
                    position: 'relative', width: '82px', height: '82px', borderRadius: '50%', background: 'black',
                    padding: '16px 10px', margin: '0 0 30px', boxSizing: 'border-box',
                    textAlign: 'center',
                    backgroundImage: 'url('+ UploadImageBg +')', backgroundPosition: 'center center', backgroundSize: 'cover',
                    boxShadow: '0 8px 16px 0 rgba(34,43,58,0.5)'
                  }}>
                  <CameraIcon color="#FFF" style={{height: '15px'}} />
                  <span className="text-uppercase" style={{color: '#fff', display: 'block', fontSize: '10px', letterSpacing: '1px', lineHeight: '14px'}}>Add Photo</span>
                <input
                  type="file"
                  style={{
                    cursor: 'pointer', position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, width: '100%', opacity: '0'
                  }} />
                </div>
              </aside>
              <div className="form-group pull-right w-8">
                <div className="form-group">
                  <DefaultFormField
                    label="First Name"
                    name="firstName"
                    value={formData.firstName}
                    onChange={this.handleInputChange}
                    required={true}
                    placeholder="First Name" />
                </div>

                <div className="clearfix">
                  <div className="form-group w-6 pull-left">
                    <DefaultFormField
                      label="Middle name"
                      name="middleName"
                      value={formData.middleName}
                      onChange={this.handleInputChange}
                      placeholder="Middle Name" />
                  </div>

                  <div className="form-group w-6 pull-right">
                    <DefaultFormField
                      label="Last Name"
                      name="lastName"
                      value={formData.lastName}
                      onChange={this.handleInputChange}
                      placeholder="Last Name"
                      required={true} />
                  </div>
                </div>{/* Middle / Last Name */}

                <div className="form-group w-6">
                  <label>Date of birth</label>
                  <div className="icon-field">
                    <CalendarIcon color="#BFCCDC" style={{pointerEvents: 'none'}} />
                    <DatePicker
                      hintText="Date of bitrth"
                      value={formData.dateOfBirth}
                      onChange={this.handleDateChange}
                      mode="landscape"
                      container="inline"
                      className="date-of-birth"
                      autoOk={true}
                      okLabel={false}
                      dialogContainerStyle={{minHeight: '0', height: '295px', overflow: 'hidden'}}
                      inputStyle={{padding: '0 14px 0 40px'}}
                      textFieldStyle={{
                        height: '37px', lineHeight: '37px',
                        border: '0.5px solid #222B3A', borderRadius: '2px', padding: '0', fontSize: '12px',
                        boxSizing: 'border-box'
                      }} />
                  </div>
                </div>{/* date of birth */}

                <div className="form-group w-6">
                  <label>Status</label>
                  <div className="icon-field">
                    <CircleIcon color={currentStatus.length ? currentStatus[0].color : '#BFCCDC'} />
                    <SelectField
                      value={formData.status}
                      onChange={this.handleStatusChange}
                      underlineStyle={{display: 'none'}}
                      className="select-input text-uppercase"
                      autoWidth={true}
                      listStyle={{top: '-19px'}}
                      iconStyle={{width: '37px', height: '37px', padding: 0, top: '4px', right: '-12px'}}
                      menuStyle={{
                        height: '37px', lineHeight: '37px',
                        border: '0.5px solid #222B3A', borderRadius: '2px', padding: '0 14px', fontSize: '12px',
                        boxSizing: 'border-box'
                      }}>
                      <MenuItem value="" primaryText="New Contact" className="text-uppercase" disabled={true} />
                      { statusOptions.map((s,i) => <MenuItem key={i} value={s.label} primaryText={s.label} className="text-uppercase" />) }
                    </SelectField>
                  </div>
                </div>{/* status */}

                <div className="clearfix">
                  <div className="form-group w-6 pull-left">
                    <DefaultFormField
                      label="Title"
                      placeholder="Add Contact Title"
                      name="title"
                      value={formData.title}
                      onChange={this.handleInputChange} />
                  </div>
                  <div className="form-group w-6 pull-right">
                    <DefaultFormField
                      label="Company"
                      value={formData.company}
                      name="company"
                      onChange={this.handleInputChange}
                      placeholder="Add Prospect Company" />
                  </div>
                </div>{/* title / company */}


                <div className="form-group">
                  <EmailRepeater emails={formData.emails} onUpdate={this.handleEmailsChange} />
                </div>

                <div className="form-group">
                  <PhoneRepeater phones={formData.phones} onUpdate={this.handlePhonesChange} />
                </div>

                <div className="form-group">
                  <IMRepeater ims={formData.ims} onUpdate={this.handleIMSChange} />
                </div>

                <div className="form-group">
                  <label>Address</label>
                  <input type="text" name="addressStreet" value={formData.addressStreet} onChange={this.handleInputChange} placeholder="Street" />
                  <input type="text" name="addressCity" value={formData.addressCity} onChange={this.handleInputChange} placeholder="City" />
                  <input type="text" name="addressCountry" value={formData.addressCountry} onChange={this.handleInputChange} placeholder="Country" />
                  <input type="text" name="addressState" value={formData.addressState} onChange={this.handleInputChange} placeholder="State" />
                  <input type="text" name="addressPostalCode" value={formData.addressPostalCode} onChange={this.handleInputChange} placeholder="Postal Code" />
                </div>


                <div className="form-group">
                  <FlatButton style={{color: '#FFFFFF', textTransform: 'uppercase', textAlign: 'center', borderRadius: '30px', float: 'right'}} label="Add" backgroundColor={themeColors.blue} hoverColor={themeColors.blue} onClick={this.handleSubmit} />
                </div>
              </div>
            </form>

            <Dialog
              title="Form Data"
              actions={[<FlatButton label="Close" primary={true} onTouchTap={this.handleCloseDialog} />]}
              modal={false}
              open={this.state.isDialogOpen}
              onRequestClose={this.handleCloseDialog}>
              <code style={{whiteSpace: 'pre-wrap', wordBreak: 'break-all'}}>
                { JSON.stringify(this.state.formData) }
              </code>
            </Dialog>
          </section>
        </div>
      </div>
    );
  }
}


class IMRepeater extends Component {
  static propTypes = {
    ims: PropTypes.array.isRequired,
    onUpdate: PropTypes.func.isRequired
  }

  addContactIM = () => {
    this.props.onUpdate(this.props.ims.concat([{client: 'skype', handle: ''}]));
  }
  handleImFieldsClientChange = (i, event, index, value) => {
    const {ims} = this.props;
    const updatedIM = {client: value, handle: ims[i].handle};
    this.props.onUpdate([...ims.slice(0,i), updatedIM, ...ims.slice(i + 1)]);
  }
  handleImFieldsInputChange = e => {
    const im = 'im';
    const { target } = e;
    const index = parseInt(target.name.slice(im.length), 10);
    const updatedIM = {client: this.props.ims[index].client, handle: target.value};
    this.props.onUpdate([...this.props.ims.slice(0, index), updatedIM, ...this.props.ims.slice(index + 1)]);
  }
  handleRemoveIMSField = index => this.props.onUpdate([...this.props.ims.slice(0,index), ...this.props.ims.slice(index + 1)])

  render() {
    return(
      <div>
        <label>IM</label>
        { this.props.ims.map((im,i) => (
          <div key={i} className="repeater-item">
            <SelectField
              autoWidth={false}
              underlineStyle={{display: 'none'}}
              className="im-type"
              value={im.client}
              style={{
                float: 'left',
                width: '28%',
                paddingLeft: '10px', marginRight: '2%',
                height: '37px', lineHeight: '37px',
                border: '0.5px solid #222b3a', borderRadius: '2px',
                fontSize: '12px', letterSpacing: 0,
                boxSizing: 'border-box'
              }}
              iconStyle={{top: '-5px'}}
              onChange={this.handleImFieldsClientChange.bind(null, i)}
              dropDownMenuProps={{className: 'imClientSelect', autoWidth: true}}>
              <MenuItem value="skype" primaryText="Skype" />
              <MenuItem value="aim" primaryText="AIM" />
              <MenuItem value="facebook" primaryText="Facebook Messenger" />
              <MenuItem value="whatsapp" primaryText="Whatsapp" />
            </SelectField>
            <input
              type="text"
              value={im.handle}
              name={`im${i}`}
              onChange={this.handleImFieldsInputChange}
              style={{width: '70%'}} />
            <span className="remove" onClick={this.handleRemoveIMSField.bind(null, i)} />
          </div>
        )) }
        <div className="add-trigger" onClick={this.addContactIM}><span className="add-icon" />Add IM</div>
      </div>
    );
  }
}

class PhoneRepeater extends Component {
  state = {
    anchorEl: null
  }
  static propTypes = {
    phones: PropTypes.array.isRequired,
    onUpdate: PropTypes.func.isRequired
  }

  addContactPhone = () => this.props.onUpdate(this.props.phones.concat([{type: 'home', number: ''}]));
  handlePhoneFieldsTypeTouchTap = (i, e) => {
    e.preventDefault();
    this.setState({
      [`popoverPhone${i}`]: true, 
      anchorEl: e.currentTarget
    })
  }
  handlePhoneFieldsTypeChange = (i, event, value) => {
    this.setState({
      [`popoverPhone${i}`]: false
    })
    
    const updatedPhoneEl = {type: value, number: this.props.phones[i].number};
    this.props.onUpdate([...this.props.phones.slice(0,i), updatedPhoneEl, ...this.props.phones.slice(i + 1)]);
  }
  handlePhoneFieldsInputChange = e => {
    const phone = 'phone';
    const { target } = e;
    const index = parseInt(target.name.slice(phone.length), 10);
    const updatedPhoneEl = {type: this.props.phones[index].type, number: target.value};
    this.props.onUpdate([...this.props.phones.slice(0,index), updatedPhoneEl, ...this.props.phones.slice(index+1)]);
  }
  handlePhoneFieldsTypeRequestClose = i => this.setState({[`popoverPhone${i}`]: false});
  handleRemovePhonesField = index => this.props.onUpdate([...this.props.phones.slice(0,index), ...this.props.phones.slice(index + 1)]);

  render() {
    return(
      <div>
        <label>Phone</label>
        { this.props.phones.map((p,i) => (
          <div className="repeater-item" key={i}>
            
            <FlatButton
              label={p.type}
              style={{position: 'absolute', top: 0, left: 0, zIndex: 3, width: '60px', minWidth: '60px'}}
              labelStyle={{textTransform: 'capitalize', fontSize: '10px', color: '#1875F0'}}
              onTouchTap={this.handlePhoneFieldsTypeTouchTap.bind(null, i)} />
            <Popover
              open={this.state[`popoverPhone${i}`]}
              anchorEl={this.state.anchorEl}
              onRequestClose={this.handlePhoneFieldsTypeRequestClose.bind(null, i)}
              anchorOrigin={{horizontal: 'left', vertical: 'top'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              useLayerForClickAway={true}>
              <Menu
                value={p.type}
                onChange={this.handlePhoneFieldsTypeChange.bind(null, i)}>
                <MenuItem value="home" primaryText="Home" />
                <MenuItem value="mobile" primaryText="Mobile" />
                <MenuItem value="work" primaryText="Work" />
                <MenuItem value="other" primaryText="Other" />
              </Menu>
            </Popover>

            <input type="text" value={p.number} name={`phone${i}`} onChange={this.handlePhoneFieldsInputChange} placeholder="Add Contact Phone" style={{paddingLeft: '70px'}} />
            <span className="remove" onClick={this.handleRemovePhonesField.bind(null, i)} />
          </div>
        )) }

        <div className="add-trigger" onClick={this.addContactPhone}><span className="add-icon" />Add Phone</div>
      </div>
    );
  }
}

class EmailRepeater extends Component {

  static propTypes = {
    emails: PropTypes.array.isRequired,
    onUpdate: PropTypes.func.isRequired
  }
  
  addContactEmail = () => this.props.onUpdate(this.props.emails.concat(['']));
  handleEmailsFieldChange = e => {
    const email = 'email';
    const { target } = e;
    const index = parseInt(target.name.slice(email.length), 10);
    this.props.onUpdate([...this.props.emails.slice(0,index), target.value, ...this.props.emails.slice(index+1)]);
  }
  handleRemoveEmailsField = index => this.props.onUpdate([...this.props.emails.slice(0,index), ...this.props.emails.slice(index + 1)]);

  render() {
    return (
      <div>
        <label>Email <span className="required" /></label>
        { this.props.emails.map((e,i) => (
          <div key={i} className="repeater-item">
            <input type="text" name={`email${i}`} onChange={this.handleEmailsFieldChange} placeholder="Add Contact Email" value={e} />
            <span className="remove" onClick={this.handleRemoveEmailsField.bind(null, i)} />
          </div>
        )) }
        <div className="add-trigger" onClick={this.addContactEmail}><span className="add-icon" />Add Email</div>
      </div>
    );
  }
}

const DefaultFormField = ({name = '', value = '', onChange = () => false, label = '', placeholder = '', required = false}) => (
  <div>
    <label>
      { label }&nbsp;{ required && <span className="required" /> }
    </label>
    <input
      type="text"
      name={name}
      value={value}
      onChange={onChange}
      placeholder={placeholder} />
  </div>
);

const initialFormData = {
  firstName: '',
  middleName: '',
  lastName: '',
  dateOfBirth: null,
  status: '',
  title: '',
  company: '',
  emails: [''],
  phones: [
    { type: 'home', number: '' }
  ],
  ims: [
    { client: 'skype', handle: '' }
  ],
  addressStreet: '',
  addressCity: '',
  addressCountry: '',
  addressState: '',
  addressPostalCode: ''
}

const statusOptions = [
  {
    label: 'Contacting',
    color: themeColors.statusBlue
  },
  {
    label: 'Nurture',
    color: themeColors.statusGold
  },
  {
    label: 'Interested',
    color: themeColors.green
  },
  {
    label: 'Not interested',
    color: themeColors.statusRed
  }
];