import React, { Component } from 'react';
import { themeColors } from '../../../theme.config';

import banner from '../../assets/images/banner.jpg';

export default class extends Component {
  render() {
    return(
      <div style={{
        backgroundImage: 'url(' + banner + ')',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundAttachment: 'fixed',
        backgroundSize: 'cover',
        minHeight: 'calc(100vh - 64px)',
        padding: '50px',
        boxSizing: 'border-box'
      }}>
        
        <h3 style={{color: themeColors.statusBlue, fontWeight: 'normal'}}>{this.props.location.pathname} -- coming soon</h3>
        {/*<br />
        <code>Currently: 404. No match: { this.props.location.pathname }</code>*/}
      </div>
    );
  }
}