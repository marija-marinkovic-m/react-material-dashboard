/**
 * @TODO: This component is work in progress, needs heavy refactor once we have presentational part done per design
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchData } from '../../../redux/actions';
import { Link } from 'react-router-dom';

import cn from 'classnames';

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import { Tabs, Tab } from 'material-ui/Tabs';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import Dialog from 'material-ui/Dialog';
import { GridList, GridTile } from 'material-ui/GridList';
import ProfileAvatar from '../../components/ProfileAvatar';

import IconMenu from 'material-ui/IconMenu';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ListItem from 'material-ui/List/ListItem';

import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import AppSpeedDial from '../../components/FloatingSpeedDial';

import { themeColors, themeInputProps, getCurrentTabStyle, customFlatButton } from '../../../theme.config';

import FontIcon from 'material-ui/FontIcon';

// icons
import SocialPerson from 'material-ui/svg-icons/social/person';
import BackArrowIcon from '../../util/svg-icons/backArrow';
import PenIcon from '../../util/svg-icons/pen';
import ToggleStar from 'material-ui/svg-icons/toggle/star';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import InvestorIcon from '../../util/svg-icons/investor';
import StatusCircle from '../../util/svg-icons/statusCircle';
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import ArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentMail from '../../util/svg-icons/open-mail';
import CommunicationPhone from '../../util/svg-icons/phone';
import PinCircleIcon from '../../util/svg-icons/pin';
import ActionQuestionAnswer from 'material-ui/svg-icons/action/question-answer';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import OutlineCircle from '../../util/svg-icons/outline-circle';
import PlaneIcon from '../../util/svg-icons/plane';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import AttachFile from 'material-ui/svg-icons/editor/attach-file';
import ForwardIcon from 'material-ui/svg-icons/content/forward';
import ReplyIcon from 'material-ui/svg-icons/content/reply';
import ReplyAllIcon from 'material-ui/svg-icons/content/reply-all';
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import CheckCircle from 'material-ui/svg-icons/action/check-circle';

import ReactQuill from 'react-quill';

import { renderCommissionStatus, SaleProgressBar } from '../../components/Sales';

import TimelineTab from '../../components/Timeline';

import 'react-quill/dist/quill.bubble.css';
import 'react-quill/dist/quill.snow.css';
import './ContactProfile.css';

const apiContactPropertyName = 'contactsFetch';
const apiContactProfileProperyName = 'contactProfileFetch';

class ContactProfile extends Component {
  state = {
    collapsedDrawer: false,
    userData: null,
    timelineData: null,
    collapsedProfileTab: false,
    currentTab: 'timeline',
    zIndex: 99
  }
  static propTypes = {
    mediaQueries: PropTypes.object, // redux
    apiState: PropTypes.object, //redux
    fetchData: PropTypes.func, // redux
    match: PropTypes.object, // router
  };

  static defaultProps = {
    apiState: {}
  };

  componentWillMount() {
    const { apiState, fetchData } = this.props;
    if (apiState && apiState[apiContactPropertyName] && typeof apiState[apiContactPropertyName].response !== 'undefined') {
      this.setCurrentContactMainInfo();
    } else {
      fetchData('/static/contacts-mockup.json', apiContactPropertyName);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.apiState[apiContactPropertyName] && typeof prevProps.apiState[apiContactPropertyName].response === 'undefined' && typeof !this.props.apiState[apiContactPropertyName].isFetching) {
      this.setCurrentContactMainInfo();
    }
  }

  setCurrentContactMainInfo() {
    const { apiState, match } = this.props;
    if (!apiState[apiContactPropertyName] || !apiState[apiContactPropertyName].response || !apiState[apiContactPropertyName].response.data) return;
    
    const current = apiState[apiContactPropertyName].response.data.filter(c => c.slug === match.params.profile);
    if (!current.length) return;
    this.setState({
      userData: current[0]
    }, () => {
      this.setCurrentContactTimeline();
    })
  }

  setCurrentContactTimeline() {
    this.props.fetchData('/static/timeline-mockup-today.json', apiContactProfileProperyName, 'timeline', 'Today');
    this.props.fetchData('/static/timeline-mockup-week.json', apiContactProfileProperyName, 'timeline', 'This Week');
    this.props.fetchData('/static/timeline-mockup-month.json', apiContactProfileProperyName, 'timeline', 'This Month');
    this.props.fetchData('/static/timeline-mockup-older.json', apiContactProfileProperyName, 'timeline', 'May')
  }

  setTabData = (propName, endpoint) => {
    if (!this.props.apiState[apiContactProfileProperyName][propName]) {
      this.props.fetchData(endpoint, apiContactProfileProperyName, propName);
    }
  }

  getIconMenu = () => (
    <IconMenu
      iconButtonElement={<IconButton><MoreVertIcon color="#FFF" /></IconButton>}
      anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      targetOrigin={{horizontal: 'right', vertical: 'top'}}>
      <MenuItem primaryText="Send message" />
      <MenuItem primaryText="Settings" />
      <MenuItem primaryText="Help" />
    </IconMenu>
  );

  toggleProfileTab = (event) => {
    this.setState({collapsedProfileTab: !this.state.collapsedProfileTab})
  }
  handleStatusChange = (event, index, status) => this.setState({userData: Object.assign({}, this.state.userData, {status})});
  handleTabChange = currentTab => this.setState({currentTab})

  updateZIndex = isOpenDialog => this.setState({zIndex: isOpenDialog ? 99 : 2909})

  render() {

    const isMobileView = this.props.mediaQueries.screenWidth <= 768;
    const isTabletView = this.props.mediaQueries.screenWidth <= 1280;
    const { collapsedProfileTab, userData, currentTab, zIndex } = this.state;

    const desktopTabWrap = Object.assign({}, styles.desktopTabWrap, isTabletView ? {
      height: 'calc(100vh - 150px)'
    } : {});
    const desktopTimelineTabWrap = Object.assign({}, styles.desktopTimelineTabWrap, isTabletView ? {
      height: 'calc(100vh - 150px)'
    } : {});

    if (userData === null) return <div>Loading...</div>;
    if (userData === false) return <div>User not found</div>; // user not found


    // content
    const apiData = this.props.apiState[apiContactProfileProperyName];
    const content = {
      timeline: apiData && apiData.timeline ? apiData.timeline : {},
      deals: apiData && apiData.contactDeals,
      messages: apiData && apiData.contactMessages,
      notes: apiData && apiData.contactNotes
    }

    return (
      <div className="main-wrap">
        <Toolbar style={Object.assign({}, styles.toolbar, isTabletView ? {height: '37px'} : {})}>
          <ToolbarGroup firstChild={true}>
            <FlatButton
              style={{color: themeColors.gold, marginLeft: '10px'}}
              label="Back"
              icon={<BackArrowIcon />}
              onClick={e => this.props.history.goBack()} />
          </ToolbarGroup>
          { !isMobileView && <AppSpeedDial /> }
        </Toolbar>

        {/* we have two view here
        ------ either it is mobileView where everything is tabbed 
        ------ or we have desktop/tablet view --> left side ProfileTab drawer - right side all other sections tabbed */}

        { (() => {
          let theTabs = [
            {
              label: 'Timeline',
              value: 'timeline'
            },
            {
              label: 'Sales',
              value: 'sales',
              onActive: this.setTabData.bind(null, 'contactDeals', '/static/contact-deals.json')
            },
            {
              label: 'Messages',
              value: 'messages',
              onActive: this.setTabData.bind(null, 'contactMessages', '/static/contact-messages.json')
            },
            {
              label: 'Notes',
              value: 'notes',
              onActive: this.setTabData.bind(null, 'contactNotes', '/static/contact-notes.json')
            }
          ];

          if (!isMobileView) {
            let otherTabProps = {
              timeline: {
                children: <TimelineTab onOpenDialog={this.updateZIndex} content={content.timeline} style={desktopTimelineTabWrap} />
              },
              sales: {
                children: <div style={desktopTabWrap}><DealsTab content={content.deals} /></div>
              },
              messages: {
                children: <MessagesTab content={content.messages} isMobileView={isMobileView} isTabletView={isTabletView} desktopTabWrap={desktopTabWrap} />
              },
              notes: {
                children: <div style={desktopTabWrap}><NotesTab content={content.notes} /></div>
              }
            }

            return (<div style={Object.assign({}, styles.desktopMainContainer, isTabletView ? {height: 'calc(100vh - 99px)'} : {})}>
              <Drawer
                containerStyle={{position: 'absolute', top: 0, bottom: 0, left: 0, zIndex: 99, backgroundColor: '#283443'}}
                width={ collapsedProfileTab ? 65 : 340}
                open={true}>

                {
                  (() => {
                    if (!collapsedProfileTab) {
                      return (<div>
                        <Toolbar style={{backgroundColor: 'inherit'}}>
                          <ToolbarGroup firstChild={true}>
                            <FlatButton style={{minWidth: 'auto'}} primary={true} icon={<ToggleStar color={themeColors.gold} />} />
                          </ToolbarGroup>
                          <ToolbarGroup lastChild={true}>
                            <FlatButton
                              icon={<ChevronLeft color="white" />}
                              hoverColor="transparent"
                              style={{margin: '10px 0', minWidth: '60px'}}
                              onClick={this.toggleProfileTab} />
                          </ToolbarGroup>
                        </Toolbar>

                        <ProfileTab userData={this.state.userData} handleStatusChange={this.handleStatusChange} />
                      </div>);
                    } else {
                      return (<div>
                        <ProfileAvatar
                          userData={userData}
                          avatarProps={{size: 37, style:{margin: '14px'} }} />
                        <FlatButton
                          icon={<ChevronRight color="white" />}
                          hoverColor="transparent"
                          style={{margin: '10px 0', minWidth: '60px'}}
                          onClick={this.toggleProfileTab} />
                        <h1 className="vertical-title">{ userData.firstName + ' ' + userData.lastName }</h1>
                      </div>)
                    }
                  })()
                }

              </Drawer>
              <section className="profile-tabs" style={{marginLeft: !collapsedProfileTab ? '340px' : '65px'}}>
              <Tabs
                className="pna-tabs"
                style={{zIndex}}
                tabItemContainerStyle={styles.desktopTabItemContainer}
                inkBarStyle={styles.desktopInkBar}
                onChange={this.handleTabChange}>
                { theTabs.map((tab,i) => <Tab key={i} style={Object.assign({}, getCurrentTabStyle(currentTab, tab.value), isTabletView ? {height: 50, padding: 0} : {})} {...tab} {...otherTabProps[tab.value]} />) }
              </Tabs>
              </section>
            </div>);
          }

          // add profile tab for mobile view
          theTabs.unshift({label: 'Profile', value: 'profile'})
          // mobile tab children
          let otherTabProps = {
            profile: {
              icon: <SocialPerson />,
              children: <ProfileTab userData={userData} handleStatusChange={this.handleStatusChange} />,
              label: null
            },
            timeline: {
              icon: <FontIcon className="tabIcon pna icon-timeline" />,
              children: <TimelineTab content={content.timeline} />,
              label: null
            },
            sales: {
              icon: <FontIcon className="tabIcon pna icon-roundedDollar" />,
              children: <div style={styles.mobileTabWrap}><DealsTab content={content.deals} /></div>,
              label: null
            },
            messages: {
              icon: <FontIcon className="tabIcon pna icon-messages-icon-inactive" />,
              children: <div style={styles.mobileTabWrap}><MessagesTab content={content.messages} isMobileView={isMobileView} mobileTabWrap={styles.mobileTabWrap} /></div>,
              label: null
            },
            notes: {
              icon: <FontIcon className="tabIcon pna icon-note-small" />,
              children: <div style={styles.mobileTabWrap}><NotesTab content={content.notes} /></div>,
              label: null
            }
          };

          return(<div>
            <Tabs
              className="pna-tabs"
              tabItemContainerStyle={styles.mobileTabItemContainer}
              inkBarStyle={styles.mobileInkBar}
              onChange={this.handleTabChange}>
              { theTabs.map((tab, i) => <Tab key={i} {...tab} {...otherTabProps[tab.value]} style={{height: 70}} />) }
            </Tabs>
          </div>);
        })() }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  apiState: state.apiDataController
});

const mapDispatchToProps = dispatch => ({
  fetchData: bindActionCreators(fetchData, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactProfile);

class ProfileTab extends Component {

  static propTypes = {
    userData: PropTypes.object.isRequired,
    handleStatusChange: PropTypes.func
  }

  static defaultProps = {
    handleStatusChange: () => false
  }

  constructor(props) {
    super(props);
    this.state = {
      opportunitiesExpanded: true,
      detailsExpanded: true,
      isOpenInviteDialog: false,
      isOpenInvitationSentSuccess: false,
      selectedOpportunities: [],
      userData: Object.assign({}, props.userData, {dialogEditedOpportunities: []})
    }
  }

  handleToggle = widget => this.setState({[widget]: !this.state[widget]});

  // Invite to invest methods
  handleOpenInviteDialog = () => this.setState({isOpenInviteDialog: true})
  handleCloseInviteDialog = () => this.setState({isOpenInviteDialog: false})
  handleCloseInvitationSentDialog = () => this.setState({isOpenInvitationSentSuccess: false})
  handleOpportunitiesSelection = selectedOpportunities => this.setState({selectedOpportunities});
  handleOpportunityEdit = (opportunity, event) => {
    event.preventDefault();
    event.stopPropagation();
    const dialogEditedOpportunities = [opportunity, ...this.state.userData.dialogEditedOpportunities]

    this.setState({userData: Object.assign({}, this.state.userData, {dialogEditedOpportunities})})
  }
  handleOpportunityDataChange = (id, event, newValue) => {
    const all = this.state.userData.opportunities;
    const oIndex = all.findIndex(o => o.id === id);
    if (oIndex < 0) return;

    const updatedData = Object.assign({}, all[oIndex], {[event.target.name]: newValue})
    const opportunities = [...all.slice(0, oIndex), updatedData, ...all.slice(oIndex+1)];
    this.setState({userData: Object.assign({}, this.state.userData, {opportunities})})
  }
  handleSaveOpportunityEdit = (opportunity) => {
    const dialogEditedOpportunities = this.state.userData.dialogEditedOpportunities.filter(item => item.id !== opportunity.id);
    this.setState({userData: Object.assign({}, this.state.userData, {dialogEditedOpportunities})})
  }
  handleSendInvitation = () => {
    console.log('sending....');
    console.log(this.state.selectedOpportunities);
    this.setState({
      isOpenInviteDialog: false,
      isOpenInvitationSentSuccess: true
    })
  }

  render() {
    const { handleStatusChange } = this.props;
    const { detailsExpanded, opportunitiesExpanded, userData, selectedOpportunities } = this.state;

    const currStatus = apiData.statuses.filter(s => s.label === userData.status);
    const descriptionSource = apiData.descriptions.map(d => ({text: d, value: <MenuItem primaryText={d} />}));


    const dialogActions = [
      <FlatButton
        label="Cancel"
        onClick={this.handleCloseInviteDialog}
        style={customFlatButton.secondary} />,
      <FlatButton
        label="Send Invitation"
        style={customFlatButton.info}
        disabled={Boolean(userData.dialogEditedOpportunities.length) || !Boolean(selectedOpportunities.length)}
        onClick={this.handleSendInvitation} />
    ];

    return (
      <div className="profile-tab">

        {/* AVATAR */}
        <div style={{textAlign: 'center', marginTop: '25px', marginBottom: '55px', color: '#FFF'}}>
          <ProfileAvatar userData={userData} avatarProps={{size: 86}} />

          <h1 style={{fontFamily: '"AvenirLTStd-Roman"', fontSize: '20px', letterSpacing: '0px', fontWeight: 'normal'}}>{ userData.firstName + ' ' + userData.lastName }</h1>
          { userData.type === 'investor' && <div><InvestorIcon style={{width: '25px', height: '35px', display: 'inline-block', verticalAlign: 'middle'}} color={themeColors.blue} /><span style={{display: 'inline-block', verticalAlign: 'top', lineHeight: '26px', color: themeColors.mediumGray, fontSize: '10px', letterSpacing: '1px', marginLeft: '-3px'}}>INVESTOR</span></div> }

          <FlatButton
            style={customFlatButton.info}
            label="Invite to invest"
            onTouchTap={this.handleOpenInviteDialog} />

          {/* invite dialog */}
          <Dialog
            title="Invitation to Invest"
            open={this.state.isOpenInviteDialog}
            actions={dialogActions}
            modal={true}
            contentStyle={styles.dialog}
            autoScrollBodyContent={true}
            contentClassName="pna-dialog info-dialog">

            <section className="pna-userData">
              <div style={{float: 'left', marginRight: '25px'}}>
                <ProfileAvatar userData={userData} avatarProps={{size: 86}} />
              </div>

              <div style={{paddingTop: '15px'}}>
                <h1 style={{margin: userData.type === 'investor' ? '6px 0 0' : '15px 0 0'}}>{ userData.firstName + ' ' + userData.lastName }</h1>
                { userData.type === 'investor' ? <div><InvestorIcon style={{width: '25px', height: '32px', display: 'inline-block', verticalAlign: 'middle'}} color={themeColors.blue} /><span style={{display: 'inline-block', verticalAlign: 'top', lineHeight: '25px', color: themeColors.mediumGray, fontSize: '10px', letterSpacing: '1px', marginLeft: '-3px'}}>INVESTOR</span></div> : <div style={{height: '32px'}} /> }
              </div>
            </section>

            <h4>List of All Opportunities for { userData.firstName + ' ' + userData.lastName }</h4>
            <p style={{marginBottom: '40px'}}>Pick one or multiple Opportunity on which you want to send an Invitation to complete an investment with Example Company Network. You can edit your opportunity before sending.</p>


            <Table
              multiSelectable={true}
              onRowSelection={this.handleOpportunitiesSelection}>
              <TableBody deselectOnClickaway={false}>
                { userData.opportunities.length && userData.opportunities.map((o,i) => {
                  const currentlyEdited = userData.dialogEditedOpportunities.filter(item => item.id === o.id);
                  const isSelected = Boolean(selectedOpportunities.filter(s => s === i).length);

                  if (currentlyEdited.length) {
                    return (
                      <TableRow selectable={false} selected={isSelected} className="pna-darkTextRow" displayBorder={false} key={i}>
                        <TableRowColumn colSpan="2">
                          <GridList cols={3} cellHeight="auto">
                            <GridTile cols={1.7}>
                              <TextField {...Object.assign({}, themeInputProps.textField, {
                                className: 'pnaTextFieldDark',
                                name: 'title',
                                value: o.title,
                                onChange: this.handleOpportunityDataChange.bind(null, o.id)
                              })} />
                            </GridTile>
                            <GridTile>
                              <TextField {...Object.assign({}, themeInputProps.textField, {
                                className: 'pnaTextFieldDark',
                                name: 'price',
                                value: o.price,
                                onChange: this.handleOpportunityDataChange.bind(null, o.id)
                              })} />
                            </GridTile>
                            <GridTile style={{textAlign: 'right'}} cols={0.3}>
                              <IconButton onTouchTap={this.handleSaveOpportunityEdit.bind(null, o)}>
                                <CheckCircle color={themeColors.green} />
                              </IconButton>
                            </GridTile>
                          </GridList>
                        </TableRowColumn>
                      </TableRow>
                    );
                  }

                  return (<TableRow selectable={true} selected={isSelected} className="pna-darkTextRow" displayBorder={false} key={i}>
                    <TableRowColumn>
                      { o.imgSrc && <figure><img width="40" src={process.env.PUBLIC_URL + '/' + o.imgSrc} alt="investment preview" /></figure> }
                      <p>{ o.title }<br />{ o.price }</p>
                    </TableRowColumn>
                    <TableRowColumn onClick={this.handleOpportunityEdit.bind(null, o)} style={{width: '50px', textAlign: 'right'}}>
                      <a href="#edit" onClick={this.handleOpportunityEdit.bind(null, o)}>Edit</a>
                    </TableRowColumn>
                  </TableRow>);
                }) }
              </TableBody>
            </Table>

          </Dialog>

          {/* invitation sent dialog  */}
          <Dialog
            title="Invitation to Invest Sent"
            open={this.state.isOpenInvitationSentSuccess}
            actions={[<FlatButton label="OK" style={customFlatButton.info} onClick={this.handleCloseInvitationSentDialog} />]}
            modal={true}
            contentStyle={styles.dialog}
            contentClassName="pna-dialog info-dialog">
            <p>Congratulations! Your Invitation for { userData.firstName + ' ' + userData.lastName } to complete an investment with Prodigy Network for Opportunities: </p>

            <Table>
              <TableBody displayRowCheckbox={false} selectable={false}>
                { selectedOpportunities.length && selectedOpportunities.map((sIndex,i) => {
                  const o = userData.opportunities[sIndex];
                  if (!o) return '';

                  return (<TableRow className="pna-darkTextRow" displayBorder={false} key={i}>
                    <TableRowColumn>
                      { o.imgSrc && <figure><img width="40" src={process.env.PUBLIC_URL + '/' + o.imgSrc} alt="investment preview" /></figure> }
                      <p>{ o.title }<br />{ o.price }</p>
                    </TableRowColumn>
                  </TableRow>);

                }) }
              </TableBody>
            </Table>

            <p>Lorem ipsum egestas malesuada felis portitor semper aenean sociosqu, turpis scelerisque erat nam lacus interdum justo nostra, non.</p>
          </Dialog>
        </div>

        <Divider style={styles.divider} />

        {/* STATUS */}
        <Toolbar style={{backgroundColor: 'transparent', marginBottom: '8px'}}>
          <ToolbarGroup firstChild={true} style={{marginLeft: '0px'}}><label>Status</label></ToolbarGroup>
          <ToolbarGroup lastChild={true} style={{marginRight: '-62px'}}>
            <StatusCircle style={{position: 'absolute', top: '20px', left: '0px'}} color={currStatus.length ? currStatus[0].color : 'black'} />
            <DropDownMenu
              underlineStyle={{border: 'none'}}
              labelStyle={{color: '#FFF', fontSize: '10px', textTransform: 'uppercase', fontFamily: '"AvenirLTStd-Heavy"', letterSpacing: '1px'}}
              value={userData.status}
              onChange={handleStatusChange}>
              { apiData.statuses.map((s,i) => (
                <MenuItem key={i} value={s.label} leftIcon={<StatusCircle color={s.color} />} primaryText={s.label} style={{fontSize: '10px', textTransform: 'uppercase', fontFamily: '"AvenirLTStd-Heavy"', letterSpacing: '1px'}}></MenuItem>
              )) }
            </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>

        <Divider style={styles.divider} />

        {/* DESCRIPTION */}
        <div className="widget">
          <AutoComplete fullWidth={true} hintText="Add description..." hintStyle={{color: '#FFF', fontSize: '10px'}} filter={AutoComplete.noFilter} dataSource={descriptionSource} underlineShow={false} />
          <IconButton style={{position: 'absolute', right: '0px', top: '10px'}}><ArrowDropDownIcon color={themeColors.lightGray} /></IconButton>
        </div>

        <Divider style={styles.divider} />

        {/* OPPORTUNITIES */}
        <Toolbar style={{backgroundColor: 'transparent', marginTop: '6px'}}>
          <ToolbarGroup firstChild={true} style={{marginLeft: '-21px'}}>
            <IconButton
              iconStyle={{color: themeColors.lightGray}}
              onClick={e => this.handleToggle('opportunitiesExpanded')}>
              {opportunitiesExpanded ? <ArrowUp /> : <ArrowDown />}
            </IconButton>&nbsp;Opportunities
          </ToolbarGroup>
          <ToolbarGroup lastChild={true} style={{marginRight: '-12px'}}>
            <FloatingActionButton className="pna-icon-btn" mini={true} zDepth={0} backgroundColor={themeColors.darkGray} iconStyle={{width: 30, height: 30}} hoveredStyle={{color: themeColors.gold}}><ContentAdd style={{width: 17, height: 30}} /></FloatingActionButton>
          </ToolbarGroup>
        </Toolbar>
        <div className={cn(['widget', {collapsed: !opportunitiesExpanded}])} style={{marginBottom: 8}}>
          <div className="d-flex space-between">
            <div style={{width: '57%', marginBottom: 20}}><label>Projects</label></div>
            <div style={{width: '38%', marginBottom: 20}}><label>Amount</label></div>
          </div>
          { userData.opportunities.length ? userData.opportunities.map((o,i) => <ProfileProject key={i} index={i} {...o} />) : '' }

          {userData.opportunities.length ? <div className="d-flex space-between">
            <div style={{width: '57%', textAlign: 'right'}}><label>Total</label></div>
            <div style={{width: '38%'}}><label>$&nbsp;<span style={{color: themeColors.lightGray}}>1240000</span></label></div>
          </div> : ''}
        </div>

        <Divider style={styles.divider} />

        {/* CONTACT DETAILS */}
        <Toolbar style={{backgroundColor: 'transparent', marginTop: '5px'}}>
          <ToolbarGroup firstChild={true} style={{marginLeft: '-21px'}}>
            <IconButton
              iconStyle={{color: themeColors.lightGray}}
              onClick={this.handleToggle.bind(this, 'detailsExpanded')}>
              {detailsExpanded ? <ArrowUp /> : <ArrowDown />}
            </IconButton>&nbsp;Contact Details
          </ToolbarGroup>
          <ToolbarGroup lastChild={true} style={{marginRight: '-12px'}}>
            <Link to="/contacts/add"><FloatingActionButton className="pna-icon-btn" mini={true} zDepth={0} backgroundColor={themeColors.darkGray} iconStyle={{width: 30, height: 30}}><PenIcon style={{width: 13, height: 30}} /></FloatingActionButton></Link>
          </ToolbarGroup>
        </Toolbar>
        <div className={cn(['widget'], {collapsed: !detailsExpanded})} style={{marginBottom: 8, padding: '0 4px'}}>
          <ListItem style={styles.contactDetail} leftIcon={<ContentMail style={{width:'15px', top: '3px'}} color={themeColors.gray} />} disabled={true} primaryText="pr.manager@hotmail.com" />
          <ListItem style={styles.contactDetail} leftIcon={<CommunicationPhone style={{width:'15px', top: '3px'}} color={themeColors.gray} />} disabled={true} primaryText="0234 4567 7665" />
          <ListItem style={styles.contactDetail} leftIcon={<PinCircleIcon style={{width:'15px', top: '3px'}} color={themeColors.gray} />} disabled={true} primaryText="West End 45ht Stree, San Francisco 90123 California, US" />
        </div>
      </div>
    );
  }
}

function DealsTab ({content}) {
  const deals = content && content.response && content.response.data;
  return (
    <div className="deals-table">
      { deals && <Table
        selectable={false}
        style={{width: 'auto', minWidth: '100%'}}
        bodyStyle={{overflowX: 'auto'}}>
        <TableBody displayRowCheckbox={false} showRowHover={false}>
          <TableRow className="head-row">
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Subscription Date" /></TableRowColumn>
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Document Status" /></TableRowColumn>
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Offering" /></TableRowColumn>
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Amount" /></TableRowColumn>
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Funds Received" /></TableRowColumn>
            <TableRowColumn><ListItem style={styles.salesHeadRowItem} disabled={true} primaryText="Commission" /></TableRowColumn>
          </TableRow>
          { deals.map((d,i) => {
            return (
            <TableRow key={i}>
              <TableRowColumn className="gray-bg" style={{paddingLeft: 40, paddingTop: 25, paddingBottom: 25}}><ListItem primaryText={d.subscriptionDate} disabled={true} style={{fontSize: '12px'}} /></TableRowColumn>
              <TableRowColumn className="gray-bg"><ListItem disabled={true} style={{fontSize: '12px'}}>
                <SaleProgressBar deal={d} />
              </ListItem></TableRowColumn>
              <TableRowColumn className="gray-bg">
                <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={d.offering} />
              </TableRowColumn>
              <TableRowColumn className="gray-bg">
                <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={`$ ${d.amount}`} />
              </TableRowColumn>
              <TableRowColumn>
                <ListItem disabled={true} style={{fontSize: '12px'}} primaryText={`$ ${d.fundsReceived}`} />
              </TableRowColumn>
              <TableRowColumn>
                <ListItem disabled={true} style={{fontSize: '12px'}}>
                    { renderCommissionStatus(d) }
                  </ListItem>
              </TableRowColumn>
            </TableRow>
          )}) }
        </TableBody>
      </Table> }
    </div>
  );
}

export class MessagesTab extends Component {
  state = {
    currentTabId: 'inbox',
    currentMessageId: null,
    replyEditorActive: false
  }
  static propTypes = {
    content: PropTypes.object,
    isMobileView: PropTypes.bool,
    isTabletView: PropTypes.bool
  }
  static defaultProps = {
    content: null,
    isMobileView: false,
    isTabletView: false
  }

  componentDidMount() {this.quillRefs()}
  componentDidUpdate() {this.quillRefs()}

  quillRefs = () => {
    if (!this.reactQuillRef || typeof this.reactQuillRef.getEditor !== 'function') return;
    this.quillRef = this.reactQuillRef.getEditor();
  }
  openMessage = currentMessageId => this.setState({currentMessageId})
  toggleReplyEditor = () => {
    this.setState({replyEditorActive: !this.state.replyEditorActive}, () => {
      this.quillRef.focus();
    })
  }

  render() {
    const { content, isMobileView, isTabletView } = this.props;
    const data = content && content.response && content.response.data;
    const currentMessageId = this.state.currentMessageId;

    const stylesDesktopTabWrap = Object.assign({}, styles.desktopTabWrap, isTabletView ? {height: `calc(100vh - 150px)`} : {});

    return (
      !isMobileView ?
      <div className="main-placeholder">
        <aside className="messages-toolbar" style={stylesDesktopTabWrap}>
          { apiData.messagesTabs.map((a,i) => {
            const defaultStyle = styles.messagesTab.asideToolbar.button;
            const style = this.state.currentTabId === a.id ? Object.assign({}, defaultStyle, {color: '#1875F0'}) : defaultStyle;
            return <FlatButton key={i} icon={a.icon} style={style} />
          }) }
        </aside>
        <section className="messages-content">
          <ul className="messages-list" style={stylesDesktopTabWrap}>
            { data && data.map((m,i) => {
              const active = currentMessageId === m.ID;
              return <li key={i} className={cn(['item', {active}])} onBlur={e => console.log(e)} onClick={this.openMessage.bind(this, m.ID)}>
                <div className="status-bar">
                  { m.status === 'unread' && <OutlineCircle color={active ? '#FFFFFF' : '#BE0000'} style={{width: '10px'}} /> }
                  { m.attachments.length > 0 && <AttachFile color={active ? '#FFFFFF' : 'inherit'} style={{width: '15px', position: 'absolute', left: '10px', bottom: 0}} /> }
                </div>
                <div className="body">
                  <h4>{ m.fromFullName }</h4>
                  <h5>{ m.subject }</h5>
                  <p>{ m.textBody }</p>
                </div>
                <time dateTime={m.receivedAt}>May 6</time>
              </li>
            }) }
          </ul>
          <article className="message" style={stylesDesktopTabWrap}>{ data && 
            <div>
              <header>
                <time dateTime={ data[0].receivedAt }>May 6, 2017 at 6:00 PM</time>
                <h3>{ data[0].fromFullName }<small>{ data[0].from }</small></h3>
                <h3>{ data[0].subject }</h3>
                <ActionDelete color="#7F8FA4" style={{width: '17px', float: 'right', position: 'relative', top: '-23px'}} />
              </header>
              <div className="content" dangerouslySetInnerHTML={{__html: data[0].htmlBody}} />

              <FlatButton icon={<ReplyIcon />} style={{minWidth: 'auto', padding: '0 3px', marginBottom: '30px'}} onClick={this.toggleReplyEditor} />
              <FlatButton icon={<ReplyAllIcon />} style={{minWidth: 'auto', padding: '0 3px', marginBottom: '30px'}} onClick={this.toggleReplyEditor} />
              <FlatButton icon={<ForwardIcon />} style={{minWidth: 'auto', padding: '0 3px', marginBottom: '30px'}} onClick={this.toggleReplyEditor} />

              { this.state.replyEditorActive && <ReactQuill ref={el => this.reactQuillRef = el} /> }
            </div> 
          }</article>
        </section>
      </div>
      : 
      <div style={styles.mobileTabWrap}>
        Mobile view messages
      </div>
    );
  }
}

function NotesTab ({content}) {
  const defaultValue = content && content.response && content.response.data && content.response.data.notes;
  if (defaultValue !== '' && !defaultValue) return <div />;

  const editorProps = {
    theme: 'bubble',
    modules: {
      toolbar: [
        [{ 'header': [1, 2, false] }],
        ['bold', 'italic', 'underline','strike', 'blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
        ['link', 'image'],
        ['clean']
      ],
    },
    formats: [
      'header',
      'bold', 'italic', 'underline', 'strike', 'blockquote',
      'list', 'bullet', 'indent',
      'link', 'image'
    ],
    defaultValue
  }

  return (
    <div className="main-placeholder">
      <span className="note-date-time">{content.response.data.updatedAt}</span>
      <ReactQuill {...editorProps} />
    </div>
  );
}

function ProfileProject ({index, title, price}) {
  return (
    <div className="d-flex space-between">
      <div style={{width: '57%'}}><TextField {...themeInputProps.textField} id={`title-${index}`} fullWidth={true} defaultValue={title} /></div>
      <div style={{width: '38%'}}><TextField {...themeInputProps.textField} id={`price-${index}`} fullWidth={true} defaultValue={price} /></div>
    </div>
  );
}

const styles = {
  toolbar: {color: '#fff', fontSize: '14px', height: '50px', background: themeColors.darkGray},
  divider: {backgroundColor: themeColors.darkGray},
  desktopMainContainer: {
    position: 'relative', width: '100%', height: 'calc(100vh - 114px)'
  },
  desktopTabWrap: {height: 'calc(100vh - 185px)', overflow: 'auto'},
  desktopTabItemContainer: {backgroundColor: '#222B3A', paddingLeft: '20px'},
  desktopInkBar: {display: 'none'},
  mobileTabWrap: {marginBottom: '48px'},
  mobileTabItemContainer: {position: 'fixed', zIndex: 999, bottom: 0, left: 0, right: 0, backgroundColor: '#222B3A'},
  mobileInkBar: {backgroundColor: themeColors.gold, position: 'fixed', zIndex: 999, bottom: '68px'},
  desktopTimelineTabWrap: {height: 'calc(100vh - 185px)', overflow: 'auto', paddingBottom: '300px'},
  mobileTimelineTabWrap: {},
  contactDetail: {color: '#FFF', fontSize: '12px', lineHeight: '20px', fontFamily: "'AvenirLTStd-Roman', sans-serif", fontWeight: 'normal', paddingLeft: '45px', letterSpacing: '0px'},
  messagesTab: {
    asideToolbar: {
      button: {minWidth: '100%', padding: '10px 0', height: 'auto', color: '#7F8FA4'}
    }
  },
  salesHeadRowItem: {
    paddingTop: '7px', paddingBottom: '7px',
    textTransform: 'uppercase', color: "#7F8FA4", lineHeight: '18px', fontSize: '10px', letterSpacing: '1px'
  },
  dialog: {
    width: '100%',
    maxWidth: 535
  }
}

const apiData = {
  statuses: [
    {
      label: 'Contacting',
      color: themeColors.statusBlue
    },
    {
      label: 'Nurture',
      color: themeColors.statusGold
    },
    {
      label: 'Interested',
      color: themeColors.green
    },
    {
      label: 'Not interested',
      color: themeColors.statusRed
    }
  ],
  descriptions: [
    'Ut vitae tempus arcu',
    'Cras rhoncus elit in turpis vestibulum suscipit',
    'Mauris aliquam tellus nunc',
    'Morbi interdum nisi at enim accumsan, sit amet mattis turpis aliquet'
  ],
  messagesTabs: [
    {id: 'inbox', icon: <ActionQuestionAnswer />, notification: 3},
    {id: 'draft', icon: <EmailIcon />, notification: 0},
    {id: 'outbox', icon: <PlaneIcon />, notification: 0},
    {id: 'trash', icon: <ActionDelete />}
  ]
};