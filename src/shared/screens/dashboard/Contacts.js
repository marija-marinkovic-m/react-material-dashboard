import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateSideNav, fetchData } from '../../../redux/actions';

import AppSpeedDial from '../../components/FloatingSpeedDial';
import ContactsComponent from '../../components/Contacts';

const apiContactPropertyName = 'contactsFetch';

class Contacts extends Component {

  static propTypes = {
    mediaQueries: PropTypes.object,
    updateSideNav: PropTypes.func,
    fetchData: PropTypes.func,
    apiState: PropTypes.object
  };

  async componentDidMount() {
    this.props.fetchData('/static/contacts-mockup.json', apiContactPropertyName);
  }
  render() {
    return (
      <div>
        <ContactsComponent 
          {...this.props}
          apiContactPropertyName={apiContactPropertyName}
          toolbarTotalLabel="Total"
          tableWrapStyle={{height: 'calc(100vh - 114px)', overflow: 'auto'}} />
        <AppSpeedDial style={{right: '190px'}} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  mediaQueries: state.mediaQueryTracker,
  apiState: state.apiDataController,
  isSideNavExpanded: state.sideNavController.expanded
});
const mapDispatchToProps = dispatch => ({
  updateSideNav: bindActionCreators(updateSideNav, dispatch),
  fetchData: bindActionCreators(fetchData, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);