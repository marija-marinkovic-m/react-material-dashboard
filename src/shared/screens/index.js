import Home from './dashboard/Home';
import Contacts from './dashboard/Contacts';
import Sales from './dashboard/Sales';
import ContactProfile from './dashboard/ContactProfile';
import AddContact from './dashboard/AddContact';
import Messages from './dashboard/Messages';
import Search from './dashboard/SearchResults';
import NotFound from './dashboard/404';


// no auth screens
import LogIn from './no-auth/LogIn';
import SignUp from './no-auth/SignUp';
import ForgotPassword from './no-auth/ForgotPassword';
import ResetPassword from './no-auth/ResetPassword';
import OnboardStart from './no-auth/OnboardStart';
import NoAuth404 from './no-auth/404';

export {
  // dashboard
  Home,
  Contacts, AddContact, ContactProfile,
  Sales,
  Messages,
  Search,
  NotFound,

  // no auth
  LogIn,
  SignUp,
  ForgotPassword,
  ResetPassword,
  OnboardStart,
  NoAuth404
}
